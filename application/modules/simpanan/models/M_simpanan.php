<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_simpanan extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = false) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
		
			

			// filter
			
			// filter user level
			
			$orderColumn = array(
				2 => "dm.nama_lengkap",
				3 => "dm.agama",
				4 => "m.tanggal_bergabung",
				5 => "dm.jenis_kelamin",
			);
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by('m.id_member', $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

		$this->db->stop_cache();

		// get num rows
		$this->db->select('m.id_member');
		$rowCount = $this->db->get()->num_rows();

		// get result
		$query_result = "
			SELECT
				m.id_member,
				dm.id_data_member id_data,
				m.jenis_kepemilikan_akun,
				nama_lengkap nama,
				jenis jenis_simpanan,
				kategori kategori_simpanan,
				jumlah,
				tanggal tanggal_simpanan
			FROM
				member m
			JOIN data_member dm ON m.id_member = dm.id_member
			JOIN simpanan s ON m.id_member = s.id_member
			JOIN jenis_simpanan j ON j.id_jenis_simpanan = s.id_jenis_simpanan
			JOIN kategori_simpanan k OK k.id_kategori_simpanan = j.id_kategori_simpanan
			WHERE
				dm.is_delete = 0
			AND s.is_delete = 0
		";

		$this->db->limit($post['length'], $post['start']);

		$val = $this->db->get()->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		$jenis_kelamin = array(
			"L" => "LAKI-LAKI",
			"P" => "PEREMPUAN"
		);
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			// $btnAksi .= "
			// <li>
			// 	<a href='{$base}page/detail/$data->slug' id='btn-detail' target='_blank'>
			// 		Detail
			// 	</a>
			// </li>
			// ";
			
			$btnAksi .= "
			<li>
				<a href='{$base}member/form/$data->id_data_member' id='btn-detail'>
					Edit
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_data_member' id='btn-delete'>
					Hapus
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";
			
			$baris = array(
				"no" => $no,
				"id_member" => $data->id_member,
				"aksi" => $aksi,
				"nama_lengkap" => strtoupper($data->nama_lengkap) . " <b>(". strtoupper($data->nama_panggilan) .")</b> <br /><b>$data->nik</b>",
				"agama" => strtoupper($data->agama),
				"tanggal_bergabung" => konversi_tanggal($data->tanggal_bergabung, "j F Y") . "<br /><small>". konversi_tanggal($data->tanggal_bergabung, "H:i") ."</small>",
				"jenis_kelamin" => $jenis_kelamin[$data->jenis_kelamin],
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert("member", array(
			"jenis_kepemilikan_akun" => "personal"
		));
		$data['id_member'] = $this->db->insert_id();
		$this->db->insert("data_member", $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where("id_data_member", $id)
			->update("data_member", $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where("id_data_member", $id)
			->update("data_member", array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("dm.id_data_member", $id)
			->get("data_member dm");
	}
}