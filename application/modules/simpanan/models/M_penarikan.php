<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_penarikan extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = false) {
		$order = $post['order'][0];

		$this->db->start_cache();
			$this->db->from("penarikan p");
			// filter
			$this->db
				->where("p.id_jenis_simpanan", $post["id_jenis_simpanan"])
				->where("p.id_member", $post["id_member"])
				->where("p.is_delete", 0);

			if (!empty($post['tanggal1'])) {
				$this->db->where("p.tanggal >= '$post[tanggal1]'");
			}
			if (!empty($post['tanggal2'])) {
				$this->db->where("p.tanggal <= '$post[tanggal2]'");
			}
			
			$orderColumn = array(
				2 => "p.tanggal",
				3 => "p.jumlah_penarikan",
			);
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by('p.tanggal', "desc");
				$this->db->order_by('p.id_penarikan', $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

		$this->db->stop_cache();

		// get num rows
		$rowCount = $this->db
			->select('p.id_penarikan')
			->get()
			->num_rows();

		// get result
		$val = $this->db
			->select("p.id_penarikan, p.id_jenis_simpanan, p.id_member, p.jumlah_penarikan, p.tanggal")
			->limit($post['length'], $post['start'])
			->get()
			->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}simpanan/penarikan/form/$data->id_jenis_simpanan/$data->id_member/$data->id_penarikan' id='btn-detail'>
					Edit
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_penarikan' id='btn-delete'>
					Hapus
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";
			
			$baris = array(
				"no" => $no,
				"aksi" => $aksi,
				"jumlah" => format_duit($data->jumlah_penarikan),
				"tanggal" => konversi_tanggal($data->tanggal, "j F Y"),
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert("penarikan", $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where("id_penarikan", $id)
			->update("penarikan", $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where("id_penarikan", $id)
			->update("penarikan", array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("id_penarikan", $id)
			->get("penarikan");
	}
}