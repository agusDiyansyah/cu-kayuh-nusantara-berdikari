<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_bunga_simpanan_kelompok extends CI_Model {
	private $tb = "simpanan_bunga";
	private $tb_id = "id_bunga_simpanan";

	public function __construct() {
		parent::__construct();
	}

	public function data ($post, $debug = false) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
			$this->db
				->from("$this->tb b")
				->join("member m", "m.id_member = b.id_member")
				->join("data_kelompok dm", "m.id_member = dm.id_member")
				->where("m.id_member", $post['id_member'])
				->where("b.is_delete", 0);
			
			// filter user level
			
			$orderColumn = array(
				2 => "b.tanggal",
				3 => "b.jumlah",
			);

			if (!empty($post['tanggal1'])) {
				$this->db->where("b.tanggal >= '$post[tanggal1]'");
			}
			if (!empty($post['tanggal2'])) {
				$this->db->where("b.tanggal <= '$post[tanggal2]'");
			}
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by("b.tanggal", "desc");
				$this->db->order_by("b.$this->tb_id", $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

		$this->db->stop_cache();

			// get num rows
			$this->db->select("b.$this->tb_id");
			$rowCount = $this->db->get()->num_rows();

			// get result
			$val = $this->db
				->select("
					b.id_bunga_simpanan,
					b.id_member,
					b.jumlah,
					b.tanggal,
					b.keterangan
				")
				->limit($post['length'], $post['start'])
				->get()
				->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}simpanan/bunga/kelompok/form/$data->id_member/$data->id_bunga_simpanan' id='btn-detail'>
					Edit
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_bunga_simpanan' id='btn-delete'>
					Hapus
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";
			
			$baris = array(
				"no" => $no,
				"aksi" => $aksi,
				"jumlah" => number_format($data->jumlah, 2, ",", "."),
				"tanggal" => konversi_tanggal($data->tanggal, "j F Y"),
				"keterangan" => $data->keterangan
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert($this->tb, $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("x.$this->tb_id", $id)
			->get("$this->tb x");
	}
}