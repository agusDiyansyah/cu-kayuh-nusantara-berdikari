<?php echo $this->session->flashdata('msg'); ?>
<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row" style="margin-bottom: 15px;">
		<div class="col-xs-12">
			<div class="pull-left">
				<b>DETAIL MEMBER</b>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-4">
			<?php echo $detail ?>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-8">
			<form class="form" action="<?php echo $form_action ?>" method="post">
				<div class="hide">
					<?php echo $input['hide']['id'] ?>
					<?php echo $input['hide']['id_member'] ?>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<span style="font-weight: 600">FORM BUNGA SIMPANAN</span>
						<hr style="margin: 10px 0px;">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="control-label">Tanggal</label>
							<?php echo $input['tanggal'] ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="control-label">Jumlah bunga simpanan member</label>
							<?php echo $input['jumlah'] ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="control-label">Keterangan</label>
							<?php echo $input['keterangan'] ?>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div class="pull-right">
							<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
							<button type="submit" name="button" class="btn btn-success">Proses</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>