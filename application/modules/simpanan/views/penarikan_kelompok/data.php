<?php echo $this->session->flashdata('msg'); ?>

<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-left">
				<a href="<?php echo $link_back ?>" class="btn btn-default btn-add">Kembali</a>
			</div>

			<div class="pull-right">
				<a href="#" class="btn btn-default btn-tool-src">Pencarian Rinci</a>
				<a href="<?php echo $link_add ?>" class="btn btn-success btn-add">Tambah Data</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid pencarian" style="display: none;">
	<div class="row">
		<div class="col-xs-12">
			<form action="" method="POST" role="form" class="form-filter">

				<div class="hide">
					<?php echo $filter['hide']['id_member'] ?>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="">Tanggal</label>
							<div class="input-group">
								<input type="text" class="form-control date tanggal1" name="tanggal1">
								<span class="input-group-addon" id="basic-addon1">s/d</span>
								<input type="text" class="form-control date tanggal2" name="tanggal2">
							</div>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-success btn-cari">Cari</button>
				<button type="button" class="btn btn-default btn-reset">Reset</button>
			</form>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="row" style="margin-bottom: 15px;">
				<div class="col-xs-12">
					<div class="pull-left">
						<b>PENARIKAN SIMPANAN KELOMPOK / LUMBUNG</b>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php echo $detail ?>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-8">
			<table class="table table-resume table-hover table-striped" style="margin-top: -8px; margin-bottom: 25px;">
				<thead>
					<tr>
						<th colspan="3">Total</th>
					</tr>
					<tr>
						<th>Simpanan</th>
						<th>Penarikan</th>
						<th>Sisa</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="total-simpanan">0</td>
						<td class="total-penarikan">0</td>
						<td class="total-sisa">0</td>
					</tr>
				</tbody>
			</table>
			<hr>
			<table class="table table-list table-hover table-striped">
				<thead>
					<tr>
						<th width="2%">#</th>
						<th width="5%">Aksi</th>
						<th>Tanggal</th>
						<th>Jumlah</th>
						<th width="50%">Keterangan</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>