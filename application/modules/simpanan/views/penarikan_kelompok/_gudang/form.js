$(document).ready(function() {
	 $(".date").datepicker({
		 "autoclose": true,
		 "format": "yyyy-mm-dd"
	 });

	 $('.form').validate({
		ignore: [],
		errorClass: "error",
		rules:{
			id_member: { required:true },
			jumlah: { required:true },
			tanggal: { required:true },
		},
		messages:{
			id_member: { required: "Error Member" },
			tanggal: { required: "Tanggal tidak boleh kosong" },
			jumlah: { required: "Jumlah tidak boleh kosong" }
		},
		errorPlacement : function (error, element) {
            error.insertAfter(element);
        },
        highlight      : function (element, validClass) {
            $(element).parent().addClass('has-error');
        },
        unhighlight    : function (element, validClass) {
            $(element).parent().removeClass('has-error');
        }
	});
});
