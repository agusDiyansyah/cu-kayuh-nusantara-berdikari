$(document).ready(function() {
	getTotalSimpanan();

	$(".date").datepicker({
		autoclose: true,
		format: "yyyy-mm-dd"
	});

	$(".table-simpanan").DataTable({
		"ajax": {
			"url"    :"<?php echo base_url("simpanan/anggota/data") ?>",
			"method" :"POST",
			"data"   : function ( d ) {
				var filter = $(".form-filter");

				d.id_jenis_simpanan = filter.find('.id_jenis_simpanan').val();
				d.id_member = filter.find('.id_member').val();
				d.tanggal1 = filter.find('.tanggal1').val();
				d.tanggal2 = filter.find('.tanggal2').val();
			}
		},
		"dom": "<'row'<'col-sm-12'<'pull-right'<'toolbarindex'>><'pull-left'l>r<'clearfix'>>><'row'<'table-responsive'<'col-xs-12't>>><'row'<'col-sm-6'<'pull-left'<'toolbar'> i>><'col-sm-6'<'pull-right'p>><'clearfix'>>",
		"columns": [
			{"data": "no"},
			{"data": "aksi"},
			{"data": "tanggal"},
			{"data": "jumlah"},
		],

		"pageLength"  : 100,
		"deferRender" : true,
		"serverSide"  : true,
		"processing"  : false,
		"filter"      : false,
		"ordering"    : true,
		"bLengthChange": false,

		"order": [[ 0, "desc" ]],

		"columnDefs": [
			{
				"targets": 0,
				"orderable": false
			},
			{
				"targets": 1,
				"orderable": false
			},
		],

		"language": {
			"sProcessing"   : "Sedang memproses...",
			"sLengthMenu"   : "Tampilkan _MENU_ entri",
			"sZeroRecords"  : "Tidak ditemukan data",
			"sInfo"         : "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
			"sInfoEmpty"    : "Menampilkan 0 sampai 0 dari 0 entri",
			"sInfoFiltered" : "(difilter dari _MAX_ entri keseluruhan)",
			"sInfoPostFix"  : "",
			"sUrl"          : "",
			"oPaginate"     : {
				"sFirst"        : "Pertama",
				"sPrevious"     : "<<",
				"sNext"         : ">>",
				"sLast"         : "Terakhir"
			}
		},
	});

	$(".table-simpanan").on("click", "#btn-delete", function(event) {
		var id = $(this).data("id");
		
		swal({
			title: "Apakah anda yakin",
			text: "Anda akan menghapus data ini",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#f4516c",
			cencelButtonColor: "#eaeaea",
			confirmButtonText: "Hapus Data",
			cancelButtonText: "Batalkan",
			showLoaderOnConfirm: true,
			closeOnConfirm: true,
			closeOnCancel: true
		},
		function (isConfirm) {
			if (isConfirm) {
				$.ajax({
					url: "<?php echo base_url("simpanan/anggota/delete_proses") ?>",
					type: "post",
					dataType: "json",
					data: { id: id },
					success: function (json) {
						if (json.stat) {
							notif(true, "Data berhasil di hapus");
							getTotalSimpanan();
							refreshTable();
						} else {
							notif(false, "Data gagal di hapus");
						}
					}
				});
			}
		});
	});

	$(".form-filter").validate({
		submitHandler : function(form) {
			refreshTable();
			return false;
		}
	});

	$(".form-filter").on('click', '.btn-reset', function(event) {
		event.preventDefault();

		$(".form-filter").find(".date").val("");

		refreshTable();
	});
});

function refreshTable () {
	var dtable = $(".table").DataTable();
	dtable.draw();
}

function getTotalSimpanan () {
	$(".jumlah_simpanan").show(function() {
		var id_jenis_simpanan = $(".id_jenis_simpanan").val();
		var id_member = $(".id_member").val();

		$.ajax({
			url: '<?php echo base_url('simpanan/anggota/getTotalSimpanan') ?>',
			type: 'post',
			dataType: 'json',
			data: {
				id_jenis_simpanan: id_jenis_simpanan,
				id_member: id_member,
			},
			success: function (json) {
				$(".jumlah_simpanan").html(json.jumlah_simpanan);
			}
		});
	});
}