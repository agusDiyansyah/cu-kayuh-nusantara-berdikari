<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bunga_kelompok extends Admin_controller {
	private $stat = false;
	private $title = "Bunga Simpanan Kelompok";
	private $module = "simpanan/bunga/kelompok";
	private $module_view = "simpanan/bunga_kelompok";
	private $id_member;
	
	// DEFAULT
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_bunga_simpanan_kelompok", "M_app");
		}
	// END OF DEFAULT

	//  INDEX
		public function index ($id_member = 0) {

			$this->cekParsingId($id_member);
			// datatables
			$this->output->js('assets/themes/admin/vendors/datatables/jquery.dataTables.js');
			$this->output->js('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js');
			$this->output->css('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');

			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			// custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/data.js");
			
			$this->load->module("member/kelompok");
			$detail = $this->kelompok->widget_detail_kelompok($this->id_member);
			$this->output->script_foot_string("
				$('.btn-edit-kelompok').remove();
			");

			$data = array(
				"title" => $this->title,
				"link_add" => base_url("$this->module/form/$this->id_member"),
				"link_back" => base_url("member/kelompok/detail/$this->id_member"),

				"detail" => $detail,
				
				"filter" => array(
					"hide" => array(
						"id_member" => form_input(array(
							"name" => "id_member",
							"class" => "form-control id_member",
							"type" => "text",
							"value" => $this->id_member,
						)),
					),
					"kategori" => form_input(array(
						"name" => "kategori",
						"class" => "form-control kategori",
						"type" => "text",
					)),
				)
			);
			
			$this->load->view("$this->module_view/data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($_POST);
			}
			return;
		}
	//  END OF INDEX
		
	// FORM
		public function form ($id_member = 0, $id = 0) {
			$this->cekParsingId($id_member);

			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			// custom
			$this->output->script_foot("$this->module_view/_gudang/form.js");
			
			if ($id > 0) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}

		private function add () {
			$data = $this->_formInputData();
			
			$this->load->view("$this->module_view/form", $data);
		}
		
		private function edit ($id) {
			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"id_member" => $this->id_member,
					"jumlah" => $val->jumlah,
					"tanggal" => $val->tanggal,
					"keterangan" => $val->keterangan,
				));
				
				$this->load->view("$this->module_view/form", $data);
			} else {
				show_404();
			}
		}
	// END OF FORM
	
	// PROSES	
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			$id_member = $this->input->post("id_member");
			$this->cekParsingId($id_member);
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}

		private function add_proses () {
			$back = "$this->module/form/$this->id_member";
			$submsg = "Data gagal di proses";
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "$this->module/$this->id_member";
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$sql = $this->M_app->cekId($id, "id_bunga_simpanan");
			
			if ($sql->num_rows() > 0) {
				$back = "$this->module/form";
				$submsg = "Data gagal di proses";
				
				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$data = $this->_formPostInputData();
					$up = $this->M_app->edit($data, $id);
					
					if ($up) {
						$this->stat = true;
						$back = "$this->module/$this->id_member";
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_bunga_simpanan");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}
	// END OF PROSES

	// OTHER
		public function getResume () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post()) {
				$this->cekParsingId($this->input->post("id_member"));

				$sql = $this->db
					->select("SUM(jumlah) jumlah")
					->from("simpanan_bunga b")
					->where("id_member", $this->id_member)
					->where("is_delete", 0)
					->get();

				$jumlah = "0,00";
				if ($sql->num_rows() > 0) {
					$val = $sql->row();
					$jumlah = number_format($val->jumlah, 2, ",", ".");
				}

				echo json_encode(array(
					"jumlah" => $jumlah
				));
			} else {
				show_404();
				die();
			}
		}

		private function cekParsingId ($id_member) {
			if (
				empty($id_member) OR
				$id_member == 0
			) {
				show_404();
				die();
			} else {
				$this->id_member = $id_member;
				return;
			}
		}
		
		private function _formInputData ($data = array()) {
			$this->load->module("member/kelompok");
			$detail = $this->kelompok->widget_detail_kelompok($this->id_member);
			$this->output->script_foot_string("
				$('.btn-edit-kelompok').remove();
			");

			return array(
				"title" => $this->title,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("$this->module/$this->id_member"),

				"detail" => $detail,
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						)),
						"id_member" => form_input(array(
							"name" => "id_member",
							"class" => "id_member",
							"type" => "text",
							"value" => $this->id_member,
						))
					),
					
					"jumlah" => form_input(array(
						"name" => "jumlah",
						"class" => "form-control jumlah",
						"type" => "text",
						"value" => @$data['jumlah'],
					)),
					"tanggal" => form_input(array(
						"name" => "tanggal",
						"class" => "form-control date tanggal",
						"type" => "text",
						"value" => @$data['tanggal'],
					)),
					"keterangan" => form_textarea(array(
						"name" => "keterangan",
						"class" => "form-control keterangan",
						"rows" => 5,
						"value" => @$data['keterangan'],
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			$data = array(
				"id_member" => $this->input->post("id_member"),
				"jumlah" => $this->input->post("jumlah"),
				"tanggal" => $this->input->post("tanggal"),
				"keterangan" => $this->input->post("keterangan"),
			);
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("tanggal")) {
				$err .= form_error("tanggal");
			}
			if (form_error("jumlah")) {
				$err .= form_error("jumlah");
			}
			if (form_error("id_member")) {
				$err .= form_error("id_member");
			}
			
			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "tanggal",
					"label" => "Tanggal",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "jumlah",
					"label" => "Jumlah bunga simpanan",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "id_member",
					"label" => "ERROR MEMBER",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
	// END OF OTHER
}