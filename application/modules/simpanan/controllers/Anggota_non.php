<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota_non extends Admin_controller {
	private $stat = false;
	private $title = "Simpanan Non-Anggota";
	private $module = "simpanan/anggota_non";
	private $module_view = "simpanan/anggota_non";
	private $jenis_simpanan = array(
		4 => "SIAR",
		5 => "TUNAS",
		6 => "PUKAT",
		7 => "UMAT",
		8 => "SINGKAYA",
	);
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_simpanan_anggota_non", "M_app");
		}
		
		public function transaksi ($id_jenis_simpanan = 0, $id_member = 0) {
			$this->cekParsingId($id_jenis_simpanan, $id_member);
			$this->load->module("member");
			$detail = $this->member->widget_detail_member($id_member);

			// datatables
			$this->output->js('assets/themes/admin/vendors/datatables/jquery.dataTables.js');
			$this->output->js('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js');
			$this->output->css('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');

			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/data.js");
			
			$data = array(
				"title" => $this->title,
				"link_add" => base_url("$this->module/form/$id_jenis_simpanan/$id_member"),
				"link_back" => base_url("member/detail/$id_member"),
				"detail" => $detail,
				"jenis_simpanan" => $this->jenis_simpanan[$id_jenis_simpanan],
				
				"filter" => array(
					"hide" => array(
						"id_jenis_simpanan" => form_input(array(
							"name" => "id_jenis_simpanan",
							"class" => "id_jenis_simpanan",
							"value" => $id_jenis_simpanan,
						)),
						"id_member" => form_input(array(
							"name" => "id_member",
							"class" => "id_member",
							"value" => $id_member,
						)),
					),
					
					"kategori" => form_input(array(
						"name" => "kategori",
						"class" => "form-control kategori",
						"type" => "text",
					)),
				)
			);
			
			$this->load->view("$this->module_view/data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($this->input->post());
			}
			return;
		}

		public function getTotalSimpanan () {
			$this->output->unset_template();

			if ($this->input->is_ajax_request() AND $this->input->post()) {
				$id_jenis_simpanan = $this->input->post("id_jenis_simpanan");
				$id_member = $this->input->post("id_member");

				$sql = $this->db
					->where("id_jenis_simpanan", $id_jenis_simpanan)
					->where("id_member", $id_member)
					->where("is_delete", 0)
					->select("SUM(jumlah) jumlah")
					->get("simpanan")
					->row();

				$jumlah_simpanan = (empty($sql->jumlah)) ? 0 : $sql->jumlah;

				echo json_encode(array(
					"jumlah_simpanan" => number_format($jumlah_simpanan, 0, ",", ".")
				));
			} else {
				show_404();
			}	
		}
		
		public function form ($id_jenis_simpanan = 0, $id_member = 0, $id = 0) {
			$this->cekParsingId($id_jenis_simpanan, $id_member);

			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

			// validate
			$this->output->js("assets/themes/admin/vendors/jquery-validate/jquery.validate.js");
			
			//custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/form.js");

			$detail = array(
				"id_member" => $id_member,
				"id_jenis_simpanan" => $id_jenis_simpanan
			);

			if ($id > 0) {
				$this->edit($id, $detail);
			} else {
				$this->add($detail);
			}
		}
		
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_simpanan");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}
	// PRIVATE
		private function add ($detail) {
			$this->cekParsingId($detail['id_jenis_simpanan'], $detail['id_member']);

			$data = $this->_formInputData(array(
				"id_jenis_simpanan" => $detail['id_jenis_simpanan'],
				"id_member" => $detail['id_member'],
			));
			
			$this->load->view("$this->module_view/form", $data);
		}
		
		private function edit ($id, $detail) {
			$this->cekParsingId($detail['id_jenis_simpanan'], $detail['id_member']);

			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"id_jenis_simpanan" => $detail['id_jenis_simpanan'],
					"id_member" => $detail['id_member'],

					"jumlah" => $val->jumlah,
					"tanggal" => $val->tanggal,
					"keterangan" => $val->keterangan,
				));
				
				$this->load->view("$this->module_view/form", $data);
			} else {
				show_404();
			}
		}
		
		private function add_proses ($data = array()) {
			$id_jenis_simpanan = $this->input->post("id_jenis_simpanan");
			$id_member = $this->input->post("id_member");

			$this->cekParsingId($id_jenis_simpanan, $id_member);

			$back = "$this->module/form/$data[id_jenis_simpanan]/$data[id_member]";
			$submsg = "Data gagal di proses";

			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();

				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "$this->module/transaksi/$data[id_jenis_simpanan]/$data[id_member]";
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$id_jenis_simpanan = $this->input->post("id_jenis_simpanan");
			$id_member = $this->input->post("id_member");
			
			$this->cekParsingId($id_jenis_simpanan, $id_member);

			$sql = $this->M_app->cekId($id, "id_simpanan");
			
			if ($sql->num_rows() > 0) {
				$back = "$this->module/form/$data[id_jenis_simpanan]/$data[id_member]";
				$submsg = "Data gagal di proses";
				
				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$data = $this->_formPostInputData();
					$up = $this->M_app->edit($data, $id);
					
					if ($up) {
						$this->stat = true;
						$back = "$this->module/transaksi/$data[id_jenis_simpanan]/$data[id_member]";
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}

		private function cekParsingId ($id_jenis_simpanan, $id_member) {
			if (
				empty($id_jenis_simpanan) OR
				empty($id_member)
			) {
				show_404();
				die();
			} else {
				return;
			}
		}
		
		private function _formInputData ($data = array()) {
			$this->load->module("member");

			return array(
				"title" => $this->title,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("$this->module/transaksi/$data[id_jenis_simpanan]/$data[id_member]"),

				"detail" => $this->member->widget_detail_member($data['id_member']),
				"jenis_simpanan" => $this->jenis_simpanan[$data['id_jenis_simpanan']],
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						)),
						"id_jenis_simpanan" => form_input(array(
							"name" => "id_jenis_simpanan",
							"class" => "id_jenis_simpanan",
							"type" => "text",
							"value" => @$data['id_jenis_simpanan'],
						)),
						"id_member" => form_input(array(
							"name" => "id_member",
							"class" => "id_member",
							"type" => "text",
							"value" => @$data['id_member'],
						)),
					),
					
					"tanggal" => form_input(array(
						"name" => "tanggal",
						"class" => "form-control date tanggal",
						"type" => "text",
						"value" => @$data['tanggal'],
					)),
					"jumlah" => form_input(array(
						"name" => "jumlah",
						"class" => "form-control jumlah",
						"type" => "number",
						"value" => @$data['jumlah'],
					)),
					"keterangan" => form_textarea(array(
						"name" => "keterangan",
						"class" => "form-control keterangan",
						"value" => @$data['keterangan'],
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			$data = array(
				"id_jenis_simpanan" => $this->input->post("id_jenis_simpanan"),
				"id_member" => $this->input->post("id_member"),
				"jumlah" => $this->input->post("jumlah"),
				"tanggal" => $this->input->post("tanggal"),
				"keterangan" => $this->input->post("keterangan"),
			);
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("id_jenis_simpanan")) {
				$err .= form_error("id_jenis_simpanan");
			}
			if (form_error("id_member")) {
				$err .= form_error("id_member");
			}
			if (form_error("jumlah")) {
				$err .= form_error("jumlah");
			}
			if (form_error("tanggal")) {
				$err .= form_error("tanggal");
			}

			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "tanggal",
					"label" => "Tanggal simpanan",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "jumlah",
					"label" => "Jumlah dana yang disimpan",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),

				array(
					"field" => "id_jenis_simpanan",
					"label" => "ERROR JENIS SIMPANAN",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "id_member",
					"label" => "ERROR MEMBER",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
}