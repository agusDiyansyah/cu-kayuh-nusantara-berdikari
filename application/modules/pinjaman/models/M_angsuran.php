<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_angsuran extends CI_Model {
	private $tb = "angsuran";
	private $tb_id = "id_angsuran";

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = false) {
		$order = $post['order'][0];

		$this->db->start_cache();
			$this->db->from("angsuran a");
			// filter
			$this->db
				->join("pinjaman p", "p.id_pinjaman = a.id_pinjaman")
				->where("p.id_pinjaman", $post["id_pinjaman"])
				->where("a.is_delete", 0)
				->where("p.is_delete", 0);

			$orderColumn = array(
				2 => "a.tanggal_angsuran",
				3 => "a.jumlah_angsuran",
				4 => "a.jumlah_bunga",
				5 => "a.jumlah_denda",
			);
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by('a.id_angsuran', $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

		$this->db->stop_cache();

		// get num rows
		$rowCount = $this->db
			->select('a.id_angsuran')
			->get()
			->num_rows();

		// get result
		$val = $this->db
			->select("a.tanggal_angsuran, a.jumlah_angsuran, a.jumlah_bunga, a.jumlah_denda, a.keterangan, a.id_angsuran")
			->limit($post['length'], $post['start'])
			->get()
			->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}pinjaman/angsuran/form/$post[id_pinjaman]/$post[id_member]/$data->id_angsuran' id='btn-detail'>
					Edit
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_angsuran' id='btn-delete'>
					Hapus
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";
			
			$baris = array(
				"no" => $no,
				"aksi" => $aksi,
				"tanggal_angsuran" => konversi_tanggal($data->tanggal_angsuran, "j F Y"),
				"jumlah_angsuran" => number_format($data->jumlah_angsuran, 0, ",", "."),
				"jumlah_bunga" => number_format($data->jumlah_bunga, 0, ",", "."),
				"jumlah_denda" => number_format($data->jumlah_denda, 0, ",", "."),
				"keterangan" => $data->keterangan
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert($this->tb, $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where($this->tb_id, $id)
			->get($this->tb);
	}
}