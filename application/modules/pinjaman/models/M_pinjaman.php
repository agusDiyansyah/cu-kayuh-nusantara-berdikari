<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pinjaman extends CI_Model {
	private $tb = "pinjaman";
	private $tb_id = "id_pinjaman";

	public function __construct() {
		parent::__construct();
	}
	
	public function add ($data) {
		$this->db->insert($this->tb, $data);
		
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("p.$this->tb_id", $id)
			->get("$this->tb p");
	}
}