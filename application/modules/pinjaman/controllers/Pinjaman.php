<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pinjaman extends Admin_controller {
	private $stat = false;
	private $title = "Pinjaman";
	private $module = "pinjaman";
	private $module_view = "pinjaman/pinjaman";
	private $id_member;
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_pinjaman", "M_app");
		}
		
		public function form ($id_member = 0, $id = 0) {
			$this->cekParsingId($id_member);

			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");
			
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

			// validate
			$this->output->js("assets/themes/admin/vendors/jquery-validate/jquery.validate.js");
			
			// custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/form.js");
			
			if ($id > 0) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}
		
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			$id_member = $this->input->post("id_member");

			$this->cekParsingId($id_member);
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_pinjaman");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}
	// PRIVATE
		private function add () {
			$data = $this->_formInputData();
			
			$this->load->view("$this->module_view/form", $data);
		}
		
		private function edit ($id) {
			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"id_produk_pinjaman" => $val->id_produk_pinjaman,
					"no_pinjaman" => $val->no_pinjaman,
					"jumlah" => $val->jumlah,
					"bunga" => $val->bunga,
					"jangka_waktu" => $val->jangka_waktu,
					"tanggal_jatuhtempo" => $val->tanggal_jatuhtempo,
					"tanggal_meminjam" => $val->tanggal_meminjam,
					"biaya_administrasi" => $val->biaya_administrasi,
					"keterangan" => $val->keterangan,
				));
				
				$this->load->view("$this->module_view/form", $data);
			} else {
				show_404();
			}
		}
		
		private function add_proses () {
			$back = "$this->module/form/$this->id_member";
			$submsg = "Data gagal di proses";
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "member/detail/$this->id_member";
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$sql = $this->M_app->cekId($id, "id_member");
			
			if ($sql->num_rows() > 0) {
				$back = "$this->module/form/$this->id_member";
				$submsg = "Data gagal di proses";
				
				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$data = $this->_formPostInputData();
					$up = $this->M_app->edit($data, $id);
					
					if ($up) {
						$this->stat = true;
						$back = "member/detail/$this->id_member";
						
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}

		private function cekParsingId ($id_member = 0) {
			if (
				empty($id_member) OR
				$id_member == 0
			) {
				show_404();
				die();
			} else {
				$this->id_member = $id_member;
				return;
			}
		}
		
		private function _formInputData ($data = array()) {
			$this->load->module("member");
			$detail = $this->member->widget_detail_member($this->id_member);

			$sql_produk_pinjaman = $this->db
				->where("id_kategori_produk_pinjaman", 1)
				->get("pinjaman_produk");

			$produk_pinjaman = array("" => "");
			foreach ($sql_produk_pinjaman->result() as $p) {
				$produk_pinjaman += array(
					$p->id_produk_pinjaman => $p->nama_produk
				);
			}

			return array(
				"title" => $this->title,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("member/detail/$this->id_member"),
				"detail" => $detail,
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						)),
						"id_member" => form_input(array(
							"name" => "id_member",
							"class" => "id_member",
							"type" => "text",
							"value" => @$this->id_member,
						)),
					),
					
					"no_pinjaman" => form_input(array(
						"name" => "no_pinjaman",
						"class" => "form-control no_pinjaman",
						"type" => "text",
						"value" => @$data['no_pinjaman'],
					)),
					"id_produk_pinjaman" => form_select(array(
						"config" => array(
							"name" => "id_produk_pinjaman",
							"class" => "form-control id_produk_pinjaman s2",
						),
						"list" => $produk_pinjaman,
						"selected" => @$data['id_produk_pinjaman'],
					)),
					"bunga" => form_input(array(
						"name" => "bunga",
						"class" => "form-control bunga",
						"type" => "number",
						"value" => @$data['bunga'],
					)),
					"jumlah" => form_input(array(
						"name" => "jumlah",
						"class" => "form-control jumlah",
						"type" => "number",
						"value" => @$data['jumlah'],
					)),
					"jangka_waktu" => form_input(array(
						"name" => "jangka_waktu",
						"class" => "form-control jangka_waktu",
						"type" => "number",
						"value" => @$data['jangka_waktu'],
					)),
					"tanggal_jatuhtempo" => form_input(array(
						"name" => "tanggal_jatuhtempo",
						"class" => "form-control tanggal_jatuhtempo",
						"type" => "number",
						"value" => @$data['tanggal_jatuhtempo'],
					)),
					"tanggal_meminjam" => form_input(array(
						"name" => "tanggal_meminjam",
						"class" => "form-control date tanggal_meminjam",
						"type" => "text",
						"value" => @$data['tanggal_meminjam'],
					)),
					"biaya_administrasi" => form_input(array(
						"name" => "biaya_administrasi",
						"class" => "form-control biaya_administrasi",
						"type" => "number",
						"value" => @$data['biaya_administrasi'],
					)),
					"keterangan" => form_textarea(array(
						"name" => "keterangan",
						"class" => "form-control keterangan",
						"value" => @$data['keterangan'],
						"rows" => 5
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			// header('Content-Type: Application/json');
			// echo json_encode($this->input->post()); die();
			
			$is_anggota = $this->input->post('is_anggota');
			
			$data = array(
				"id_member" => $this->input->post("id_member"),
				"id_produk_pinjaman" => $this->input->post("id_produk_pinjaman"),
				"no_pinjaman" => $this->input->post("no_pinjaman"),
				"jumlah" => $this->input->post("jumlah"),
				"bunga" => $this->input->post("bunga"),
				"jangka_waktu" => $this->input->post("jangka_waktu"),
				"tanggal_jatuhtempo" => $this->input->post("tanggal_jatuhtempo"),
				"tanggal_meminjam" => $this->input->post("tanggal_meminjam"),
				"biaya_administrasi" => $this->input->post("biaya_administrasi"),
				"keterangan" => $this->input->post("keterangan"),
			);
			
			// header('Content-Type: Application/json');
			// echo json_encode($data); die();
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("no_pinjaman")) {
				$err .= form_error("no_pinjaman");
			}
			if (form_error("jumlah")) {
				$err .= form_error("jumlah");
			}
			if (form_error("bunga")) {
				$err .= form_error("bunga");
			}
			if (form_error("biaya_administrasi")) {
				$err .= form_error("biaya_administrasi");
			}
			if (form_error("jangka_waktu")) {
				$err .= form_error("jangka_waktu");
			}
			if (form_error("tanggal_jatuhtempo")) {
				$err .= form_error("tanggal_jatuhtempo");
			}
			if (form_error("tanggal_meminjam")) {
				$err .= form_error("tanggal_meminjam");
			}
			
			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "no_pinjaman",
					"label" => "Nomor pinjaman tidak boleh kosong",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "jumlah",
					"label" => "Jumlah dana yang dipinjam tidak boleh kosong",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "bunga",
					"label" => "Bunga pinjaman tidak boleh kosong",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "jangka_waktu",
					"label" => "Jangka waktu tidak boleh kosong",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "tanggal_jatuhtempo",
					"label" => "Tanggal jatuh tempo tidak boleh kosong",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "tanggal_meminjam",
					"label" => "Tanggal meminjam tidak boleh kosong",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
}