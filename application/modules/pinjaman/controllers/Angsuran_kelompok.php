<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Angsuran_kelompok extends Admin_controller {
	private $stat = false;
	private $title = "Angsuran pinjaman dana";
	private $module = "pinjaman/angsuran/kelompok";
	private $module_view = "pinjaman/angsuran_kelompok";
	private $id_pinjaman;
	private $id_member;
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_angsuran_kelompok", "M_app");
		}
		
		public function transaksi ($id_pinjaman = 0) {
			$this->cekParsingId($id_pinjaman);

			$this->load->module("member/kelompok");
			$detail = $this->kelompok->widget_detail_kelompok($this->id_member);
			$this->output->script_foot_string("
				$('.btn-edit-kelompok').remove();
			");

			// datatables
			$this->output->js('assets/themes/admin/vendors/datatables/jquery.dataTables.js');
			$this->output->js('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js');
			$this->output->css('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');
			
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/data.js");
			
			$data = array(
				"title" => $this->title,
				"link_add" => base_url("$this->module/form/$id_pinjaman"),
				"link_back" => base_url("member/kelompok/detail/$this->id_member"),
				"detail" => $detail,
				"produk_pinjaman" => $this->getProdukPinjaman(),
				
				"filter" => array(
					"hide" => array(
						"id_pinjaman" => form_input(array(
							"name" => "id_pinjaman",
							"class" => "id_pinjaman",
							"value" => $id_pinjaman,
						))
					),
					
					"kategori" => form_input(array(
						"name" => "kategori",
						"class" => "form-control kategori",
						"type" => "text",
					)),
				)
			);
			
			$this->load->view("$this->module_view/data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($this->input->post());
			}
			return;
		}
		
		public function form ($id_pinjaman = 0, $id = 0) {
			$this->cekParsingId($id_pinjaman);

			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

			// validate
			$this->output->js("assets/themes/admin/vendors/jquery-validate/jquery.validate.js");
			
			//custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/form.js");

			if ($id > 0) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}
		
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			$id_pinjaman = $this->input->post("id_pinjaman");
			$id_member = $this->input->post("id_member");

			$this->cekParsingId($id_pinjaman, $id_member);
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_pinjaman");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}

		public function getResume () {
			$this->output->unset_template();

			if ($this->input->is_ajax_request() AND $this->input->post()) {
				$id_pinjaman = $this->input->post("id_pinjaman");

				$sql = $this->db
					->select("
						SUM(jumlah_angsuran) jumlah_angsuran,
						SUM(jumlah_bunga) jumlah_bunga,
						SUM(jumlah_denda) jumlah_denda
					")
					->join("angsuran a", "p.id_pinjaman = a.id_pinjaman")
					->where("p.id_pinjaman", $id_pinjaman)
					->where("p.is_delete", 0)
					->where("a.is_delete", 0)
					->get("pinjaman p", 1)
					->row();

				$angsuran = $sql->jumlah_angsuran;

				$jumlah_angsuran = (empty($sql->jumlah_angsuran)) ? 0 : $sql->jumlah_angsuran;
				$jumlah_bunga = (empty($sql->jumlah_bunga)) ? 0 : $sql->jumlah_bunga;
				$jumlah_denda = (empty($sql->jumlah_denda)) ? 0 : $sql->jumlah_denda;

				$sql = $this->db
					->select("jumlah, bunga, tanggal_jatuhtempo, tanggal_meminjam")
					->where("id_pinjaman", $id_pinjaman)
					->get("pinjaman", 1)
					->row();

				$jumlah_sisa = (empty($sql->jumlah - $angsuran)) ? 0 : ($sql->jumlah - $angsuran);
				$jumlah_pinjaman = (empty($sql->jumlah)) ? 0 : $sql->jumlah;
				$total_bunga = (empty($sql->bunga)) ? 0 : $sql->bunga;

				echo json_encode(array(
					"angsuran" => number_format($jumlah_angsuran, 0, ",", "."),
					"bunga" => number_format($jumlah_bunga, 0, ",", "."),
					"denda" => number_format($jumlah_denda, 0, ",", "."),
					"sisa" => number_format($jumlah_sisa, 0, ",", "."),

					"pinjaman" => number_format($jumlah_pinjaman, 0, ",", "."),
					"total_bunga" => number_format($total_bunga, 0, ",", "."),
					"tanggal_jatuhtempo" => $sql->tanggal_jatuhtempo,
					"tanggal_meminjam" => konversi_tanggal($sql->tanggal_meminjam, "j F Y"),
				));
			} else {
				show_404();
			}	
		}
	// PRIVATE
		private function add () {
			$data = $this->_formInputData();
			
			$this->load->view("$this->module_view/form", $data);
		}
		
		private function edit ($id) {
			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"jumlah_angsuran" => $val->jumlah_angsuran,
					"jumlah_bunga" => $val->jumlah_bunga,
					"jumlah_denda" => $val->jumlah_denda,
					"tanggal_angsuran" => $val->tanggal_angsuran,
					"keterangan" => $val->keterangan,
				));
				
				$this->load->view("$this->module_view/form", $data);
			} else {
				show_404();
			}
		}
		
		private function add_proses ($data = array()) {
			$id_pinjaman = $this->input->post("id_pinjaman");

			$this->cekParsingId($id_pinjaman);

			$back = "$this->module/form/$id_pinjaman";
			$submsg = "Data gagal di proses";

			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "$this->module/$id_pinjaman";
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$id_pinjaman = $this->input->post("id_pinjaman");
			
			$this->cekParsingId($id_pinjaman);

			$sql = $this->M_app->cekId($id, "id_pinjaman");

			if ($sql->num_rows() > 0) {
				$back = "$this->module/form/$id_pinjaman/$id";
				$submsg = "Data gagal di proses";

				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$data = $this->_formPostInputData();
					$up = $this->M_app->edit($data, $id);
					
					if ($up) {
						$this->stat = true;
						$back = "$this->module/$id_pinjaman";
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}

		private function cekParsingId ($id_pinjaman) {
			if (
				empty($id_pinjaman)
			) {
				show_404();
				die();
			} else {
				$sql = $this->db
					->select("m.id_member")
					->from("pinjaman p")
					->join("member m", "m.id_member = p.id_member")
					->where("p.id_pinjaman", $id_pinjaman)
					->where("p.is_delete", 0)
					->get();

				if ($sql->num_rows() > 0) {
					$val = $sql->row();
					$this->id_pinjaman = $id_pinjaman;
					$this->id_member = $val->id_member;
				} else {
					show_404();
					die();
				}
				return;
			}
		}

		private function getProdukPinjaman () {
			if (!empty($this->id_pinjaman)) {
				$sql_cek_produk = $this->db
					->select("nama_produk")
					->join("pinjaman_produk pk", "pk.id_produk_pinjaman = p.id_produk_pinjaman")
					->where("p.id_pinjaman", $this->id_pinjaman)
					->get("pinjaman p", 1)
					->row();
				return $sql_cek_produk->nama_produk;
			} else {
				show_404();
			}
		}
		
		private function _formInputData ($data = array()) {
			$this->load->module("member/kelompok");
			$detail = $this->kelompok->widget_detail_kelompok($this->id_member);
			$this->output->script_foot_string("
				$('.btn-edit-kelompok').remove();
			");

			return array(
				"title" => $this->title,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("$this->module/transaksi/$this->id_pinjaman/$this->id_member"),

				"detail" => $detail,
				"produk_pinjaman" => $this->getProdukPinjaman(),
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						)),
						"id_pinjaman" => form_input(array(
							"name" => "id_pinjaman",
							"class" => "id_pinjaman",
							"type" => "text",
							"value" => @$this->id_pinjaman,
						))
					),
					
					"tanggal_angsuran" => form_input(array(
						"name" => "tanggal_angsuran",
						"class" => "form-control date tanggal_angsuran",
						"type" => "text",
						"value" => @$data['tanggal_angsuran'],
					)),
					"jumlah_angsuran" => form_input(array(
						"name" => "jumlah_angsuran",
						"class" => "form-control jumlah_angsuran",
						"type" => "number",
						"value" => @$data['jumlah_angsuran'],
					)),
					"jumlah_bunga" => form_input(array(
						"name" => "jumlah_bunga",
						"class" => "form-control jumlah_bunga",
						"type" => "number",
						"value" => @$data['jumlah_bunga'],
					)),
					"jumlah_denda" => form_input(array(
						"name" => "jumlah_denda",
						"class" => "form-control jumlah_denda",
						"type" => "number",
						"value" => @$data['jumlah_denda'],
					)),
					"keterangan" => form_textarea(array(
						"name" => "keterangan",
						"class" => "form-control keterangan",
						"value" => @$data['keterangan'],
						"rows" => 5
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			$data = array(
				"id_pinjaman" => $this->input->post("id_pinjaman"),
				"tanggal_angsuran" => $this->input->post("tanggal_angsuran"),
				"jumlah_angsuran" => $this->input->post("jumlah_angsuran"),
				"jumlah_bunga" => $this->input->post("jumlah_bunga"),
				"jumlah_denda" => $this->input->post("jumlah_denda"),
				"keterangan" => $this->input->post("keterangan"),
			);
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("id_pinjaman")) {
				$err .= form_error("id_pinjaman");
			}
			if (form_error("tanggal_angsuran")) {
				$err .= form_error("tanggal_angsuran");
			}
			if (form_error("jumlah_angsuran")) {
				$err .= form_error("jumlah_angsuran");
			}
			if (form_error("jumlah_bunga")) {
				$err .= form_error("jumlah_bunga");
			}
			if (form_error("jumlah_denda")) {
				$err .= form_error("jumlah_denda");
			}

			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "tanggal_angsuran",
					"label" => "Tanggal angsuran",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "jumlah_angsuran",
					"label" => "Jumlah angsuran",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),

				array(
					"field" => "id_pinjaman",
					"label" => "ERROR PINJAMAN",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
}