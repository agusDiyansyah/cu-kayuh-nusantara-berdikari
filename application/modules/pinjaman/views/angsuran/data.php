<?php echo $this->session->flashdata('msg'); ?>

<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-left">
				<a href="<?php echo $link_back ?>" class="btn btn-default btn-add">Kembali</a>
			</div>

			<div class="pull-right">
				<a href="#" class="btn btn-default btn-tool-src">Pencarian Rinci</a>
				<a href="<?php echo $link_add ?>" class="btn btn-success btn-add">Tambah Data</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid pencarian" style="display: none;">
	<div class="row">
		<div class="col-xs-12">
			<form action="" method="POST" role="form" class="form-filter">

				<div class="hide">
					<?php echo $filter['hide']['id_pinjaman'] ?>
					<?php echo $filter['hide']['id_member'] ?>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="">label</label>
							<input type="text" class="form-control" id="" placeholder="Input field">
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-success btn-cari">Cari</button>
				<button type="button" class="btn btn-default btn-reset">Reset</button>
			</form>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="row" style="margin-bottom: 15px;">
				<div class="col-xs-12">
					<div class="pull-left">
						<b>ANGSURAN PINJAMAN / <?php echo strtoupper($produk_pinjaman) ?></b>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php echo $detail ?>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-8">
			<table class="table table-resume table-hover table-striped" style="margin-top: -8px; margin-bottom: 25px;">
				<thead>
					<tr>
						<th colspan="4">Total</th>
					</tr>
					<tr>
						<th>Pinjaman</th>
						<th>Angsuran</th>
						<th>Sisa</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="total-pinjaman">0</td>
						<td class="total-angsuran">0</td>
						<td class="total-sisa">0</td>
					</tr>
				</tbody>
			</table>
			<!-- <br> -->
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Total Bunga Pinjaman</th>
						<th>Bunga Angsuran</th>
						<th>Denda</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="total-bunga">0</td>
						<td class="bunga">0</td>
						<td class="denda">0</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Jatuh tempo setiap tanggal</th>
						<td class="jatuh-tempo"></td>
						<th>Tanggal peminjaman dana</th>
						<td class="tanggal-meminjam"></td>
					</tr>
				</thead>
			</table>

			<hr>

			<table class="table table-list table-hover table-striped">
				<thead>
					<tr>
						<th width="2%">#</th>
						<th width="5%">Aksi</th>
						<th>Tanggal</th>
						<th>Angsuran</th>
						<th>Bunga</th>
						<th>Denda</th>
						<th>Keterangan</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>