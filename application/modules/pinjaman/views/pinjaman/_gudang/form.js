$(document).ready(function() {
	$('.form').validate({
		ignore: [],
		errorClass: "error",
		rules:{
			id_member: { required:true },
			no_pinjaman: { required:true },
			jumlah: { required:true },
			bunga: { required:true },
			jangka_waktu: { required:true },
			tanggal_jatuhtempo: { required:true },
			tanggal_meminjam: { required:true },
		},
		messages:{
			id_member: { required: "Error Member" },
			no_pinjaman: { required: "Nomor pinjaman tidak boleh kosong" },
			jumlah: { required: "Jumlah dana yang dipinjaman  tidak boleh kosong" },
			bunga: { required: "Bunga tidak boleh kosong" },
			jangka_waktu: { required: "Jangka waktu pinjaman dana tidak boleh kosong" },
			tanggal_jatuhtempo: { required: "Tanggal jatuh tempo tidak boleh kosong" },
			tanggal_meminjam: { required: "Tanggal pinjaman tidak boleh kosong" },
		},
		errorPlacement : function (error, element) {
            error.insertAfter(element);
        },
        highlight      : function (element, validClass) {
            $(element).parent().addClass('has-error');
        },
        unhighlight    : function (element, validClass) {
            $(element).parent().removeClass('has-error');
        }
	});

	 $(".date").datepicker({
		 "autoclose": true,
		 "format": "yyyy-mm-dd"
	 });
	 
	 $(".s2").select2({
		 wudth: "100%"
	 });
});
