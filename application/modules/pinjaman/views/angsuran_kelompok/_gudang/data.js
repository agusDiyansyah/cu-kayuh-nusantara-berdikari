$(document).ready(function() {
	getResume();

	$(".table-list").DataTable({
		"ajax": {
			"url"    :"<?php echo base_url("pinjaman/angsuran_kelompok/data") ?>",
			"method" :"POST",
			"data"   : function ( d ) {
				var filter = $(".form-filter");

				d.id_pinjaman = filter.find('.id_pinjaman').val();
			}
		},
		"dom": "<'row'<'col-sm-12'<'pull-right'<'toolbarindex'>><'pull-left'l>r<'clearfix'>>><'row'<'table-responsive'<'col-xs-12't>>><'row'<'col-sm-6'<'pull-left'<'toolbar'> i>><'col-sm-6'<'pull-right'p>><'clearfix'>>",
		"columns": [
			{"data": "no"},
			{"data": "aksi"},
			{"data": "tanggal_angsuran"},
			{"data": "jumlah_angsuran"},
			{"data": "jumlah_bunga"},
			{"data": "jumlah_denda"},
			{"data": "keterangan"},
		],

		"pageLength"  : 100,
		"deferRender" : true,
		"serverSide"  : true,
		"processing"  : false,
		"filter"      : false,
		"ordering"    : true,
		"bLengthChange": false,

		"order": [[ 0, "desc" ]],

		"columnDefs": [
			{ "targets": 0, "orderable": false },
			{ "targets": 1, "orderable": false },
			{ "targets": 6, "orderable": false },
		],

		"language": {
			"sProcessing"   : "Sedang memproses...",
			"sLengthMenu"   : "Tampilkan _MENU_ entri",
			"sZeroRecords"  : "Tidak ditemukan data",
			"sInfo"         : "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
			"sInfoEmpty"    : "Menampilkan 0 sampai 0 dari 0 entri",
			"sInfoFiltered" : "(difilter dari _MAX_ entri keseluruhan)",
			"sInfoPostFix"  : "",
			"sUrl"          : "",
			"oPaginate"     : {
				"sFirst"        : "Pertama",
				"sPrevious"     : "<<",
				"sNext"         : ">>",
				"sLast"         : "Terakhir"
			}
		},
	});

	$(".table-list").on("click", "#btn-delete", function(event) {
		var id = $(this).data("id");
		
		swal({
			title: "Apakah anda yakin",
			text: "Anda akan menghapus data ini",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#f4516c",
			cencelButtonColor: "#eaeaea",
			confirmButtonText: "Hapus Data",
			cancelButtonText: "Batalkan",
			showLoaderOnConfirm: true,
			closeOnConfirm: true,
			closeOnCancel: true
		},
		function (isConfirm) {
			if (isConfirm) {
				$.ajax({
					url: "<?php echo base_url("pinjaman/angsuran_kelompok/delete_proses") ?>",
					type: "post",
					dataType: "json",
					data: { id: id },
					success: function (json) {
						if (json.stat) {
							notif(true, "Data berhasil di hapus");
							getResume();
							refreshTable();
						} else {
							notif(false, "Data gagal di hapus");
						}
					}
				});
			}
		});
	});

	$(".form-filter").validate({
		submitHandler : function(form) {
			refreshTable();
			return false;
		}
	});

	$(".form-filter").on('click', '.btn-reset', function(event) {
		event.preventDefault();

		$(".form-filter").find("input").val("");

		refreshTable();
	});
});

function refreshTable () {
	var dtable = $(".table-list").DataTable();
	dtable.draw();
}

function getResume () {
	var id_pinjaman = $(".id_pinjaman").val();
	var id_member = $(".id_member").val();

	$.ajax({
		url: '<?php echo base_url('pinjaman/angsuran_kelompok/getResume') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			id_pinjaman: id_pinjaman,
			id_member: id_member,
		},
		success: function (json) {
			$(".total-pinjaman").html(json.pinjaman);
			$(".total-angsuran").html(json.angsuran);
			$(".total-sisa").html(json.sisa);

			$(".total-bunga").html(json.total_bunga);
			$(".bunga").html(json.bunga);
			$(".denda").html(json.denda);

			$(".jatuh-tempo").html(json.tanggal_jatuhtempo);
			$(".tanggal-meminjam").html(json.tanggal_meminjam);
		}
	});
}