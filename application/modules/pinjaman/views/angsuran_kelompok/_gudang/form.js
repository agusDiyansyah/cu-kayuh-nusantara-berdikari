$(document).ready(function() {
	 $(".date").datepicker({
		 "autoclose": true,
		 "format": "yyyy-mm-dd"
	 });

	 $('.form').validate({
		ignore: [],
		errorClass: "error",
		rules:{
			id_pinjaman: { required:true },
			tanggal_angsuran: { required:true },
			jumlah_angsuran: { required:true },
		},
		messages:{
			id_pinjaman: { required: "Error Jenis Simpanan" },
			tanggal_angsuran: { required: "Tanggal tidak boleh kosong" },
			jumlah_angsuran: { required: "Jumlah tidak boleh kosong" }
		},
		errorPlacement : function (error, element) {
            error.insertAfter(element);
        },
        highlight      : function (element, validClass) {
            $(element).parent().addClass('has-error');
        },
        unhighlight    : function (element, validClass) {
            $(element).parent().removeClass('has-error');
        }
	});
});
