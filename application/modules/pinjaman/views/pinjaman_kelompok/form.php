<?php echo $this->session->flashdata('msg'); ?>
<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			<?php echo $detail ?>
		</div>
		<div class="col-xs-12 col-md-8">
			<form class="form" action="<?php echo $form_action ?>" method="post">
				<div class="hide">
					<?php echo $input['hide']['id'] ?>
					<?php echo $input['hide']['id_member'] ?>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="control-label">Nomor Pinjaman</label>
							<?php echo $input['no_pinjaman'] ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="control-label">Produk Pinjaman</label>
							<?php echo $input['id_produk_pinjaman'] ?>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="form-group">
							<label for="" class="control-label">Jumlah dana yang dipinjam</label>
							<?php echo $input['jumlah'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4">
						<div class="form-group">
							<label for="" class="control-label">Bunga</label>
							<?php echo $input['bunga'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4">
						<div class="form-group">
							<label for="" class="control-label">Biaya Administrasi</label>
							<?php echo $input['biaya_administrasi'] ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<label for="" class="control-label">Jangka waktu</label>
							<?php echo $input['jangka_waktu'] ?>
							<small>
								Jangka waktu pinjaman dalam bulan
							</small>
						</div>
					</div>

					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<label for="" class="control-label">Tanggal jatuh tempo</label>
							<?php echo $input['tanggal_jatuhtempo'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="" class="control-label">Tanggal Meminjam</label>
							<?php echo $input['tanggal_meminjam'] ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="control-label">Keterangan</label>
							<?php echo $input['keterangan'] ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="pull-right">
							<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
							<button type="submit" name="button" class="btn btn-success">Proses</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>