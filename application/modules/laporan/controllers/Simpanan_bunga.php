<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simpanan_bunga extends Admin_controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("M_laporan_simpanan_bunga");
	}

	public function index() {
		// select2
		$this->output->css("assets/themes/admin/vendors/select2/select2.css");
		$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
		$this->output->js("assets/themes/admin/vendors/select2/select2.js");
		
		// datepicker
		$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
		$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
		$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

		// custom
		$this->output->script_foot("laporan/simpanan_bunga/js/form.js");

		$data = array(
			"filter" => array(
				"nik" => form_input(array(
					"name" => "nik",
					"class" => "form-control nik",
					"type" => "text",
				)),
				"nama_lengkap" => form_input(array(
					"name" => "nama_lengkap",
					"class" => "form-control nama_lengkap",
					"type" => "text",
				)),
				"tanggal1" => form_input(array(
					"name" => "tanggal1",
					"class" => "form-control date tanggal1",
					"type" => "text",
				)),
				"tanggal2" => form_input(array(
					"name" => "tanggal2",
					"class" => "form-control date tanggal2",
					"type" => "text",
				)),
				"tanggal_pembukuan" => form_input(array(
					"name" => "tanggal_pembukuan",
					"class" => "form-control date tanggal_pembukuan",
					"type" => "text",
				)),
			)
		);
		$this->load->view("laporan/simpanan_bunga/form", $data);
	}

	public function laporan () {
		$this->output->unset_template();
		$this->output->set_template("admin/laporan");

		$data = array(
			"laporan" => $this->M_laporan_simpanan_bunga->laporan()
		);

		$this->load->view("laporan/simpanan_bunga/laporan", $data);
	}

}

/* End of file Operasional.php */
/* Location: ./application/controllers/Operasional.php */