<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operasional extends Admin_controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("M_laporan_operasional");
	}

	public function index() {
		// select2
		$this->output->css("assets/themes/admin/vendors/select2/select2.css");
		$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
		$this->output->js("assets/themes/admin/vendors/select2/select2.js");
		
		// datepicker
		$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
		$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
		$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

		// custom
		$this->output->script_foot("laporan/operasional/js/form.js");

		$data = array(
			"filter" => array(
				"nama_barang" => form_input(array(
					"name" => "nama_barang",
					"class" => "form-control nama_barang",
					"type" => "text",
				)),
				"tanggal1" => form_input(array(
					"name" => "tanggal1",
					"class" => "form-control date tanggal1",
					"type" => "text",
				)),
				"tanggal2" => form_input(array(
					"name" => "tanggal2",
					"class" => "form-control date tanggal2",
					"type" => "text",
				)),
				"tanggal_pembukuan" => form_input(array(
					"name" => "tanggal_pembukuan",
					"class" => "form-control date tanggal_pembukuan",
					"type" => "text",
				)),
				"id_jenis_barang" => form_select(array(
					"config" => array(
						"name" => "id_jenis_barang",
						"class" => "form-control s2 id_jenis_barang",
					),
					"list" => array(
						"db" => array(
							"id" => "id_jenis_barang",
							"title" => "jenis_barang",
							"table" => "op_jenis_barang",
						)
					),
				))
			)
		);
		$this->load->view("laporan/operasional/form", $data);
	}

	public function laporan () {
		$this->output->unset_template();
		$this->output->set_template("admin/laporan");

		$data = array(
			"laporan" => $this->M_laporan_operasional->laporan()
		);

		$this->load->view("laporan/operasional/laporan", $data);
	}

}

/* End of file Operasional.php */
/* Location: ./application/controllers/Operasional.php */