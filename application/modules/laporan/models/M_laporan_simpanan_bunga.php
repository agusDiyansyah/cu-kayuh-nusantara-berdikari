<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_laporan_simpanan_bunga extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function laporan () {
		$filter_anggota = $filter_kelompok = "";

		$sql = "
		SELECT
			dm.nik, dm.nama_lengkap, b.jumlah, b.tanggal, b.keterangan, 'anggota' jenis_keanggotaan
		FROM
			member m
		JOIN data_member dm ON m.id_member = dm.id_member
		JOIN simpanan_bunga b ON m.id_member = b.id_member
		WHERE
			dm.is_delete = 0
		AND b.is_delete = 0
		$filter_anggota

		UNION ALL

		SELECT
			dk.no_akta, dk.nama_kelompok, b.jumlah, b.tanggal, b.keterangan, 'kelompok' jenis_keanggotaan
		FROM
			member m
		JOIN data_kelompok dk ON m.id_member = dk.id_member
		JOIN simpanan_bunga b ON m.id_member = b.id_member
		WHERE
			dk.is_delete = 0
		AND b.is_delete = 0
		$filter_kelompok
		";

		return $this->db->query($sql);
	}

}

/* End of file M_laporan_operasional.php */
/* Location: ./application/models/M_laporan_operasional.php */