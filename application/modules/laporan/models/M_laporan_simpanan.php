<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_laporan_simpanan extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function laporan () {
		$filter_anggota = "";
		$filter_kelompok = "";

		if (!empty($this->input->post("nik"))) {
			$filter_anggota .= " AND dm.nik LIKE '%". $this->input->post("nik") ."%' ";
			$filter_kelompok .= " AND dk.no_akta LIKE '%". $this->input->post("nik") ."%' ";
		}
		if (!empty($this->input->post("nama_lengkap"))) {
			$filter_anggota .= " AND dm.nama_lengkap LIKE '%". $this->input->post("nama_lengkap") ."%' ";
			$filter_kelompok .= " AND dk.nama_kelompok LIKE '%". $this->input->post("nama_lengkap") ."%' ";
		}
		if (!empty($this->input->post("id_jenis_simpanan"))) {
			$filter_anggota .= " AND s.id_jenis_simpanan = " . $this->input->post("id_jenis_simpanan");
			$filter_kelompok .= " AND s.id_jenis_simpanan = " . $this->input->post("id_jenis_simpanan");
		}
		if (!empty($this->input->post("tanggal1"))) {
			$filter_anggota .= " AND s.tanggal >= " . $this->input->post("tanggal1");
			$filter_kelompok .= " AND s.tanggal >= " . $this->input->post("tanggal1");
		}
		if (!empty($this->input->post("tanggal2"))) {
			$filter_anggota .= " AND s.tanggal <= " . $this->input->post("tanggal2");
			$filter_kelompok .= " AND s.tanggal <= " . $this->input->post("tanggal2");
		}

		$sql = "
		SELECT
			dm.nik, dm.nama_lengkap, s.tanggal, s.jumlah, j.id_jenis_simpanan, j.jenis, 'anggota' jenis_keanggotaan
		FROM
			member m
		LEFT JOIN data_member dm ON m.id_member = dm.id_member
		LEFT JOIN simpanan s ON m.id_member = s.id_member
		LEFT JOIN jenis_simpanan j ON j.id_jenis_simpanan = s.id_jenis_simpanan
		WHERE
			j.id_jenis_simpanan != 9
		AND s.is_delete = 0
		AND dm.is_delete = 0
		$filter_anggota

		UNION ALL

		SELECT
			dk.no_akta, dk.nama_kelompok, s.tanggal, s.jumlah, j.id_jenis_simpanan, j.jenis, 'kelompok' jenis_keanggotaan
		FROM
			member m
			LEFT JOIN data_kelompok dk ON m.id_member = dk.id_member
			JOIN simpanan s ON m.id_member = s.id_member
			LEFT JOIN jenis_simpanan j ON j.id_jenis_simpanan = s.id_jenis_simpanan
		WHERE
			j.id_jenis_simpanan = 9
		AND s.is_delete = 0
		AND dk.is_delete = 0
		$filter_kelompok
		ORDER BY
			id_jenis_simpanan
		";
		return $this->db->query($sql);
	}

}

/* End of file M_laporan_operasional.php */
/* Location: ./application/models/M_laporan_operasional.php */