<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_laporan_pinjaman_angsuran extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function laporan () {
		$filter_anggota = "";
		$filter_kelompok = "";

		if (!empty($this->input->post("nik"))) {
			$filter_anggota .= " AND dm.nik LIKE '%". $this->input->post("nik") ."%' ";
			$filter_kelompok .= " AND dk.no_akta LIKE '%". $this->input->post("nik") ."%' ";
		}
		if (!empty($this->input->post("nama_lengkap"))) {
			$filter_anggota .= " AND dm.nama_lengkap LIKE '%". $this->input->post("nama_lengkap") ."%' ";
			$filter_kelompok .= " AND dk.nama_kelompok LIKE '%". $this->input->post("nama_lengkap") ."%' ";
		}
		if (!empty($this->input->post("id_produk_pinjaman"))) {
			$filter_anggota .= " AND pr.id_produk_pinjaman = " . $this->input->post("id_produk_pinjaman");
			$filter_kelompok .= " AND pr.id_produk_pinjaman = " . $this->input->post("id_produk_pinjaman");
		}
		if (!empty($this->input->post("tanggal1"))) {
			$filter_anggota .= " AND a.tanggal_angsuran >= " . $this->input->post("tanggal1");
			$filter_kelompok .= " AND a.tanggal_angsuran >= " . $this->input->post("tanggal1");
		}
		if (!empty($this->input->post("tanggal2"))) {
			$filter_anggota .= " AND a.tanggal_angsuran <= " . $this->input->post("tanggal2");
			$filter_kelompok .= " AND a.tanggal_angsuran <= " . $this->input->post("tanggal2");
		}

		$sql = "
		SELECT
			dm.nik, 
			dm.nama_lengkap, 
			a.jumlah_angsuran jumlah,
			a.jumlah_bunga bunga,
			a.jumlah_denda denda,
			a.tanggal_angsuran,
			a.keterangan,
			pr.id_produk_pinjaman, 
			pr.nama_produk, 
			'anggota' jenis_keanggotaan
		FROM
			member m
		JOIN data_member dm ON m.id_member = dm.id_member
		JOIN pinjaman p ON m.id_member = p.id_member
		JOIN angsuran a ON p.id_pinjaman = a.id_pinjaman
		LEFT JOIN pinjaman_produk pr ON pr.id_produk_pinjaman = p.id_produk_pinjaman
		WHERE
			p.is_delete = 0
		AND a.is_delete = 0
		AND dm.is_delete = 0
		$filter_anggota

		UNION ALL

		SELECT
			dk.no_akta, 
			dk.nama_kelompok, 
			a.jumlah_angsuran jumlah,
			a.jumlah_bunga bunga,
			a.jumlah_denda denda,
			a.tanggal_angsuran,
			a.keterangan,
			pr.id_produk_pinjaman, 
			pr.nama_produk, 
			'kelompok' jenis_keanggotaan
		FROM
			member m
		JOIN data_kelompok dk ON m.id_member = dk.id_member
		JOIN pinjaman p ON m.id_member = p.id_member
		JOIN angsuran a ON p.id_pinjaman = a.id_pinjaman
		LEFT JOIN pinjaman_produk pr ON pr.id_produk_pinjaman = p.id_produk_pinjaman
		WHERE
			p.is_delete = 0
		AND a.is_delete = 0
		AND dk.is_delete = 0
		$filter_kelompok
		ORDER BY
			id_produk_pinjaman,
			jenis_keanggotaan
		";
		return $this->db->query($sql);
	}

}

/* End of file M_laporan_operasional.php */
/* Location: ./application/models/M_laporan_operasional.php */