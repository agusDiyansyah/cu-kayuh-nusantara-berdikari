<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_laporan_administrasi extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function laporan () {
		if (!empty($this->input->post("nik"))) {
			$this->db->like("dm.nik", $this->input->post("nik"), "both");
		}
		if (!empty($this->input->post("nama_lengkap"))) {
			$this->db->like("dm.nama_lengkap", $this->input->post("nama_lengkap"), "both");
		}
		if (!empty($this->input->post("id_jenis_adm"))) {
			$this->db->where("j.id_jenis_adm", $this->input->post("id_jenis_adm"));
		}
		if (!empty($this->input->post("tanggal1"))) {
			$this->db->where("i.tanggal >=", $this->input->post("tanggal1"), true);
		}
		if (!empty($this->input->post("tanggal2"))) {
			$this->db->where("i.tanggal <=", $this->input->post("tanggal2"), true);
		}

		return $this->db
			->select("dm.nama_lengkap, dm.nik")
			->select("i.tanggal, i.jumlah")
			->select("j.id_jenis_adm, j.jenis_adm jenis")
			->from("member m")
			->join("data_member dm", "m.id_member = dm.id_member")
			->join("adm_iuran i", "m.id_member = i.id_member")
			->join("adm_jenis j", "j.id_jenis_adm = i.id_jenis_adm")
			->where("dm.is_delete = 0")
			->where("i.is_delete = 0")
			->order_by("j.id_jenis_adm")
			->order_by("i.tanggal", "desc")
			->get();
	}

}

/* End of file M_laporan_administrasi.php */
/* Location: ./application/models/M_laporan_administrasi.php */