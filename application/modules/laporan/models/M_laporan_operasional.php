<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_laporan_operasional extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function laporan () {
		if (!empty($this->input->post("nama_barang"))) {
			$this->db->like("o.nama_barang", $this->input->post("nama_barang"), "both");
		}
		if (!empty($this->input->post("id_jenis_barang"))) {
			$this->db->where("j.id_jenis_barang", $this->input->post("id_jenis_barang"));
		}
		if (!empty($this->input->post("tanggal1"))) {
			$this->db->where("o.tanggal >=", $this->input->post("tanggal1"), true);
		}
		if (!empty($this->input->post("tanggal2"))) {
			$this->db->where("o.tanggal <=", $this->input->post("tanggal2"), true);
		}
		return $this->db
			->select("o.nama_barang, o.jumlah, o.harga, o.satuan, o.tanggal, o.keterangan")
			->select("j.id_jenis_barang, j.jenis_barang")
			->from("op_data o")
			->join("op_jenis_barang j", "j.id_jenis_barang = o.id_jenis_barang")
			->order_by("j.id_jenis_barang")
			->order_by("o.tanggal", "desc")
			->where("o.is_delete = 0")
			->get();
	}

}

/* End of file M_laporan_operasional.php */
/* Location: ./application/models/M_laporan_operasional.php */