<center class="title">
	<h2>CU KAYUH NUSANTARA BERDIKARI</h2>
	<h2>LAPORAN ANGSURAN PINJAMAN ANGGOTA / KELOMPOK</h2>
</center>

<table>
	<thead>
		<tr>
			<th width="2%">No</th>
			<th>NIK / No Akta</th>
			<th>Nama Anggota / Kelompok</th>
			<th>Tanggal Angsuran</th>
			<th>Bunga</th>
			<th>Denda</th>
			<th>Jumlah Angsuran</th>
			<th width="20%">Keterangan</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$id_produk =
		$produk = 
		$sub_total =
		$sub_total_denda =
		$sub_total_bunga =
		$sub_keanggotaan =
		$sub_keanggotaan_denda =
		$sub_keanggotaan_bunga =
		$total =
		$bunga =
		$denda =
		$no = 0;

		$jenis_keanggotaan = "";
		foreach ($laporan->result() as $data) {
			$no++;
			if ($data->id_produk_pinjaman != $id_produk AND $id_produk > 0) {
				echo "
				<tr style='background-color: #E0E6E6'>
					<td colspan='4'>
						<b>TOTAL PINJAMAN ". strtoupper($produk) ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_total_bunga, 2, ",", ".") ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_total_denda, 2, ",", ".") ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_total, 2, ",", ".") ."</b>
					</td>
					<td></td>
				</tr>
				";

				$sub_total_bunga =
				$sub_total_denda =
				$sub_total = 0;
			}
			if ($data->jenis_keanggotaan != $jenis_keanggotaan AND $jenis_keanggotaan != "") {
				echo "
				<tr style='background-color: #959797'>
					<td colspan='4'>
						<b>TOTAL PINJAMAN ". strtoupper($jenis_keanggotaan) ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_keanggotaan_bunga, 2, ",", ".") ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_keanggotaan_denda, 2, ",", ".") ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_keanggotaan, 2, ",", ".") ."</b>
					</td>
					<td></td>
				</tr>
				";

				$sub_keanggotaan_bunga =
				$sub_keanggotaan_denda =
				$sub_keanggotaan = 0;
			}
			if ($data->jenis_keanggotaan != $jenis_keanggotaan) {
				echo "
				<tr>
					<td colspan='8'>
						<b>". strtoupper($data->jenis_keanggotaan) ."</b>
					</td>
				</tr>
				";

				$jenis_keanggotaan = $data->jenis_keanggotaan;
			}
			if ($data->id_produk_pinjaman != $id_produk) {
				echo "
				<tr>
					<td></td>
					<td colspan='7'>
						<b>". strtoupper($data->nama_produk) ."</b>
					</td>
				</tr>
				";

				$id_produk = $data->id_produk_pinjaman;
			}

			echo "
			<tr>
				<td>$no</td>
				<td>$data->nik</td>
				<td>$data->nama_lengkap</td>
				<td class='center-align'>". konversi_tanggal($data->tanggal_angsuran, "j F Y") ."</td>
				<td class='right-align'>". number_format($data->bunga, 2, ",", ".") ."</td>
				<td class='right-align'>". number_format($data->denda, 2, ",", ".") ."</td>
				<td class='right-align'>". number_format($data->jumlah, 2, ",", ".") ."</td>
				<td>$data->keterangan</td>
			</tr>
			";

			$produk = $data->nama_produk;

			$sub_total += $data->jumlah;
			$sub_total_bunga += $data->bunga;
			$sub_total_denda += $data->denda;

			$sub_keanggotaan += $data->jumlah;
			$sub_keanggotaan_bunga += $data->bunga;
			$sub_keanggotaan_denda += $data->denda;

			$total += $data->jumlah;
			$bunga += $data->bunga;
			$denda += $data->denda;
		}
		echo "
		<tr style='background-color: #E0E6E6'>
			<td colspan='4'>
				<b>TOTAL PINJAMAN ". strtoupper($produk) ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_total_bunga, 2, ",", ".") ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_total_denda, 2, ",", ".") ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_total, 2, ",", ".") ."</b>
			</td>
			<td></td>
		</tr>
		";
		echo "
		<tr style='background-color: #959797'>
			<td colspan='4'>
				<b>TOTAL PINJAMAN ". strtoupper($jenis_keanggotaan) ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_keanggotaan_bunga, 2, ",", ".") ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_keanggotaan_denda, 2, ",", ".") ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_keanggotaan, 2, ",", ".") ."</b>
			</td>
			<td></td>
		</tr>
		";
		echo "
		<tr style='background-color: #959797'>
			<td colspan='4'>
				<b>TOTAL PINJAMAN KESELURUHAN</b>
			</td>
			<td class='right-align'>
				<b>". number_format($denda, 2, ",", ".") ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($bunga, 2, ",", ".") ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($total, 2, ",", ".") ."</b>
			</td>
			<td></td>
		</tr>
		";
		?>
	</tbody>
</table>

<div class="foot">
	Pontianak, <?php echo empty($this->input->post("tanggal_pembukuan")) ? konversi_tanggal(date("Y-m-d"), "j F Y") : konversi_tanggal($this->input->post("tanggal_pembukuan"), "j F Y") ?>
</div>