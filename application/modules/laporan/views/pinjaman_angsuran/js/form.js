$(document).ready(function() {
	$(".mn-laporan-pinjaman").addClass('collapsed');
	$(".mn-laporan-pinjaman-angsuran").addClass('active');
	
	$(".date").datepicker({
		autoclose: true,
		format: "yyyy-mm-dd"
	});

	$(".s2").select2({
		width: "100%"
	});

	$(".btn-reset").on('click', function(event) {
		event.preventDefault();
		var filter = $("form");

		filter.find('input').val("");
		filter.find('.s2').select2("val", "");
	});
});