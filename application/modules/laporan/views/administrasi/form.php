<div class="container-fluid">
	<form action="<?php echo base_url("laporan/administrasi/laporan") ?>" target="_blank" method="post">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">NIK</label>
					<?php echo $filter['nik'] ?>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Nama Anggota</label>
					<?php echo $filter['nama_lengkap'] ?>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Jenis Administrasi</label>
					<?php echo $filter['id_jenis_adm'] ?>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Tanggal</label>
					<div class="input-group">
						<?php echo $filter['tanggal1'] ?>
						<div class="input-group-addon">s/d</div>
						<?php echo $filter['tanggal2'] ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Tanggal Pembukuan</label>
					<?php echo $filter['tanggal_pembukuan'] ?>
				</div>
			</div>

			<div class="col-xs-12">
				<div class="form-group text-right">
					<a href="" class="btn btn-default btn-reset">Reset</a>
					<button class="btn btn-success" type="submit">Proses</button>
				</div>
			</div>
		</div>
	</form>
</div>