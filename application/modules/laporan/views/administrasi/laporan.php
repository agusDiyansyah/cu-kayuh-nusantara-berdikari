<center class="title">
	<h2>CU KAYUH NUSANTARA BERDIKARI</h2>
	<h2>LAPORAN ANGSURAN ADMINISTRASI ANGGOTA</h2>
</center>

<table>
	<thead>
		<tr>
			<th width="2%">NO</th>
			<th>NIK</th>
			<th>NAMA ANGGOTA</th>
			<th>TANGGAL IURAN</th>
			<th>JUMLAH</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 
		$sub_total =
		$total =
		$jenis =
		$id_jenis = 0;
		foreach ($laporan->result() as $data) {
			$no++;
			if ($data->id_jenis_adm != $id_jenis AND $id_jenis > 0) {
				echo "
					<tr style='background-color: #E0E6E6'>
						<td colspan='4'><b>SUB TOTAL ". strtoupper($jenis) ."</b></td>
						<td style='text-align: right'><b>". number_format($sub_total, 0, ",", ".") ."</b></td>
					</tr>
				";

				$sub_total = 0;
			}
			if ($data->id_jenis_adm != $id_jenis) {
				echo "
					<tr>
						<td colspan='5'><b>". strtoupper($data->jenis) ."</b></td>
					</tr>
				";
				$id_jenis = $data->id_jenis_adm;
			}

			echo "
				<tr>
					<td>$no</td>
					<td>$data->nik</td>
					<td>$data->nama_lengkap</td>
					<td style='text-align: center'>". konversi_tanggal($data->tanggal, "j F Y") ."</td>
					<td style='text-align: right'>". number_format($data->jumlah, 0, ",", ".") ."</td>
				</tr>
			";

			$jenis = $data->jenis;
			$sub_total += $data->jumlah;
			$total += $data->jumlah;
		}
		echo "
			<tr style='background-color: #E0E6E6'>
				<td colspan='4'><b>SUB TOTAL ". strtoupper($jenis) ."</b></td>
				<td style='text-align: right'><b>". number_format($sub_total, 0, ",", ".") ."</b></td>
			</tr>
		";
		?>
		<tr style='background-color: #E0E6E6'>
			<td colspan="4">
				<b>TOTAL SELURUH IURAN ADMINISTRASI ANGGOTA</b>
			</td>
			<td style='text-align: right'>
				<b><?php echo number_format($total, 0, ",", ".") ?></b>
			</td>
		</tr>
	</tbody>
</table>

<div class="foot">
	Pontianak, <?php echo empty($this->input->post("tanggal_pembukuan")) ? konversi_tanggal(date("Y-m-d"), "j F Y") : konversi_tanggal($this->input->post("tanggal_pembukuan"), "j F Y") ?>
</div>