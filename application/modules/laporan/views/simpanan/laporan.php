<center class="title">
	<h2>CU KAYUH NUSANTARA BERDIKARI</h2>
	<h2>LAPORAN SIMPANAN ANGGOTA / KELOMPOK</h2>
</center>

<table>
	<thead>
		<tr>
			<th width="2%">No</th>
			<th>NIK / No Akta</th>
			<th>Nama Anggota / Kelompok</th>
			<th>Tanggal Simpanan</th>
			<th>Jumlah</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$id_jenis =
		$jenis = 
		$sub_total =
		$sub_keanggotaan =
		$total =
		$no = 0;

		$jenis_keanggotaan = "";
		foreach ($laporan->result() as $data) {
			$no++;
			if ($data->id_jenis_simpanan != $id_jenis AND $id_jenis > 0) {
				echo "
				<tr style='background-color: #E0E6E6'>
					<td colspan='4'>
						<b>TOTAL ". strtoupper($jenis) ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_total, 2, ",", ".") ."</b>
					</td>
				</tr>
				";

				$sub_total = 0;
			}
			if ($data->jenis_keanggotaan != $jenis_keanggotaan AND $jenis_keanggotaan != "") {
				echo "
				<tr style='background-color: #E0E6E6'>
					<td colspan='4'>
						<b>TOTAL SIMPANAN ". strtoupper($jenis_keanggotaan) ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_keanggotaan, 2, ",", ".") ."</b>
					</td>
				</tr>
				";

				$sub_keanggotaan = 0;
			}
			if ($data->jenis_keanggotaan != $jenis_keanggotaan) {
				echo "
				<tr>
					<td colspan='5'>
						<b>". strtoupper($data->jenis_keanggotaan) ."</b>
					</td>
				</tr>
				";

				$jenis_keanggotaan = $data->jenis_keanggotaan;
			}
			if ($data->id_jenis_simpanan != $id_jenis) {
				echo "
				<tr>
					<td></td>
					<td colspan='4'>
						<b>". strtoupper($data->jenis) ."</b>
					</td>
				</tr>
				";

				$id_jenis = $data->id_jenis_simpanan;
			}

			echo "
			<tr>
				<td>$no</td>
				<td>$data->nik</td>
				<td>$data->nama_lengkap</td>
				<td class='center-align'>". konversi_tanggal($data->tanggal, "j F Y") ."</td>
				<td class='right-align'>". number_format($data->jumlah, 2, ",", ".") ."</td>
			</tr>
			";

			$jenis = $data->jenis;
			$sub_total += $data->jumlah;
			$sub_keanggotaan += $data->jumlah;
			$total += $data->jumlah;
		}
		echo "
		<tr style='background-color: #E0E6E6'>
			<td colspan='4'>
				<b>TOTAL ". strtoupper($jenis) ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_total, 2, ",", ".") ."</b>
			</td>
		</tr>
		";
		echo "
		<tr style='background-color: #E0E6E6'>
			<td colspan='4'>
				<b>TOTAL SIMPANAN ". strtoupper($jenis_keanggotaan) ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_keanggotaan, 2, ",", ".") ."</b>
			</td>
		</tr>
		";
		echo "
		<tr style='background-color: #E0E6E6'>
			<td colspan='4'>
				<b>TOTAL SIMPANAN KESELURUHAN</b>
			</td>
			<td class='right-align'>
				<b>". number_format($total, 2, ",", ".") ."</b>
			</td>
		</tr>
		";
		?>
	</tbody>
</table>

<div class="foot">
	Pontianak, <?php echo empty($this->input->post("tanggal_pembukuan")) ? konversi_tanggal(date("Y-m-d"), "j F Y") : konversi_tanggal($this->input->post("tanggal_pembukuan"), "j F Y") ?>
</div>