<center class="title">
	<h2>CU KAYUH NUSANTARA BERDIKARI</h2>
	<h2>LAPORAN BUNGA SIMPANAN ANGGOTA / KELOMPOK</h2>
</center>

<table>
	<thead>
		<tr>
			<th>NO</th>
			<th>NIK / AKTA</th>
			<th>NAMA LENGKAP / KELOMPOK</th>
			<th>TANGGAL</th>
			<th>JUMLAH BUNGA SIMPANAN</th>
			<th>KETERANGAN</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 
		$total =
		$sub_total =
		$jenis =
		$id_jenis = 0;

		$jenis_keanggotaan = "";
		foreach ($laporan->result() as $data) {
			$no++;

			if ($data->jenis_keanggotaan != $jenis_keanggotaan AND $jenis_keanggotaan != "") {
				echo "
				<tr style='background-color: #E0E6E6'>
					<td colspan='4'>
						<b>TOTAL ". strtoupper($jenis_keanggotaan) ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_total, 2, ",", ".") ."</b>
					</td>
					<td></td>
				</tr>
				";

				$sub_total = 0;
			}
			if ($data->jenis_keanggotaan != $jenis_keanggotaan) {
				echo "
				<tr>
					<td colspan='6'>
						<b>". strtoupper($data->jenis_keanggotaan) ."</b>
					</td>
				</tr>
				";

				$jenis_keanggotaan = $data->jenis_keanggotaan;
			}

			echo "
			<tr>
				<td>$no</td>
				<td>$data->nik</td>
				<td>$data->nama_lengkap</td>
				<td class='center-align'>". konversi_tanggal($data->tanggal, "j F Y") ."</td>
				<td class='right-align'>". number_format($data->jumlah, 2, ',', '.') ."</td>
				<td width='20%'>$data->keterangan</td>
			</tr>
			";

			$sub_total += $data->jumlah;
			$total += $data->jumlah;
		}
		echo "
		<tr style='background-color: #E0E6E6'>
			<td colspan='4'>
				<b>TOTAL ". strtoupper($jenis_keanggotaan) ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_total, 2, ",", ".") ."</b>
			</td>
			<td></td>
		</tr>
		";
		echo "
		<tr style='background-color: #E0E6E6'>
			<td colspan='4'>
				<b>TOTAL BUNGA SIMPANAN KESELIRIHAN</b>
			</td>
			<td class='right-align'>
				<b>". number_format($total, 2, ",", ".") ."</b>
			</td>
			<td></td>
		</tr>
		";
		?>
	</tbody>
</table>

<div class="foot">
	Pontianak, <?php echo empty($this->input->post("tanggal_pembukuan")) ? konversi_tanggal(date("Y-m-d"), "j F Y") : konversi_tanggal($this->input->post("tanggal_pembukuan"), "j F Y") ?>
</div>