<center class="title">
	<h2>CU KAYUH NUSANTARA BERDIKARI</h2>
	<h2>LAPORAN OPERASIONAL</h2>
</center>

<table>
	<thead>
		<tr>
			<th>NO</th>
			<th>NAMA OPERASIONAL</th>
			<th>TANGGAL</th>
			<th>JUMLAH</th>
			<th>SATUAN</th>
			<th>HARGA SATUAN</th>
			<th>TOTAL</th>
			<th>KETERANGAN</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 
		$total =
		$sub_total =
		$jenis =
		$id_jenis = 0;
		foreach ($laporan->result() as $data) {
			$no++;

			if ($data->id_jenis_barang != $id_jenis AND $id_jenis > 0) {
				echo "
				<tr style='background-color: #E0E6E6'>
					<td colspan='6'>
						<b>TOTAL ". strtoupper($jenis) ."</b>
					</td>
					<td class='right-align'>
						<b>". number_format($sub_total, 2, ",", ".") ."</b>
					</td>
					<td></td>
				</tr>
				";

				$sub_total = 0;
			}
			if ($data->id_jenis_barang != $id_jenis) {
				echo "
				<tr>
					<td colspan='8'>
						<b>". strtoupper($data->jenis_barang) ."</b>
					</td>
				</tr>
				";

				$id_jenis = $data->id_jenis_barang;
			}

			echo "
			<tr>
				<td>$no</td>
				<td>$data->nama_barang</td>
				<td class='center-align'>". konversi_tanggal($data->tanggal, "j F Y") ."</td>
				<td class='center-align'>$data->jumlah</td>
				<td class='center-align'>$data->satuan</td>
				<td class='right-align'>". number_format($data->harga, 2, ",", ".") ."</td>
				<td class='right-align'>". number_format($data->jumlah * $data->harga, 2, ",", ".") ."</td>
				<td width='20%'>$data->keterangan</td>
			</tr>
			";

			$jenis = $data->jenis_barang;
			$sub_total += $data->jumlah * $data->harga;
			$total += $data->jumlah * $data->harga;
		}
		echo "
		<tr style='background-color: #E0E6E6'>
			<td colspan='6'>
				<b>TOTAL ". strtoupper($jenis) ."</b>
			</td>
			<td class='right-align'>
				<b>". number_format($sub_total, 2, ",", ".") ."</b>
			</td>
			<td></td>
		</tr>
		";
		?>
		<tr style='background-color: #E0E6E6'>
			<td colspan='6'>
				<b>TOTAL KESELURUHAN</b>
			</td>
			<td class='right-align'>
				<b><?php echo number_format($total, 2, ",", ".") ?></b>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>

<div class="foot">
	Pontianak, <?php echo empty($this->input->post("tanggal_pembukuan")) ? konversi_tanggal(date("Y-m-d"), "j F Y") : konversi_tanggal($this->input->post("tanggal_pembukuan"), "j F Y") ?>
</div>