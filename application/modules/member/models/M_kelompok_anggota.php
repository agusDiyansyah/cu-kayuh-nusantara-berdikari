<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kelompok_anggota extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = false) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
		
			$this->db
				->from("anggota a")
				->where("a.id_data_kelompok", $post['id_data_kelompok'])
				->where("a.is_delete", 0)
				->join("data_member dm", "dm.nik = a.nik", "left");
			
			// filter
			
			$orderColumn = array(
				2 => "a.nama",
				3 => "a.jabatan",
				4 => "a.no_telp",
				5 => "a.email",
			);
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by('a.jabatan', 'asc');
				$this->db->order_by('a.id_anggota', $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

		$this->db->stop_cache();

		// get num rows
		$this->db->select('a.id_anggota');
		$rowCount = $this->db->get()->num_rows();

		// get result
		$this->db->select("a.*, dm.is_anggota, dm.id_data_member, dm.id_member");
		// echo $this->db->last_query(); die();

		$this->db->limit($post['length'], $post['start']);

		$val = $this->db->get()->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		$jenis_kelamin = array(
			"L" => "LAKI-LAKI",
			"P" => "PEREMPUAN"
		);
		$jabatan = array(
			"1" => "KETUA",
			"2" => "SEKRETARIS",
			"3" => "BENDAHARA",
			"4" => "ANGGOTA",
		);
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}member/kelompok_anggota/form/$data->id_data_kelompok/$data->id_anggota' id='btn-detail'>
					Edit
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_anggota' id='btn-delete'>
					Hapus
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";

			$member = "<span class='pull-right'>";

			if (!empty($data->id_data_member)) {
				$member .= "<a target='_blank' href='". base_url("member/kelompok_anggota/detail/$data->id_member/$data->id_data_kelompok") ."' class='btn btn-default btn-xs'>MEMBER</a>&nbsp";
			}

			if (!empty($data->is_anggota) AND $data->is_anggota != '0') {
				$member .= "<a class='btn btn-success btn-xs' target='_blank' href='". base_url("member/kelompok_anggota/detail/$data->id_member/$data->id_data_kelompok") ."'>ANGGOTA</a>&nbsp";
			}

			$member .= "</span>";
			
			$baris = array(
				"no" => $no,
				"id_anggota" => $data->id_anggota,
				"aksi" => $aksi,
				"nama" => strtoupper($data->nama) . "&nbsp$member <br><small><b>$data->nik</b></small>",
				"jabatan" => $jabatan[$data->jabatan],
				"no_telp" => $data->no_telp,
				"email" => $data->email,
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert("anggota", $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where("id_anggota", $id)
			->update("anggota", $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where("id_anggota", $id)
			->update("anggota", array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("a.id_anggota", $id)
			->get("anggota a");
	}
}