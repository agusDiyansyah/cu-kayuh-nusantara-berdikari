<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_administrasi extends CI_Model {
	private $tb = "adm_iuran";
	private $tb_id = "id_iuran_adm";

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = true) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
		
			$this->db
				->where("is_delete", 0)
				->where("id_jenis_adm", $post['id_jenis_adm'])
				->where("id_member", $post['id_member'])
				->from("$this->tb i");
			
			// filter
			if (!empty($post['tanggal1'])) {
				$this->db->where("tanggal >= '$post[tanggal1]'");
			}
			if (!empty($post['tanggal2'])) {
				$this->db->where("tanggal <= '$post[tanggal2]'");
			}

		$this->db->stop_cache();

		// get num rows
		$this->db->select('i.id_iuran_adm');
		$rowCount = $this->db->get()->num_rows();
		
		$orderColumn = array(
			2 => "i.tanggal",
			3 => "i.jumlah",
		);
		
		// order
		if ($order['column'] == 0) {
			$this->db->order_by('i.tanggal', "desc");
			$this->db->order_by('i.id_iuran_adm', $order['dir']);
		} else {
			$this->db->order_by($orderColumn[$order['column']], $order['dir']);
		}
		
		// get result
		// $this->db->select("
		// 	i.id_iuran_adm,
		// 	i.id_jenis_adm,
		// 	i.id_member,
		// 	i.keterangan,
		// 	i.tanggal,
		// 	i.jumlah
		// ");

		$this->db->limit($post['length'], $post['start']);

		$val = $this->db->get()->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}member/administrasi/form/$data->id_jenis_adm/$data->id_member/$data->id_iuran_adm' id='btn-edit' data-id_jenis_adm='$data->id_jenis_adm' data-id_member='$data->id_member' data-id_iuran_adm='$data->id_iuran_adm'>
					Edit
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_iuran_adm' id='btn-delete'>
					Hapus
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";
			
			$baris = array(
				"no" => $no,
				"aksi" => $aksi,
				"keterangan" => $data->keterangan,
				"tanggal" => konversi_tanggal($data->tanggal, "j F Y"),
				"jumlah" => number_format($data->jumlah, 0, ",", "."),
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert($this->tb, $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("i.$this->tb_id", $id)
			->get("$this->tb i");
	}
}