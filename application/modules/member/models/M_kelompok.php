<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kelompok extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = true) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
		
			$this->db->from("member m");
			$this->db->join("data_kelompok dk", "m.id_member = dk.id_member");
			$this->db->join("anggota a", "dk.id_data_kelompok = a.id_data_kelompok", "left");
			$this->db->where("m.jenis_kepemilikan_akun", "kelompok");
			$this->db->where("dk.is_delete", "0");
			$this->db->group_by("dk.id_data_kelompok");
			
			// filter
			if (!empty($post['nomor_anggota'])) {
				$this->db
					->join("relasi_member_jenissimpanan rel", "rel.id_member = m.id_member")
					->group_by("rel.id_member")
					->like("rel.nomor_anggota", $post['nomor_anggota'], "both");
			}
			if (!empty($post['nama_kelompok'])) {
				$this->db->like("dk.nama_kelompok", $post['nama_kelompok'], "both");
			}
			if (!empty($post['bidang_usaha'])) {
				$this->db->like("dk.bidang_usaha", $post['bidang_usaha'], "both");
			}
			if (!empty($post['email'])) {
				$this->db->like("dk.email", $post['email'], "both");
			}
			if (!empty($post['telp'])) {
				$this->db->like("dk.telp", $post['telp'], "both");
			}

		$this->db->stop_cache();

		// get num rows
		$this->db->select('m.id_member');
		$rowCount = $this->db->get()->num_rows();
		
		$orderColumn = array(
			2 => "dk.nama_kelompok",
			3 => "dk.bidang_usaha",
			4 => "anggota",
			// 5 => "dk.telp",
			// 6 => "dk.email",
			7 => "dk.tanggal_berdiri",
			8 => "dk.tanggal_bergabung",
		);
		
		// order
		if ($order['column'] == 0) {
			$this->db->order_by('m.id_member', $order['dir']);
		} else {
			$this->db->order_by($orderColumn[$order['column']], $order['dir']);
		}
		
		// get result
		$this->db->select('
			SUM(IF(a.is_delete = 1 OR a.id_data_kelompok IS NULL, 0, 1)) anggota,
			m.id_member,
			m.tanggal_bergabung,
			dk.id_data_kelompok,
			dk.nama_kelompok,
			dk.tanggal_berdiri,
			dk.bidang_usaha,
			dk.jumlah_anggota,
			dk.no_akta,
			dk.telp,
			dk.email,
		');

		$this->db->limit($post['length'], $post['start']);

		$val = $this->db->get()->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		$jenis_kelamin = array(
			"L" => "LAKI-LAKI",
			"P" => "PEREMPUAN"
		);
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}member/kelompok/form/$data->id_data_kelompok' id='btn-detail'>
					Edit
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_data_kelompok' id='btn-delete'>
					Hapus
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
				<a href='". base_url("member/kelompok/detail/$data->id_member") ."' class='btn btn-default'>DETAIL</a>
			</div>
			";
			
			$baris = array(
				"no" => $no,
				"id_member" => $data->id_member,
				"aksi" => $aksi,
				"nama_kelompok" => "<b>". strtoupper($data->nama_kelompok) ."</b><br /><small>$data->no_akta</small>",
				"tanggal_berdiri" => konversi_tanggal($data->tanggal_berdiri, "j F Y"),
				"tanggal_bergabung" => konversi_tanggal($data->tanggal_bergabung, "j F Y") . "<br /><small>". konversi_tanggal($data->tanggal_bergabung, "H:i") ."</small>",
				"bidang_usaha" => $data->bidang_usaha,
				"jumlah_anggota" => "<a href='{$base}member/kelompok_anggota/daftar/$data->id_data_kelompok'>ANGGOTA ($data->anggota)</a>",
				"telp" => $data->telp,
				"email" => $data->email,
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert("member", array(
			"jenis_kepemilikan_akun" => "kelompok"
		));
		$data['id_member'] = $this->db->insert_id();
		$this->db->insert("data_kelompok", $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where("id_data_kelompok", $id)
			->update("data_kelompok", $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where("id_data_kelompok", $id)
			->update("data_kelompok", array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("dk.id_data_kelompok", $id)
			->get("data_kelompok dk");
	}

	public function simpanan ($id_member) {
		// order dana simpanan
		$this->db->query("DROP TABLE IF EXISTS tmp_simpanan;");
		$this->db->query("
			CREATE TEMPORARY TABLE tmp_simpanan (
				SELECT
					jumlah, tanggal
				FROM
					simpanan s
				WHERE
					s.id_member = $id_member
				AND s.is_delete = 0
				ORDER BY 
					s.tanggal DESC
			);
		");

		// get simpanan
		$simpanan = $this->db
			->select("SUM(jumlah) jumlah, tanggal")
			->get("tmp_simpanan")
			->row();

		return array(
			"jumlah" => $simpanan->jumlah,
			"tanggal" => @konversi_tanggal($simpanan->tanggal, "j F Y")
		);
	}

	public function penarikan ($id_member) {
		// order dana penarikan
		$this->db->query("DROP TABLE IF EXISTS tmp_penarikan;");
		$this->db->query("
			CREATE TEMPORARY TABLE tmp_penarikan (
				SELECT
					jumlah_penarikan jumlah, tanggal
				FROM
					penarikan p
				WHERE
					p.id_member = $id_member
				AND p.is_delete = 0
				ORDER BY 
					p.tanggal DESC
			);
		");

		// get penarikan
		$penarikan = $this->db
			->select("SUM(jumlah) jumlah, tanggal")
			->get("tmp_penarikan")
			->row();

		return array(
			"jumlah" => $penarikan->jumlah,
			"tanggal" => @konversi_tanggal($penarikan->tanggal, "j F Y")
		);
	}

	public function pinjaman ($id_member) {
		// get angsuran ordered by tanggal
		$this->db->query("DROP TABLE IF EXISTS tmp_angsuran;");
		$this->db->query("
			CREATE TEMPORARY TABLE tmp_angsuran (
				SELECT
					a.id_angsuran, 
					a.is_delete, 
					p.id_pinjaman, 
					a.jumlah_angsuran, 
					a.jumlah_denda, 
					a.jumlah_bunga, 
					a.tanggal_angsuran
				FROM
					pinjaman p
				JOIN angsuran a ON p.id_pinjaman = a.id_pinjaman
				WHERE
					p.is_delete = 0
				AND a.is_delete = 0
				AND p.id_member = $id_member
				ORDER BY
					a.tanggal_angsuran DESC
			);
		");

		// get pinjaman and angsuran list
		return $this->db
			->select("
				pr.nama_produk, 
				p.id_member,
				p.id_pinjaman,
				p.no_pinjaman,
				p.jangka_waktu,
				p.tanggal_meminjam,
				p.tanggal_jatuhtempo,
				p.jumlah pinjaman,
				a.tanggal_angsuran,
				COUNT(a.id_angsuran) jumlah_transaksi_angsuran,
				SUM(jumlah_angsuran) angsuran, 
				SUM(a.jumlah_bunga) bunga,
				SUM(a.jumlah_denda) denda
			")
			->from("pinjaman p")
			->join("tmp_angsuran a", "p.id_pinjaman = a.id_pinjaman", "left")
			->join("pinjaman_produk pr", "pr.id_produk_pinjaman = p.id_produk_pinjaman")
			->where("p.is_delete", 0)
			// ->where("a.is_delete", 0)
			->where("p.id_member", $id_member)
			->order_by("p.id_pinjaman", "desc")
			->group_by("p.id_pinjaman")
			->get();
	}

	public function bunga_simpanan ($id_member) {
		$this->db->query("DROP TABLE IF EXISTS tmp_bunga;");
		$this->db->query("
			CREATE TEMPORARY TABLE tmp_bunga (
				SELECT
					jumlah, tanggal
				FROM
					simpanan_bunga
				WHERE
					is_delete = 0
				AND id_member = $id_member
				ORDER BY
					tanggal DESC
			);
		");

		return $this->db
			->select("SUM(jumlah) jumlah, tanggal")
			->get("tmp_bunga")
			->row();
	}

	public function nomor_anggota ($id_member, $id_jenis_simpanan) {
		$sql = $this->db
			->where("id_member", $id_member)
			->where("id_jenis_simpanan", $id_jenis_simpanan)
			->select("nomor_anggota")
			->get("relasi_member_jenissimpanan");
		$nomor_anggota = "";
		if ($sql->num_rows() > 0) {
			$val = $sql->row();
			$nomor_anggota = $val->nomor_anggota;
		}
		return $nomor_anggota;
	}
}