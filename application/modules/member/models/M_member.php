<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = false) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
		
			$this->db->from("member m");
			$this->db->join("data_member dm", "m.id_member = dm.id_member");
			$this->db->where("m.jenis_kepemilikan_akun", "personal");
			$this->db->where("dm.is_delete", "0");

			// filter
			if ($post['is_anggota'] != '') {
				$this->db->where("dm.is_anggota", $post['is_anggota']);
			}
			if (!empty($post['nik'])) {
				$this->db->where("dm.nik", $post['nik']);
			}
			if (!empty($post['nama_lengkap'])) {
				$this->db->like("dm.nama_lengkap", $post['nama_lengkap'], "both");
			}
			if (!empty($post['nomor_anggota'])) {
				$this->db
					->join("relasi_member_jenissimpanan rel", "rel.id_member = m.id_member")
					->group_by("rel.id_member")
					->like("rel.nomor_anggota", $post['nomor_anggota'], "both");
			}
			
			$orderColumn = array(
				2 => "dm.nama_lengkap",
				3 => "dm.agama",
				4 => "m.tanggal_bergabung",
				5 => "dm.jenis_kelamin",
			);
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by('m.id_member', $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

		$this->db->stop_cache();

		// get num rows
		$this->db->select('m.id_member');
		$rowCount = $this->db->get()->num_rows();

		// get result
		$this->db->select('
			m.id_member,
			m.tanggal_bergabung,
			dm.is_anggota,
			dm.id_data_member,
			dm.nama_lengkap,
			dm.nama_panggilan,
			dm.nik,
			dm.agama,
			dm.jenis_kelamin,
		');

		$this->db->limit($post['length'], $post['start']);

		$val = $this->db->get()->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		$jenis_kelamin = array(
			"L" => "LAKI-LAKI",
			"P" => "PEREMPUAN"
		);
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			// $btnAksi .= "
			// <li>
			// 	<a href='{$base}page/detail/$data->slug' id='btn-detail' target='_blank'>
			// 		Detail
			// 	</a>
			// </li>
			// ";
			
			$btnAksi .= "
			<li>
				<a href='{$base}member/form/$data->id_data_member' id='btn-detail'>
					EDIT
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_data_member' id='btn-delete'>
					HAPUS
				</a>
			</li>
			";

			// $btnAksi .= "
			// 	<li><hr style='margin-top: 5px; margin-bottom: 5px;'></li>
			// 	<li>
			// 		<a ". (($data->is_anggota) ? "" : "style='color: gray'") ." href='". base_url("member/administrasi/$data->id_member") ."'>
			// 			ADMINISTRASI ANGGOTA
			// 		</a>
			// 	</li>
			// 	<li><hr style='margin-top: 5px; margin-bottom: 5px;'></li>
			// 	<li>
			// 		<a href='". base_url("simpanan/$data->id_member") ."'>
			// 			SIMPANAN
			// 		</a>
			// 	</li>
			// 	<li>
			// 		<a href='". base_url("simpanan/bunga/$data->id_member") ."'>
			// 			BUNGA SIMPANAN
			// 		</a>
			// 	</li>
			// 	<li><hr style='margin-top: 5px; margin-bottom: 5px;'></li>
			// 	<li>
			// 		<a href='". base_url("pinjaman/$data->id_member") ."'>
			// 			PINJAMAN
			// 		</a>
			// 	</li>
			// ";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
				<a href='{$base}member/detail/$data->id_member' class='btn btn-default'>DETAIL</a>
			</div>
			";
			
			$baris = array(
				"no" => $no,
				"id_member" => $data->id_member,
				"is_anggota" => ($data->is_anggota == '1') ? "ANGGOTA" : "BUKAN ANGGOTA",
				"aksi" => $aksi,
				"nama_lengkap" => strtoupper($data->nama_lengkap) . " <b>(". strtoupper($data->nama_panggilan) .")</b> <br /><b>$data->nik</b>",
				"agama" => strtoupper($data->agama),
				"tanggal_bergabung" => konversi_tanggal($data->tanggal_bergabung, "j F Y") . "<br /><small>". konversi_tanggal($data->tanggal_bergabung, "H:i") ."</small>",
				"jenis_kelamin" => @$jenis_kelamin[$data->jenis_kelamin],
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert("member", array(
			"jenis_kepemilikan_akun" => "personal"
		));
		$id_member = $this->db->insert_id();
		
		if (!empty($this->input->post('jenis_simpanan'))) {
			$relasi = array();
			foreach ($this->input->post('jenis_simpanan') as $jenis) {
				array_push($relasi, array(
					"id_member" => $id_member,
					"id_jenis_simpanan" => $jenis,
				));
			}
			$this->db->insert_batch("relasi_member_jenissimpanan", $relasi);
		}
		
		$data['id_member'] = $id_member;
		$this->db->insert("data_member", $data);
		
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id, $id_member) {
		if (!empty($this->input->post('jenis_simpanan'))) {
			/*
			- colecting jenis simpanan yang di pilih sebelumnya dan jadikan array
			- jika post jenis simpanan tidak sama dengan yang dipilih maka tambahkan jenis baru
			- simpan post jenis simpanan kedalam array
			- jika jenis simpanan pada db tidak sama dengan post jeni simpanan maka update is delete ke 1 
			*/

			$sql = $this->db
				->select("id_jenis_simpanan")
				->where("id_member", $id_member)
				->get("relasi_member_jenissimpanan");

			$db_jenis_simpanan = array();
			foreach ($sql->result() as $d) {
				array_push($db_jenis_simpanan, $d->id_jenis_simpanan);
			}

			$post_jenis_simpanan = $this->input->post("jenis_simpanan");
			foreach ($post_jenis_simpanan as $d) {
				if (!in_array($d, $db_jenis_simpanan)) {
					$this->db->insert("relasi_member_jenissimpanan", array(
						"id_member" => $id_member,
						"id_jenis_simpanan" => $d,
					));
				}
			}

			foreach ($db_jenis_simpanan as $d) {
				if (!in_array($d, $post_jenis_simpanan)) {
					$this->db
						->where("id_member", $id_member)
						->where("id_jenis_simpanan", $d)
						->update("relasi_member_jenissimpanan", array(
							"is_delete" => 1
						));
				} else {
					$this->db
						->where("id_member", $id_member)
						->where("id_jenis_simpanan", $d)
						->update("relasi_member_jenissimpanan", array(
							"is_delete" => 0
						));
				}
			}
		}
		
		return $this->db
			->where("id_data_member", $id)
			->update("data_member", $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where("id_data_member", $id)
			->update("data_member", array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("dm.id_data_member", $id)
			->get("data_member dm");
	}
}