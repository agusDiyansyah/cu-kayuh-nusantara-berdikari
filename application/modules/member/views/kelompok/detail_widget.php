<span style="font-weight: 600">
	DATA KELOMPOK
	<a href="<?php echo base_url("member/kelompok/edit_detail/$kelompok->id_data_kelompok/$kelompok->id_member") ?>" class="pull-right btn-edit-kelompok">EDIT</a>
</span>
<hr style="margin: 10px 0px;">

<b>No Akta</b>
<p><?php echo $kelompok->no_akta ?></p>

<b>Nama Kelompok</b>
<p><?php echo $kelompok->nama_kelompok ?></p>

<b>Tanggal Berdiri</b>
<p><?php echo $kelompok->tanggal_berdiri ?></p>

<b>Bidang Usaha</b>
<p><?php echo $kelompok->bidang_usaha ?></p>


<br>
<span style="font-weight: 600">KONTAK DAN ALAMAT</span>
<hr style="margin: 10px 0px;">

<b>Telephone</b>
<p><?php echo $kelompok->telp ?></p>

<b>Email</b>
<p><?php echo $kelompok->email ?></p>

<b>Alamat</b>
<p><?php echo $kelompok->alamat ?></p>

<br>
<span style="font-weight: 600">
	<a href="<?php echo base_url("member/kelompok_anggota/daftar/$kelompok->id_data_kelompok") ?>" target="_blank">
		LIST ANGGOTA
	</a>
</span>
<hr style="margin: 10px 0px;">

<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Nama Anggota</th>
			<th>Jabatan</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		$jabatan = array(
			"1" => "KETUA",
			"2" => "SEKRETARIS",
			"3" => "BENDAHARA",
			"4" => "ANGGOTA",
		);
		foreach ($anggota as $data) {
			$no++;
			echo "
			<tr>
				<td>$no</td>
				<td>$data->nama</td>
				<td>{$jabatan[$data->jabatan]}</td>
			</tr>
			";
		}
		?>
	</tbody>
</table>