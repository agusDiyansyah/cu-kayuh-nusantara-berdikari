<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-left">
				<a href="<?php echo base_url("member") ?>" class="btn btn-default btn-back">Kembali</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-3">
			<?php echo $detail ?>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-9">
			<div class="row">
				<div class="col-xs-12">
					<span style="font-weight: 600">BUNGA SIMPANAN</span>
					<hr style="margin: 10px 0px;">
					
					<div class="table-responsive">
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th class='aksi'>Aksi</th>
									<th style="text-align: right">Tanggal Input Terakhir</th>
									<th style="text-align: right">Total Bunga Simpanan</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td class="aksi">
										<a href="<?php echo base_url("simpanan/bunga/kelompok/$id_member") ?>" class="btn btn-default btn-xs">LIST DATA BUNGA SIMPANAN</a>
									</td>
									<td style="text-align: right">
										<?php echo (empty($bunga_simpanan->tanggal)) ? "-" : konversi_tanggal($bunga_simpanan->tanggal, "j F Y") ?>
									</td>
									<td style="text-align: right">
										<?php echo (empty($bunga_simpanan->jumlah)) ? "0,00" : number_format($bunga_simpanan->jumlah, 2, ",", ".") ?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<span style="font-weight: 600">
						SIMPANAN KELOMPOK
					</span>
					<hr style="margin: 10px 0px;">

					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th colspan="2">LUMBUNG</th>
									<th class="text-right">
										<div class="aksi">
											<div class="btn-group btn-group-xs">
												<button class="btn btn-default" disabled style="background-color: #878787; color: #F8F8F8">NOMOR BA</button>
												<a 
													href="" 
													class="btn btn-default btn-xs btn-ba" 
													data-id_member="<?php echo $id_member ?>"
													data-id_jenis_simpanan="9"
													data-ba="<?php echo $nomor_anggota ?>"
												>
													<?php echo empty($nomor_anggota) ? "EDIT NOMOR BUKU ANGGOTA" : $nomor_anggota ?>
												</a> &nbsp
											</div>
											<a href="<?php echo base_url("simpanan/kelompok/transaksi/$id_member") ?>" class="btn btn-default btn-xs">SIMPANAN</a> &nbsp
											<a href="<?php echo base_url("simpanan/penarikan/kelompok/$id_member") ?>" class="btn btn-default btn-xs">PENARIKAN</a>
										</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Total Simpanan</td>
									<td class="text-right"><?php echo @$simpanan['tanggal'] ?></td>
									<td class="text-right"><?php echo (empty($simpanan['jumlah'])) ? "0,00" : number_format($simpanan['jumlah'], 2, ',', '.') ?></td>
								</tr>
								<tr>
									<td>Penarikan</td>
									<td class="text-right"><?php echo @$penarikan['tanggal'] ?></td>
									<td class="text-right"><?php echo (empty($penarikan['jumlah'])) ? "0,00" : number_format($penarikan['jumlah'], 2, ',', '.') ?></td>
								</tr>
								<tr>
									<th>Sisa</th>
									<th></th>
									<th class="text-right">
										<?php $sisa = $simpanan['jumlah'] - $penarikan['jumlah']; ?>
										<?php echo (empty($sisa)) ? "0,00" : number_format($sisa, 2, ',', '.') ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<span style="font-weight: 600">
						PINJAMAN KELOMPOK
						<a href="<?php echo base_url("pinjaman/kelompok/form/$id_member") ?>" class="btn btn-default btn-xs aksi pull-right">TAMBAH PINJAMAN</a>
					</span>
					<hr style="margin: 10px 0px;">

					<div class="table-responsive" style="min-height: 400px !important;">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Produk Pinjaman</th>
									<th>Tanggal Meminjam</th>
									<th>Jatuh Tempo Tanggal</th>
									<th style="text-align: right">Pinjaman</th>
									<th style="text-align: right">Angsuran</th>
									<th style="text-align: right">Bunga</th>
									<th style="text-align: right">Denda</th>
									<th style="text-align: right">Sisa</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no 
								= $sum_pinjaman
								= $sum_angsuran
								= $sum_bunga
								= $sum_denda
								= $sum_sisa
								= 0;
								foreach ($pinjaman->result() as $p) {
									$no++;
									$tanggal_meminjam = konversi_tanggal($p->tanggal_meminjam, "j F Y");
									$pinjaman = number_format($p->pinjaman, 2, ',', '.');
									$nama_produk = strtoupper($p->nama_produk);
									$no_pinjaman = strtoupper($p->no_pinjaman);
									$jumlah_pinjaman = number_format($p->pinjaman, 2, ",", ".");

									$angsuran = (empty($p->angsuran)) ? "0,00" : number_format($p->angsuran, 2, ',', '.');
									$bunga = (empty($p->bunga)) ? "0,00" : number_format($p->bunga, 2, ',', '.');
									$denda = (empty($p->denda)) ? "0,00" : number_format($p->denda, 2, ',', '.');
									$sisa = $p->pinjaman - $p->angsuran;
									$sum_pinjaman += $p->pinjaman;
									$sum_angsuran += $p->angsuran;
									$sum_bunga += $p->bunga;
									$sum_denda += $p->denda;
									$sum_sisa += $sisa;
									$sisa = (empty($sisa)) ? "0,00" : number_format($sisa, 2, ',', '.');

									$dropup = "";
									if ($no > 5) {
										$dropup = "dropup";
									}

									$aksi = "
									<div class='btn-group $dropup'>
										<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
											<i class='fa fa-gear'></i>
										</button>
										<ul class='dropdown-menu'>
											<li>
												<a href='". base_url("pinjaman/kelompok/form/$p->id_member/$p->id_pinjaman") ."' class='btn-edit'>
													Edit
												</a>
											</li>
											<li>
												<a href='#' data-id='$p->id_pinjaman' class='btn-delete-pinjaman'>
													Hapus
												</a>
											</li>
											<li><hr style='margin-top: 5px; margin-bottom: 5px;'></li>
											<li>
												<a href='". base_url("pinjaman/angsuran/kelompok/$p->id_pinjaman") ."' class='btn-angsuran'>
													Angsuran
												</a>
											</li>
										</ul>
									</div>
									";

									$tanggal_angsuran = konversi_tanggal($p->tanggal_angsuran, "j F Y");

									echo "
									<tr>
										<td>$no</td>
										<td>
											<div style='position: relative'>
												<div style='position: absolute; top: 2px;'>$aksi</div>
												<div style='margin-left: 45px;'>
													<b>$nama_produk</b> (<b><a href='". base_url("pinjaman/angsuran/kelompok/$p->id_pinjaman") ."'>$p->jumlah_transaksi_angsuran</a></b> / $p->jangka_waktu) <br>
													<b style='margin-top: 6px;'>$no_pinjaman</b>
												</div>
											</div>
										</td>
										<td>$tanggal_meminjam</td>
										<td>$p->tanggal_jatuhtempo</td>
										<td align='right'>$jumlah_pinjaman</td>
										<td align='right'>$angsuran</td>
										<td align='right'>$bunga</td>
										<td align='right'>$denda</td>
										<td align='right'>$sisa</td>
									</tr>
									";
								}
								?>
								<tr>
									<th>TOTAL</th>
									<th></th>
									<th></th>
									<th></th>
									<th class="text-right"><?php echo (empty($sum_pinjaman)) ? "0,00" : number_format($sum_pinjaman, 2, ',', '.') ?></th>
									<th class="text-right"><?php echo (empty($sum_angsuran)) ? "0,00" : number_format($sum_angsuran, 2, ',', '.') ?></th>
									<th class="text-right"><?php echo (empty($sum_bunga)) ? "0,00" : number_format($sum_bunga, 2, ',', '.') ?></th>
									<th class="text-right"><?php echo (empty($sum_denda)) ? "0,00" : number_format($sum_denda, 2, ',', '.') ?></th>
									<th class="text-right"><?php echo (empty($sum_sisa)) ? "0,00" : number_format($sum_sisa, 2, ',', '.') ?></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wrp-modal">
	<div class="modal fade" tabindex="-1" role="dialog" aria-hidden='true'>
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary modal-proses">Proses</button>
				</div>
			</div>
		</div>
	</div>
</div>