$(document).ready(function() {
	$(".btn-delete-pinjaman").on('click', function(event) {
		event.preventDefault();
		var id = $(this).data("id");

		swal({
			title: "Apakah anda yakin",
			text: "Anda akan menghapus data ini",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#f4516c",
			cencelButtonColor: "#eaeaea",
			confirmButtonText: "Hapus Data",
			cancelButtonText: "Batalkan",
			showLoaderOnConfirm: true,
			closeOnConfirm: true,
			closeOnCancel: true
		},
		function (isConfirm) {
			if (isConfirm) {
				$.ajax({
					url: "<?php echo base_url("pinjaman/kelompok/delete_proses") ?>",
					type: "post",
					dataType: "json",
					data: { id: id },
					success: function (json) {
						if (json.stat) {
							notif(true, "Data berhasil di hapus");
							location.reload();
						} else {
							notif(false, "Data gagal di hapus");
						}
					}
				});
			}
		});
	});

	$(".btn-ba").on('click', function(event) {
		event.preventDefault();
		var m = $(".modal");
		var m_clone = m.clone();
		var form = "";
		var ba = $(this).data('ba');
		var id_member = $(this).data('id_member');
		var id_jenis_simpanan = $(this).data('id_jenis_simpanan');

		form += '	<div class="form-group">';
		form += '		<input type="text" class="form-control nomor_anggota" name="nomor_anggota" value="'+ ba +'">';
		form += '	</div>';

		m.find('.modal-title').html("No Buku Anggota");
		m.find('.modal-body').html(form);

		m.modal();

		m.find('.modal-proses').on('click', function(event) {
			event.preventDefault();
			var no_ba = m.find('.nomor_anggota').val();
			$.ajax({
				url: '<?php echo base_url('simpanan/edit_ba') ?>',
				type: 'post',
				dataType: 'json',
				data: {
					ba: no_ba,
					id_member: id_member,
					id_jenis_simpanan: id_jenis_simpanan,
				},
				success: function (json) {
					location.reload();
					// m.modal("hide");
				}
			});
		});

		// m.on('hidden.bs.modal', function(e) { 
		// 	m.remove();
		// 	$(".wrp-modal").html(m_clone);
		// });
	});
});
