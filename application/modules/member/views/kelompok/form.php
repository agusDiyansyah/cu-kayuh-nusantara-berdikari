<?php echo $this->session->flashdata('msg'); ?>

<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="<?php echo $link_back ?>" class="btn btn-default btn-back btn-back">Kembali</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<form class="form" action="<?php echo $form_action ?>" method="post">
		<div class="hide">
			<?php echo $input['hide']['id'] ?>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Nama Kelompok</label>
					<?php echo $input['nama_kelompok'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Bidang Usaha</label>
					<?php echo $input['bidang_usaha'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Nomor Akta</label>
					<?php echo $input['no_akta'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-3">
				<div class="form-group">
					<label for="" class="control-label">Tangal Berdiri</label>
					<?php echo $input['tanggal_berdiri'] ?>
				</div>
			</div>
			
			<!-- <div class="col-xs-12 col-sm-3">
				<div class="form-group">
					<label for="" class="control-label">Jumlah Anggota</label>
					<?php // echo $input['jumlah_anggota'] ?>
				</div>
			</div> -->
			
			<div class="col-xs-12 col-sm-3">
				<div class="form-group">
					<label for="" class="control-label">Nomor Telepon</label>
					<?php echo $input['telp'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-3">
				<div class="form-group">
					<label for="" class="control-label">Email</label>
					<?php echo $input['email'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Alamat</label>
					<?php echo $input['alamat'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="pull-right">
					<a href="<?php echo $link_back ?>" class="btn btn-default btn-back">Kembali</a>
					<button type="submit" name="button" class="btn btn-success">Proses</button>
				</div>
			</div>
		</div>
	</form>
</div>