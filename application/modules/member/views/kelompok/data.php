<?php echo $this->session->flashdata('msg'); ?>

<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="" class="btn btn-default btn-tool-src">Pencarian Rinci</a>
				<a href="<?php echo $link_add ?>" class="btn btn-success btn-add">Tambah Data</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid pencarian" style="display: none;">
	<div class="row">
		<div class="col-xs-12">
			<form action="" method="POST" role="form" class="form-filter">

				<div class="hide">
					<?php echo $filter['hide']['is_anggota'] ?>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Nama Kelompok</label>
							<?php echo $filter['nama_kelompok'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Bindang Usaha</label>
							<?php echo $filter['bidang_usaha'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Telp</label>
							<?php echo $filter['telp'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Email</label>
							<?php echo $filter['email'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Nomor Anggota</label>
							<?php echo $filter['nomor_anggota'] ?>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-success btn-cari">Cari</button>
				<button type="button" class="btn btn-default btn-reset">Reset</button>
			</form>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<!-- <div class="table-responsive"> -->
			<div class="col-xs-12">
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th width="2%">#</th>
							<th>Aksi</th>
							<th>Nama Kelompok</th>
							<th>Bidang Usaha</th>
							<th></th>
							<th>Telephone</th>
							<th>Email</th>
							<th>Tanggal Berdiri</th>
							<th>Tanggal Bergabung</th>
						</tr>
					</thead>
				</table>
			</div>
		<!-- </div> -->
	</div>
</div>