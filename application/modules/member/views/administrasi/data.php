<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-left">
				<a href="<?php echo base_url("member") ?>" class="btn btn-default btn-back">Kembali</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-3">
			<?php echo $detail ?>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-9">
			<?php echo $adm ?>
		</div>
	</div>
</div>