<div class="row">
	<div class="col-xs-12">
		<span style="font-weight: 600">
			ADMINISTRASI ANGGOTA 
			<small class="pull-right" style="color: #b5b5b5">KHUSUS ANGGOTA</small>
		</span>
		<hr style="margin: 10px 0px;">

		<div class="table-responsive">
			<table class="table table-hover table-striped" style="margin-bottom: 5px;">
				<thead>
					<tr>
						<th>#</th>
						<th class="aksi">Aksi</th>
						<th>Jenis Administrasi</th>
						<th>Tanggal Input Terakhir</th>
						<th style="text-align: right">Jumlah</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$no 
						= $sum_adm
						= 0;
						foreach ($adm->result() as $data) {
							$no++;

							$jenis_adm = strtoupper($data->jenis_adm);

							if ($is_anggota) {
								$aksi = "<a class='btn btn-default btn-xs' href='". base_url("member/administrasi/angsuran/$data->id_jenis_adm/$id_member") ."'>ANGSURAN</a>";
							} else {
								$aksi = "<span class='btn btn-xs btn-default' style='color: #cfd5d5'>ANGSURAN</span>";
							}

							$jumlah_adm = (empty($data->jumlah))
								? 0
								: $data->jumlah;

							$sum_adm += $jumlah_adm;

							$jumlah_adm = number_format($jumlah_adm, 0, ",", ".");

							$tanggal_adm = (empty($data->tanggal))
								? "-"
								: konversi_tanggal($data->tanggal, "j F Y");

							echo "
							<tr>
								<td>$no</td>
								<td class='aksi'>$aksi</td>
								<td>$jenis_adm</td>
								<td>$tanggal_adm</td>
								<td align='right'>$jumlah_adm</td>
							</tr>
							";
						}
					?>
					<tr>
						<th>TOTAL</th>
						<th class='aksi'></th>
						<th></th>
						<th></th>
						<th style="text-align: right"><?php echo number_format($sum_adm, 0, ",", "."); ?></th>
					</tr>
				</tbody>
			</table>
		</div>

		<small class="text-danger" style="opacity: .5; margin-bottom: 20px; display: block;">
			<em>
				Jika terdapat total dana administrasi untuk member non-anggota, maka kemungkinan sebelumnya member pernah bergabung sebagai anggota dan telah menyetorkan biaya administrasi
			</em>
		</small>
	</div>
</div>