$(document).ready(function() {
	 $(".mn-kelompok").addClass('active');

	 if ($(".is_member").val()) {
	 	$(".nama").prop('readonly', 'true');
	 }
	 
	 $(".date").datepicker({
		 "autoclose": true,
		 "format": "yyyy-mm-dd"
	 });

	 $(".btn-cek-nik").on('click', function(event) {
	 	event.preventDefault();
	 	var id_data_kelompok = $(".id_data_kelompok").val();
	 	var nik = $(".nik").val();
	 	var url = "<?php echo base_url("member/kelompok_anggota/detail_nik/") ?>" + id_data_kelompok+"/"+nik;
	 	window.open(url, "_blank");
	 });

	 $('.nik').select2({
		placeholder : '',
	    minimumInputLength : 2,
	  	ajax: {
		    url : '<?php echo base_url('member/kelompok_anggota/cekMemberByNIK') ?>',
		    dataType : 'json',
		    type : 'POST',
		    delay : 250,
		    data: function (params) {
			    return {
			        q : params.term
			    };
	    	},
		    processResults: function (json) {
			    return {
			       	results: json
			    };
		    },
		    cache: false,
		    initSelection: function(element, callback) {
		    	alert();
            	if (data) {
            		return callback({id: data, text: data});
            	}
            }
	  	}
	});

	$('.nik').on('select2:select', function (e) {
	    var data = e.params.data;
	    if (data.id == data.text) {
	    	$(".nama").val('');
	    	$(".jenis_kelamin").val('');
			$(".no_telp").val('');

	    	$(".nama").removeAttr('readonly');
	    } else {
			$(".nama").val(data.nama);
			$(".jenis_kelamin").val(data.jenis_kelamin);
			$(".no_telp").val(data.telp_pribadi);

			$(".nama").prop('readonly', 'true');
	    }
	});
});
