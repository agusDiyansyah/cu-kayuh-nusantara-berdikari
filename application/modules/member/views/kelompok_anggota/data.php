<?php echo $this->session->flashdata('msg'); ?>

<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-left">
				<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali ke List Data Kelompok</a>
			</div>
			<div class="pull-right">
				<a href="<?php echo $link_add ?>" class="btn btn-success btn-add">Tambah Data</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<span style="padding: 3px 0; display: block">
				<span style="font-weight: 600"><?php echo strtoupper($nama_kelompok) ?></span> / LIST ANGGOTA
			</span>
			<hr style="margin: 10px 0px;">
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="table-responsive">
			<div class="col-xs-12">
				<table class="table table-hover table-striped" data-id_data_kelompok="<?php echo $id_data_kelompok ?>">
					<thead>
						<tr>
							<th width="2%">#</th>
							<th width="2%">Aksi</th>
							<th>Nama</th>
							<th>Jabatan</th>
							<th>Nomor Telepon</th>
							<th>Email</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>