<?php echo $this->session->flashdata('msg'); ?>

<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<form class="form" action="<?php echo $form_action ?>" method="post">
		<div class="hide">
			<?php echo $input['hide']['id'] ?>
			<?php echo $input['hide']['id_data_kelompok'] ?>
			<?php echo $input['hide']['is_member'] ?>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<span style="padding: 3px 0; display: block">
					<span style="font-weight: 600"><?php echo strtoupper($nama_kelompok) ?></span> / FORM ANGGOTA 
				</span>
				<hr style="margin: 10px 0px;">
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">NIK</label>
					<?php echo $input['nik'] ?>
					<small style="margin-top: 5px; display: block" class="text-right">
						<a href="#" class="btn-cek-nik">CEK DATA</a>
					</small>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Nama</label>
					<?php echo $input['nama'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Jenis Kelamin</label>
					<?php echo $input['jenis_kelamin'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Nomor Telepon</label>
					<?php echo $input['no_telp'] ?>
				</div>
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Email</label>
					<?php echo $input['email'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Jabatan</label>
					<?php echo $input['jabatan'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Alamat</label>
					<?php echo $input['alamat'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="pull-right">
					<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
					<button type="submit" name="button" class="btn btn-success">Proses</button>
				</div>
			</div>
		</div>
	</form>
</div>