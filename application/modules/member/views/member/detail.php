<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-left">
				<a href="<?php echo base_url("member") ?>" class="btn btn-default btn-back">Kembali</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-3">
			<?php echo $detail ?>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-9">
			<?php echo $adm ?>
			
			<div class="row">
				<div class="col-xs-12">
					<span style="font-weight: 600">BUNGA SIMPANAN</span>
					<hr style="margin: 10px 0px;">
					
					<div class="table-responsive">
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th class='aksi'>Aksi</th>
									<th style="text-align: right">Tanggal Input Terakhir</th>
									<th style="text-align: right">Total Bunga Simpanan</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td class="aksi">
										<a href="<?php echo base_url("simpanan/bunga/$id_member") ?>" class="btn btn-default btn-xs">LIST DATA BUNGA SIMPANAN</a>
									</td>
									<td style="text-align: right">
										<?php echo (empty($bunga_simpanan->tanggal)) ? "-" : konversi_tanggal($bunga_simpanan->tanggal, "j F Y") ?>
									</td>
									<td style="text-align: right">
										<?php echo (empty($bunga_simpanan->jumlah)) ? "0,00" : number_format($bunga_simpanan->jumlah, 2, ",", ".") ?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<span style="font-weight: 600">SIMPANAN ANGGOTA</span>
					<hr style="margin: 10px 0px;">
					
					<div class="table-responsive">
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th class="aksi">Aksi</th>
									<th>Jenis Simpanan</th>
									<th>Tanggal Input Terakhir</th>
									<th style="text-align: right">Simpanan</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no 
								= $sum_simpanan 
								= 0;
								foreach ($jenis_simpanan->result() as $s) {
									if ($s->id_kategori_simpanan != 1) {
										continue;
									}

									$no++;
									if ($is_anggota) {
										$aksi = "<a class='btn btn-default btn-xs' href='". base_url("simpanan/anggota/transaksi/$s->id_jenis_simpanan/$id_member") ."'>TRANSAKSI</a>";
									} else {
										$aksi = "<span class='btn btn-xs btn-default' style='color: #cfd5d5'>TRANSAKSI</span>";
									}
									
									$jumlah_simpanan = (empty($simpanan[$s->id_jenis_simpanan]['jumlah'])) ? 0 : $simpanan[$s->id_jenis_simpanan]['jumlah'];
									$sum_simpanan += $jumlah_simpanan;
									$jumlah_simpanan = number_format($jumlah_simpanan, 2, ",", ".");
									$tanggal = (empty($simpanan[$s->id_jenis_simpanan]['tanggal'])) ? "-" : konversi_tanggal($simpanan[$s->id_jenis_simpanan]['tanggal'], "j F Y");
									
									$jenis = strtoupper($s->jenis);

									$no_ba = "";
									$nomor_anggota = @$relasi_member_jenissimpanan[$s->id_jenis_simpanan]['nomor_anggota'];
									if ($is_anggota) {
										if ($s->id_jenis_simpanan == 3) {
											$no_ba = (empty($nomor_anggota))
												? "<br><a href='' class='btn-ba' data-ba='$nomor_anggota' data-id_member='$id_member' data-id_jenis_simpanan='$s->id_jenis_simpanan'>EDIT NO BUKU ANGGOTA</a>"
												: "<br><a href='' class='btn-ba' data-ba='$nomor_anggota' data-id_member='$id_member' data-id_jenis_simpanan='$s->id_jenis_simpanan'>$nomor_anggota</a>";
										}
									} else {
										if ($s->id_jenis_simpanan == 3) {
											$no_ba = (empty($nomor_anggota))
												? ""
												: "<br>$nomor_anggota";
										}
									}
									
									echo "
									<tr>
										<td>$no</td>
										<td class='aksi'>
											$aksi
										</td>
										<td>
											$jenis 
											$no_ba
										</td>
										<td>$tanggal</td>
										<td align='right'>$jumlah_simpanan</td>
									</tr>
									";
								}
								?>
								<tr>
									<th>TOTAL</th>
									<th class="aksi"></th>
									<th></th>
									<th></th>
									<th style="text-align: right"><?php echo number_format($sum_simpanan, 2, ",", ".") ?></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<span style="font-weight: 600">SIMPANAN NON-ANGGOTA</span>
					<hr style="margin: 10px 0px;">

					<div class="table-responsive">
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th class='aksi'>Aksi</th>
									<th>Jenis Simpanan</th>
									<th>No Buku Anggota</th>
									<th style="text-align: right">Simpanan</th>
									<th style="text-align: right">Penarikan</th>
									<th style="text-align: right">Sisa</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no 
									= $sum_simpanan 
									= $sum_penarikan
									= $sum_sisa
									= 0;
									foreach ($jenis_simpanan->result() as $s) {
										if ($s->id_kategori_simpanan != 2) {
											continue;
										}
										
										$no++;
										if (in_array($s->id_jenis_simpanan, $relasi_jenis_simpanan) AND $relasi_member_jenissimpanan[$s->id_jenis_simpanan]['is_delete'] == "0" AND !$is_anggota) {
											$aksi = "<a class='btn btn-default btn-xs' href='". base_url("simpanan/anggota_non/transaksi/$s->id_jenis_simpanan/$id_member") ."'>TRANSAKSI</a>";
										} else {
											$aksi = "<span class='btn btn-xs btn-default' style='color: #cfd5d5'>TRANSAKSI</span>";
										}
										
										$jumlah_simpanan = (empty($simpanan[$s->id_jenis_simpanan]['jumlah'])) ? 0 : $simpanan[$s->id_jenis_simpanan]['jumlah'];
										$jumlah_penarikan = (empty($penarikan[$s->id_jenis_simpanan]['jumlah'])) ? 0 : $penarikan[$s->id_jenis_simpanan]['jumlah'];
										$jumlah_sisa = $jumlah_simpanan - $jumlah_penarikan;
										
										// if ($jumlah_simpanan > 0) {
											$aksi .= "&nbsp<a class='btn btn-default btn-xs' href='". base_url("simpanan/penarikan/transaksi/$s->id_jenis_simpanan/$id_member") ."'>TARIK DANA</a>";
										// } else {
										// 	$aksi .= "&nbsp<span class='btn btn-xs btn-default' style='color: #cfd5d5'>TARIK DANA</span>";
										// }

										$danger_minus = "";
										if ($jumlah_sisa < 0) {
											$danger_minus = "style='background-color: #ffcdcd'";
										}
										
										$sum_simpanan += $jumlah_simpanan;
										$sum_penarikan += $jumlah_penarikan;
										$sum_sisa = $sum_simpanan - $sum_penarikan;

										$jumlah_simpanan = number_format($jumlah_simpanan, 2, ",", ".");
										$jumlah_penarikan = number_format($jumlah_penarikan, 2, ",", ".");
										$jumlah_sisa = number_format($jumlah_sisa, 2, ",", ".");

										$tanggal_simpanan = (empty($simpanan[$s->id_jenis_simpanan]['tanggal'])) 
											? "-" 
											: konversi_tanggal($simpanan[$s->id_jenis_simpanan]['tanggal'], "j F Y");

										$tanggal_penarikan = (empty($penarikan[$s->id_jenis_simpanan]['tanggal'])) 
											? "-" 
											: konversi_tanggal($penarikan[$s->id_jenis_simpanan]['tanggal'], "j F Y");

										$jenis = strtoupper($s->jenis);

										$ba = "";
										$ba_title = (empty($relasi_member_jenissimpanan[$s->id_jenis_simpanan]['nomor_anggota'])) ? "-" : $relasi_member_jenissimpanan[$s->id_jenis_simpanan]['nomor_anggota'];
										if (in_array($s->id_jenis_simpanan, $relasi_jenis_simpanan) AND $relasi_member_jenissimpanan[$s->id_jenis_simpanan]['is_delete'] == "0") {
											$ba_title = ($ba_title == "-") ? "" : $ba_title;
											$ba__title = ($ba_title == "") ? "EDIT NO BUKU ANGGOTA" : $ba_title;
											$ba = "
												<a href=''
													class='btn-ba' 
													data-id_member='$id_member'
													data-id_jenis_simpanan='$s->id_jenis_simpanan'
													data-ba='$ba_title'
												>$ba__title</a>
											";
										} else {
											$ba = "<span>$ba_title</span>";
										}
										
										echo "
										<tr $danger_minus>
											<td>$no</td>
											<td class='aksi'>$aksi</td>
											<td>$jenis</td>
											<td>$ba</td>
											<td align='right'>
												$jumlah_simpanan <br />
												<small style='font-size: 10px'>$tanggal_simpanan</small>
											</td>
											<td align='right'>
												$jumlah_penarikan <br />
												<small style='font-size: 10px'>$tanggal_penarikan</small>
											</td>
											<td align='right'>
												$jumlah_sisa
											</td>
										</tr>
										";
									}
								?>
								<tr>
									<th>TOTAL</th>
									<th class='aksi'></th>
									<th></th>
									<th></th>
									<th style="text-align: right"><?php echo number_format($sum_simpanan, 2, ",", ".") ?></th>
									<th style="text-align: right"><?php echo number_format($sum_penarikan, 2, ",", ".") ?></th>
									<th style="text-align: right"><?php echo number_format($sum_sisa, 2, ",", ".") ?></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<span style="font-weight: 600">
						PINJAMAN
						<span class="pull-right">
							<?php if ($is_anggota): ?>
								<a href="<?php echo base_url("pinjaman/form/$id_member") ?>" class="btn btn-default btn-xs aksi">TAMBAH PINJAMAN</a>
							<?php else: ?>
								<span style="color: #BFC5C6; font-weight: 500;">HANYA ANGGOTA YANG DAPAT MELAKUKAN PINJAMAN DANA</span>
							<?php endif ?>
						</span>
					</span>
					<hr style="margin: 10px 0px;">
					
					<div class="table-responsive" style="min-height: 400px;">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Produk Pinjaman</th>
									<th>Tanggal Meminjam</th>
									<th>Jatuh Tempo Tanggal</th>
									<th style="text-align: right">Pinjaman</th>
									<th style="text-align: right">Angsuran</th>
									<th style="text-align: right">Bunga</th>
									<th style="text-align: right">Denda</th>
									<th style="text-align: right">Sisa</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no
								= $sum_pinjaman 
								= $sum_angsuran 
								= $sum_bunga
								= $sum_denda
								= $sum_sisa
								= 0;
								foreach ($pinjaman->result() as $p) {
									$no++;

									$nama_produk = strtoupper($p->nama_produk);
									$no_pinjaman = strtoupper($p->no_pinjaman);
									$tanggal_meminjam = konversi_tanggal($p->tanggal_meminjam, "j F Y");

									// jumlah pinjaman
									$jumlah_pinjaman = number_format($p->jumlah_pinjaman, 2, ",", ".");
									$sum_pinjaman += $p->jumlah_pinjaman;

									// angsuran
									$angsuran = number_format($p->angsuran, 2, ",", ".");
									$sum_angsuran += $p->angsuran;

									// jumlah pinjaman
									$bunga = number_format($p->bunga, 2, ",", ".");
									$sum_bunga += $p->bunga;

									// jumlah pinjaman
									$denda = number_format($p->denda, 2, ",", ".");
									$sum_denda += $p->denda;

									$sisa = $p->jumlah_pinjaman - $p->angsuran;

									$btnAksi = "";
									// if ($is_anggota) {
										$btnAksi .= "
											<li>
												<a href='". base_url("pinjaman/form/$id_member/$p->id_pinjaman") ."' class=''>EDIT</a>
											</li>
											<li>
												<a href='' data-id='$p->id_pinjaman' class='btn-hapus-pinjaman'>HAPUS</a>
											</li>
										";
									// }
									
									if ($sisa > 0) {
										$btnAksi .= "<li>
											<a href='". base_url("pinjaman/angsuran/transaksi/$p->id_pinjaman/$id_member") ."' class=''>ANGSURAN</a>
										</li>";
									} else {
										$btnAksi .= "
											<li>
												<span class=''>ANGSURAN</span>
											</li>
										";
									}

									$dropup = "";
									if ($no > 5) {
										$dropup = "dropup";
									}

									$aksi = "
									<div class='aksi btn-group $dropup'>
										<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
											<i class='fa fa-gear'></i>
										</button>
										<ul class='dropdown-menu'>
											$btnAksi
										</ul>
									</div>
									";
									
									$sum_sisa += $sisa;
									$sisa = number_format($sisa, 2, ",", ".");

									echo "
									<tr>
										<td>$no</td>
										<td>
											<div style='position: relative'>
												<div style='position: absolute; top: 2px;'>$aksi</div>
												<div style='margin-left: 45px;'>
													<b>$nama_produk</b> (<b><a href='". base_url("pinjaman/angsuran/transaksi/$p->id_pinjaman/$id_member") ."'>$p->jumlah_transaksi_angsuran</a></b> / $p->jangka_waktu) <br>
													<b style='margin-top: 6px;'>$no_pinjaman</b>
												</div>
											</div>
										</td>
										<td>$tanggal_meminjam</td>
										<td>$p->tanggal_jatuhtempo</td>
										<td align='right'>$jumlah_pinjaman</td>
										<td align='right'>$angsuran</td>
										<td align='right'>$bunga</td>
										<td align='right'>$denda</td>
										<td align='right'>$sisa</td>
									</tr>
									";
								}
								?>
								<tr>
									<th>TOTAL</th>
									<th></th>
									<th></th>
									<th></th>
									<th style="text-align: right"><?php echo number_format($sum_pinjaman, 2, ",", "."); ?></th>
									<th style="text-align: right"><?php echo number_format($sum_angsuran, 2, ",", "."); ?></th>
									<th style="text-align: right"><?php echo number_format($sum_bunga, 2, ",", "."); ?></th>
									<th style="text-align: right"><?php echo number_format($sum_denda, 2, ",", "."); ?></th>
									<th style="text-align: right"><?php echo number_format($sum_sisa, 2, ",", "."); ?></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wrp-modal">
	<div class="modal fade" tabindex="-1" role="dialog" aria-hidden='true'>
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary modal-proses">Proses</button>
				</div>
			</div>
		</div>
	</div>
</div>