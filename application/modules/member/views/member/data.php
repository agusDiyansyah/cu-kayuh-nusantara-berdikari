<?php echo $this->session->flashdata('msg'); ?>

<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="" class="btn btn-default btn-tool-src">Pencarian Rinci</a>
				<a href="<?php echo $link_add ?>" class="btn btn-success btn-add">Tambah Data</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid pencarian" style="display: none;">
	<div class="row">
		<div class="col-xs-12">
			<form action="" method="POST" role="form" class="form-filter">

				<div class="hide">
					<?php echo $filter['hide']['is_anggota'] ?>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Nama Anggota</label>
							<?php echo $filter['nama_lengkap'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">NIK</label>
							<?php echo $filter['nik'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">No Buku Anggota</label>
							<?php echo $filter['nomor_anggota'] ?>
						</div>
					</div>

					<div class="col-xs-12">
						<div class="form-group">
							<label for="">Status Keanggotaan</label>
							<div class="wrp-anggota">
								<div class="anggota true">
									ANGGOTA
								</div>
								<div class="anggota false">
									NONO-ANGGOTA
								</div>
							</div>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-success btn-cari">Cari</button>
				<button type="button" class="btn btn-default btn-reset">Reset</button>
			</form>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<!-- <div class="table-responsive"> -->
			<div class="col-xs-12">
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th width="2%">#</th>
							<th>Aksi</th>
							<th>Nama Lengkap / Panggilan / NIK</th>
							<th>Status Keanggotaan</th>
							<th>Agama</th>
							<th>Tanggal Bergabung</th>
							<th>Jenis Kelamin</th>
						</tr>
					</thead>
				</table>
			</div>
		<!-- </div> -->
	</div>
</div>