$(document).ready(function() {
	var is_anggota = $(".is_anggota").val();
	if (is_anggota == "1") {
		$(".anggota.true").addClass('active');
	} else {
		$(".anggota.false").addClass('active');
	}
	
	$(".anggota").on('click', function(event) {
		event.preventDefault();
		var val = $(this).data('val');
		
		$(".anggota").removeClass('active');
		$(this).addClass('active');
		
		$(".is_anggota").val(val);
	});
	 
	$(".date").datepicker({
		"autoclose": true,
		"format": "yyyy-mm-dd"
	});
	 
	$(".s2").select2({
		width: "100%"
	});
});