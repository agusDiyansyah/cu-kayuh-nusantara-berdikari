<?php echo $this->session->flashdata('msg'); ?>
<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<form class="form" action="<?php echo $form_action ?>" method="post">
		<div class="hide">
			<?php echo $input['hide']['id'] ?>
			<?php echo $input['hide']['is_anggota'] ?>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<span style="font-weight: 600">STATUS KEANGGOTAAN</span>
				<hr style="margin: 10px 0px;">
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<div class="anggota true" data-val="1">
					ANGGOTA
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="anggota false" data-val="0">
					BUKAN ANGGOTA
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Simpanan Non-Anggota</label>
					<?php echo $input['jenis_simpanan'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<span style="font-weight: 600">DATA PRIBADI MEMBER</span>
				<hr style="margin: 10px 0px;">
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="form-group">
					<label for="" class="control-label">Nama Lengkap</label>
					<?php echo $input['nama_lengkap'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="form-group">
					<label for="" class="control-label">Nama Panggilan</label>
					<?php echo $input['nama_panggilan'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="form-group">
					<label for="" class="control-label">Jenis Kelamin</label>
					<?php echo $input['jenis_kelamin'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="form-group">
					<label for="" class="control-label">NIK</label>
					<?php echo $input['nik'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-md-6">
				<div class="form-group">
					<label for="" class="control-label">Kota Dikeluarkannya KTP</label>
					<?php echo $input['kota_dikeluarkan_ktp'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="form-group">
					<label for="" class="control-label">Agama</label>
					<?php echo $input['agama'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-md-6">
				<div class="form-group">
					<label for="" class="control-label">Nama Ibu Kandung</label>
					<?php echo $input['nama_ibukandung'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="form-group">
					<label for="" class="control-label">Kota/Daerah Asal (Kelahiran)</label>
					<?php echo $input['daerah_asal'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-8">
				<div class="form-group">
					<label for="" class="control-label">Tanggal Lahir</label>
					<?php echo $input['tanggal_lahir'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Pendidikan Terakhir</label>
					<?php echo $input['pendidikan_terakhir'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<div class="form-group">
					<label for="" class="control-label">Status Pernikahan</label>
					<?php echo $input['status_pernikahan'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-8">
				<div class="form-group">
					<label for="" class="control-label">Jumlah Tanggungan</label>
					<?php echo $input['jumlah_tanggungan'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<span style="font-weight: 600">KONTAK DAN ALAMAT MEMBER</span>
				<hr style="margin: 10px 0px;">
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Nomor Telepon Pribadi</label>
					<?php echo $input['telp_pribadi'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Kode POS</label>
					<?php echo $input['kodepos'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Alamat Sekarang</label>
					<?php echo $input['alamat_sekarang'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<span style="font-weight: 600">PEKERJAAN</span>
				<hr style="margin: 10px 0px;">
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Pekerjaan</label>
					<?php echo $input['pekerjaan'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Jabatan</label>
					<?php echo $input['jabatan'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Nama Kantor</label>
					<?php echo $input['nama_kantor'] ?>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<label for="" class="control-label">Bidang Usaha</label>
					<?php echo $input['bidang_usaha'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Nomor Telepon Kantor</label>
					<?php echo $input['telp_kantor'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="" class="control-label">Alamat Kantor</label>
					<?php echo $input['alamat_kantor'] ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="pull-right">
					<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
					<button type="submit" name="button" class="btn btn-success">Proses</button>
				</div>
			</div>
		</div>
	</form>
</div>