<span style="font-weight: 600">
	DATA PRIBADI MEMBER
	<?php if ($show_edit): ?>
		<a href="<?php echo base_url("member/form/$detail->id_data_member") ?>" class="pull-right btn-edit-member">EDIT</a>
	<?php endif ?>
</span>
<hr style="margin: 10px 0px;">

<b>Nama Lengkap</b>
<p><?php echo strtoupper($detail->nama_lengkap) . " (". $detail->nama_panggilan .")" ?></p>

<b>NIK</b>
<p><?php echo $detail->nik ?></p>

<b>Status Keanggotaan</b>
<p><?php echo ($detail->is_anggota) ? "ANGGOTA" : "BUKAN ANGGOTA" ?></p>

<b>Kota Dikeluarkannya KTP</b>
<p><?php echo $detail->kota_dikeluarkan_ktp ?></p>

<b>Agama</b>
<p><?php echo $detail->agama ?></p>

<b>Nama Ibu Kandung</b>
<p><?php echo $detail->nama_ibukandung ?></p>

<b>Tempat dan Tanggal Lahir</b>
<p><?php echo $detail->daerah_asal . ", " . konversi_tanggal($detail->tanggal_lahir, "j F Y") ?></p>

<b>Jenjang Pendidikan Terakhir</b>
<p><?php echo $detail->pendidikan_terakhir ?></p>

<b>Status Pernikahan</b>
<p><?php echo ($detail->status_pernikahan == "0") ? "Belum" : "Sudah" ?></p>

<b>Jumlah Tanggungan</b>
<p><?php echo $detail->jumlah_tanggungan ?></p>

<br>
<span style="font-weight: 600">KONTAK DAN ALAMAT</span>
<hr style="margin: 10px 0px;">

<b>Nomor Telepon Pribadi</b>
<p><?php echo $detail->telp_pribadi ?></p>

<b>Kode POS</b>
<p><?php echo $detail->kodepos ?></p>

<b>Alamat Sekarang</b>
<p><?php echo $detail->alamat_sekarang ?></p>

<br>
<span style="font-weight: 600">PEKERJAAN</span>
<hr style="margin: 10px 0px;">

<b>Pekerjaan</b>
<p><?php echo $detail->pekerjaan ?></p>

<b>Jabatan</b>
<p><?php echo $detail->jabatan ?></p>

<b>Nama Kantor</b>
<p><?php echo $detail->nama_kantor ?></p>

<b>Bidang Usaha</b>
<p><?php echo $detail->bidang_usaha ?></p>

<b>Nomor Telepon Kantor</b>
<p><?php echo $detail->telp_kantor ?></p>

<b>Alamat Kantor</b>
<p><?php echo $detail->alamat_kantor ?></p>