<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends Admin_controller {
	private $stat = false;
	private $title = "Member";
	private $module = "member";
	private $module_view = "member/member";
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_member", "M_app");
		}
		
		public function index () {
			// datatables
			$this->output->js('assets/themes/admin/vendors/datatables/jquery.dataTables.js');
			$this->output->js('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js');
			$this->output->css('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');
			
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/data.js");
			$this->output->script_head("$this->module_view/_gudang/data.css");
			
			$this->session->unset_userdata("link_back");
			
			$data = array(
				"title" => $this->title,
				"link_add" => base_url("$this->module/form"),
				
				"filter" => array(
					"hide" => array(
						"is_anggota" => form_input(array(
							"name" => "is_anggota",
							"class" => "is_anggota",
							"type" => "text",
						))
					),
					"nama_lengkap" => form_input(array(
						"name" => "nama_lengkap",
						"class" => "form-control nama_lengkap",
						"type" => "text",
					)),
					"nik" => form_input(array(
						"name" => "nik",
						"class" => "form-control nik",
						"type" => "number",
					)),
					"nomor_anggota" => form_input(array(
						"name" => "nomor_anggota",
						"class" => "form-control nomor_anggota",
						"type" => "text",
					)),
				)
			);
			
			$this->load->view("$this->module_view/data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($_POST);
			}
			return;
		}
		
		public function detail ($id_member = 0) {
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/detail.js");
			
			$this->session->set_userdata("link_back", "member/detail/$id_member");
			
			// cek apakah member adalah anggota atau bukan
				$sql = $this->db
					->select("is_anggota")
					->where("id_member", $id_member)
					->get("data_member", 1)
					->row();

				$is_anggota = $sql->is_anggota;

			// list data jenis simpanan
				$sql_jenis_simpanan = $this->db
					->select("
						j.id_kategori_simpanan, j.id_jenis_simpanan, j.jenis
					")
					->where("j.id_jenis_simpanan !=", 9, true)
					->get("jenis_simpanan j");

			// list data jenis administrasi
				$sql_jenis_adm = $this->db->get("adm_jenis");

			// list data pinjaman
				$this->db->query("DROP TABLE IF EXISTS tmp_order_angsuran;");
				$this->db->query("
					CREATE TEMPORARY TABLE tmp_order_angsuran (
						SELECT
							a.id_angsuran,
							a.id_pinjaman,
							a.tanggal_angsuran, 
							a.jumlah_bunga,
							a.jumlah_denda,
							a.jumlah_angsuran
						FROM
							pinjaman p
						JOIN angsuran a ON p.id_pinjaman = a.id_pinjaman
						WHERE
							p.is_delete = 0
						AND a.is_delete = 0
						AND p.id_member = $id_member
						ORDER BY
							a.tanggal_angsuran DESC
					);
				");

				$pinjaman = $this->db
					->select("
						j.nama_produk,
						p.id_pinjaman,
						p.no_pinjaman,
						p.tanggal_meminjam,
						p.tanggal_jatuhtempo,
						p.jumlah jumlah_pinjaman,
						p.jangka_waktu,
						COUNT(a.id_angsuran) jumlah_transaksi_angsuran,
						SUM(a.jumlah_bunga) bunga,
						SUM(a.jumlah_denda) denda,
						SUM(a.jumlah_angsuran) angsuran,
						a.tanggal_angsuran
					")
					->from("pinjaman p")
					->join("tmp_order_angsuran a", "p.id_pinjaman = a.id_pinjaman", "left")
					->join("pinjaman_produk j", "j.id_produk_pinjaman = p.id_produk_pinjaman")
					->where("p.id_member", $id_member)
					->where("p.is_delete", 0)
					->group_by("p.id_pinjaman")
					->order_by("p.id_pinjaman", "desc")
					->get();

			// list jumlah simpanan member per jenis simpanan
				$this->db->query("DROP TABLE IF EXISTS tmp_simpanan;");
				$this->db->query("
					CREATE TEMPORARY TABLE tmp_simpanan (
						SELECT
							id_jenis_simpanan,
							tanggal,
							jumlah 
						FROM
							simpanan 
						WHERE
							id_member = $id_member 
							AND is_delete = '0' 
						ORDER BY
							id_jenis_simpanan,
							tanggal DESC
					);
				");
				$sql_simpanan = $this->db->query("
					SELECT
						id_jenis_simpanan, SUM(jumlah) jumlah, tanggal
					FROM
						tmp_simpanan
					GROUP BY
						id_jenis_simpanan
				");
				
				$simpanan = array();
				foreach ($sql_simpanan->result() as $s) {
					$simpanan[$s->id_jenis_simpanan] = array(
						"jumlah" => $s->jumlah,
						"tanggal" => $s->tanggal
					);
				}

			// list jumlah penarikan member per jenis simpanan
				$this->db->query("DROP TABLE IF EXISTS tmp_penarikan;");
				$this->db->query("
					CREATE TEMPORARY TABLE tmp_penarikan (
						SELECT
							id_jenis_simpanan,
							tanggal,
							jumlah_penarikan 
						FROM
							penarikan 
						WHERE
							id_member = $id_member 
							AND is_delete = '0' 
						ORDER BY
							id_jenis_simpanan,
							tanggal DESC
					);
				");
				$sql_penarikan = $this->db->query("
					SELECT
						id_jenis_simpanan, SUM(jumlah_penarikan) jumlah, tanggal
					FROM
						tmp_penarikan
					GROUP BY
						id_jenis_simpanan
				");
				
				$penarikan = array();
				foreach ($sql_penarikan->result() as $s) {
					$penarikan[$s->id_jenis_simpanan] = array(
						"jumlah" => $s->jumlah,
						"tanggal" => $s->tanggal
					);
				}
			
			// list jumlah iuran administrasi anggota per jenis administrasi
				$this->db->query("DROP TABLE IF EXISTS tmp_adm;");
				$this->db->query("
					CREATE TEMPORARY TABLE tmp_adm (
						SELECT
							id_jenis_adm,
							tanggal,
							jumlah 
						FROM
							adm_iuran 
						WHERE
							id_member = $id_member 
							AND is_delete = '0' 
						ORDER BY
							id_jenis_adm,
							tanggal DESC
					);
				");
				$sql_administrasi = $this->db->query("
					SELECT
						id_jenis_adm, SUM(jumlah) jumlah, tanggal
					FROM
						tmp_adm
					GROUP BY
						id_jenis_adm
				");
				
				$administrasi = array();
				foreach ($sql_administrasi->result() as $s) {
					$administrasi[$s->id_jenis_adm] = array(
						"jumlah" => $s->jumlah,
						"tanggal" => $s->tanggal
					);
				}

			// mencari jenis simpanan yang dipilih member saat melakukan registrasi
				$sql_jenis_simpanan_member = $this->db
					->where("id_member", $id_member)
					->get("relasi_member_jenissimpanan");
					
				$relasi_jenis_simpanan = array();
				$relasi_member_jenissimpanan = array();
				foreach ($sql_jenis_simpanan_member->result() as $d) {
					array_push($relasi_jenis_simpanan, (int) $d->id_jenis_simpanan);
					$relasi_member_jenissimpanan[$d->id_jenis_simpanan] = array(
						"nomor_anggota" => $d->nomor_anggota,
						"is_delete" => $d->is_delete
					);
				}
			
			// administrasi
				$this->load->module("member/administrasi");
				$w_adm = $this->administrasi->widget_data($id_member);

			// bunga smpanan
				$this->db->query("DROP TABLE IF EXISTS tmp_bunga;");
				$this->db->query("
					CREATE TEMPORARY TABLE tmp_bunga (
						SELECT
							jumlah, tanggal
						FROM
							simpanan_bunga
						WHERE
							id_member = $id_member
						AND is_delete = 0
						ORDER BY
							tanggal DESC
					);
				");

				$sql_bunga_simpanan = $this->db
					->select("SUM(jumlah) jumlah, tanggal")
					->get("tmp_bunga", 1);

			$data = array(
				"id_member" => $id_member,
				"is_anggota" => $is_anggota,

				"jenis_simpanan" => $sql_jenis_simpanan,
				"jenis_adm" => $sql_jenis_adm,
				"bunga_simpanan" => $sql_bunga_simpanan->row(),

				"adm" => $w_adm,
				
				"administrasi" => $administrasi,
				"simpanan" => $simpanan,
				"penarikan" => $penarikan,
				"pinjaman" => $pinjaman,
				"relasi_member_jenissimpanan" => $relasi_member_jenissimpanan,
				
				"relasi_jenis_simpanan" => $relasi_jenis_simpanan,

				"detail" => $this->widget_detail_member($id_member, true)
			);
			$this->load->view("$this->module_view/detail", $data);
		}

		public function widget_detail_member ($id_member, $show_edit = false) {
			$sql_detail = $this->db
				->select("dm.*")
				->join("member m", "m.id_member = dm.id_member")
				->where("m.id_member", $id_member)
				->from("data_member dm")
				->get();

			$data = array(
				"detail" => $sql_detail->row(),
				"show_edit" => $show_edit
			);

			return $this->load->view("$this->module_view/detail_widget", $data, true);
		}
		
		public function form ($id = 0) {
			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");
			
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// custom
			$this->output->script_head("$this->module_view/_gudang/form.css");
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/form.js");
			
			if ($id > 0) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}
		
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_data_member");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}
	// PRIVATE
		private function add () {
			$data = $this->_formInputData(array(
				// "nik" => "156151561651616516",
				// "kota_dikeluarkan_ktp" => "Sintang",
				// "nama_lengkap" => "Rio Qhodam",
				// "nama_panggilan" => "Rio",
				// "agama" => "islam",
				// "daerah_asal" => "Sintang",
				// "tanggal_lahir" => "1994-01-01",
				// "nama_ibukandung" => "Rokhimah",
				// "jenis_kelamin" => "L",
				// "alamat_sekarang" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
				// "kodepos" => "78115",
				// "telp_pribadi" => "08765432100",
				// "status_pernikahan" => "0",
				// "jumlah_tanggungan" => "2",
				// "pendidikan_terakhir" => "S1",
				// "pekerjaan" => "Programmer",
				// "jabatan" => "Pelaksana",
				// "nama_kantor" => "Qapuas",
				// "bidang_usaha" => "Swasta",
				// "alamat_kantor" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
				// "telp_kantor" => "05652025208",
			));
			
			$this->load->view("$this->module_view/form", $data);
		}
		
		private function edit ($id) {
			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"nik" => $val->nik,
					"kota_dikeluarkan_ktp" => $val->kota_dikeluarkan_ktp,
					"nama_lengkap" => $val->nama_lengkap,
					"nama_panggilan" => $val->nama_panggilan,
					"agama" => $val->agama,
					"daerah_asal" => $val->daerah_asal,
					"tanggal_lahir" => $val->tanggal_lahir,
					"nama_ibukandung" => $val->nama_ibukandung,
					"jenis_kelamin" => $val->jenis_kelamin,
					"alamat_sekarang" => $val->alamat_sekarang,
					"kodepos" => $val->kodepos,
					"telp_pribadi" => $val->telp_pribadi,
					"status_pernikahan" => $val->status_pernikahan,
					"jumlah_tanggungan" => $val->jumlah_tanggungan,
					"pendidikan_terakhir" => $val->pendidikan_terakhir,
					"pekerjaan" => $val->pekerjaan,
					"jabatan" => $val->jabatan,
					"nama_kantor" => $val->nama_kantor,
					"bidang_usaha" => $val->bidang_usaha,
					"alamat_kantor" => $val->alamat_kantor,
					"telp_kantor" => $val->telp_kantor,
					"is_anggota" => $val->is_anggota,
				));
				
				$this->load->view("$this->module_view/form", $data);
			} else {
				show_404();
			}
		}
		
		private function add_proses () {
			$back = "$this->module/form";
			$submsg = "Data gagal di proses";
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = $this->module;
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$sql = $this->M_app->cekId($id, "id_data_member, id_member");
			
			if ($sql->num_rows() > 0) {
				$back = "$this->module/form";
				$submsg = "Data gagal di proses";
				
				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$val = $sql->row();
					$data = $this->_formPostInputData();
					// header("content-type: application/json");
					// echo json_encode($this->input->post("jenis_simpanan")); die();
					$up = $this->M_app->edit($data, $id, $val->id_member);
					
					if ($up) {
						$this->stat = true;
						
						if (empty($this->session->userdata('link_back'))) {
							$back = $this->module;
						} else {
							$back = $this->session->userdata('link_back');
							$this->session->unset_userdata("link_back");
						}
						
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}
		
		private function _formInputData ($data = array()) {
			// list jenis simpanan non anggota
			$list_simpanan_nonanggota = array();
			$sql = $this->db
				->where("id_kategori_simpanan", 2)
				->where("id_jenis_simpanan != 9")
				->get("jenis_simpanan");
				
			foreach ($sql->result() as $list) {
				$list_simpanan_nonanggota += array(
					$list->id_jenis_simpanan => $list->jenis
				);
			}
			
			// list jenis simpanan yang dipilih
			$jenis_simpanan_selected = array();
			if (!empty($data['id'])) {
				$sql = $this->db
					->select("r.id_jenis_simpanan")
					->from("relasi_member_jenissimpanan r")
					->join("member m", "m.id_member = r.id_member")
					->join("data_member dm", "m.id_member = dm.id_member")
					->where("dm.id_data_member", $data['id'])
					->where("r.is_delete", 0)
					->get();
					
				foreach ($sql->result() as $list) {
					array_push($jenis_simpanan_selected, $list->id_jenis_simpanan);
				}
			}
			
			return array(
				"title" => $this->title,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("$this->module"),
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						)),
						"is_anggota" => form_input(array(
							"name" => "is_anggota",
							"class" => "is_anggota",
							"type" => "text",
							"value" => empty($data['is_anggota']) ? 0 : $data['is_anggota'],
						)),
					),
					
					"nik" => form_input(array(
						"name" => "nik",
						"class" => "form-control nik",
						"type" => "text",
						"value" => @$data['nik'],
					)),
					"nama_lengkap" => form_input(array(
						"name" => "nama_lengkap",
						"class" => "form-control nama_lengkap",
						"type" => "text",
						"value" => @$data['nama_lengkap'],
					)),
					"nama_panggilan" => form_input(array(
						"name" => "nama_panggilan",
						"class" => "form-control nama_panggilan",
						"type" => "text",
						"value" => @$data['nama_panggilan'],
					)),
					"kota_dikeluarkan_ktp" => form_input(array(
						"name" => "kota_dikeluarkan_ktp",
						"class" => "form-control kota_dikeluarkan_ktp",
						"type" => "text",
						"value" => @$data['kota_dikeluarkan_ktp'],
					)),
					"agama" => form_select(array(
						"config" => array(
							"name" => "agama",
							"class" => "form-control agama",
						),
						"list" => array(
							"" => "",
							"islam" => "Islam",
							"kristen" => "Kristen",
							"protestan" => "Protestan",
							"hindu" => "Hindu",
							"budha" => "Budha",
							"konghuchu" => "Konghuchu",
						),
						"selected" => @$data['agama'],
					)),
					"daerah_asal" => form_input(array(
						"name" => "daerah_asal",
						"class" => "form-control daerah_asal",
						"type" => "text",
						"value" => @$data['daerah_asal'],
					)),
					"tanggal_lahir" => form_input(array(
						"name" => "tanggal_lahir",
						"class" => "form-control tanggal_lahir date",
						"type" => "text",
						"value" => @$data['tanggal_lahir'],
					)),
					"nama_ibukandung" => form_input(array(
						"name" => "nama_ibukandung",
						"class" => "form-control nama_ibukandung",
						"type" => "text",
						"value" => @$data['nama_ibukandung'],
					)),
					"jenis_kelamin" => form_select(array(
						"config" => array(
							"name" => "jenis_kelamin",
							"class" => "form-control jenis_kelamin s2",
						),
						"list" => array(
							"" => "",
							"L" => "Laki-laki",
							"P" => "Perempuan",
						),
						"selected" => @$data['jenis_kelamin'],
					)),
					"alamat_sekarang" => form_textarea(array(
						"rows" => 2,
						"name" => "alamat_sekarang",
						"class" => "form-control alamat_sekarang",
						"type" => "text",
						"value" => @$data['alamat_sekarang'],
					)),
					"kodepos" => form_input(array(
						"name" => "kodepos",
						"class" => "form-control kodepos",
						"type" => "number",
						"value" => @$data['kodepos'],
					)),
					"telp_pribadi" => form_input(array(
						"name" => "telp_pribadi",
						"class" => "form-control telp_pribadi",
						"type" => "text",
						"value" => @$data['telp_pribadi'],
					)),
					"status_pernikahan" => form_select(array(
						"config" => array(
							"name" => "status_pernikahan",
							"class" => "form-control status_pernikahan s2",
						),
						"list" => array(
							"" => "",
							"belum" => "Belum",
							"sudah" => "Sudah",
							"duda" => "Duda",
							"janda" => "Janda",
						),
						"selected" => @$data['status_pernikahan'],
					)),
					"jumlah_tanggungan" => form_input(array(
						"name" => "jumlah_tanggungan",
						"class" => "form-control jumlah_tanggungan",
						"type" => "number",
						"value" => @$data['jumlah_tanggungan'],
					)),
					"pendidikan_terakhir" => form_input(array(
						"name" => "pendidikan_terakhir",
						"class" => "form-control pendidikan_terakhir",
						"type" => "text",
						"value" => @$data['pendidikan_terakhir'],
					)),
					"pekerjaan" => form_input(array(
						"name" => "pekerjaan",
						"class" => "form-control pekerjaan",
						"type" => "text",
						"value" => @$data['pekerjaan'],
					)),
					"jabatan" => form_input(array(
						"name" => "jabatan",
						"class" => "form-control jabatan",
						"type" => "text",
						"value" => @$data['jabatan'],
					)),
					"nama_kantor" => form_input(array(
						"name" => "nama_kantor",
						"class" => "form-control nama_kantor",
						"type" => "text",
						"value" => @$data['nama_kantor'],
					)),
					"bidang_usaha" => form_input(array(
						"name" => "bidang_usaha",
						"class" => "form-control bidang_usaha",
						"type" => "text",
						"value" => @$data['bidang_usaha'],
					)),
					"alamat_kantor" => form_textarea(array(
						"rows" => 2,
						"name" => "alamat_kantor",
						"class" => "form-control alamat_kantor",
						"type" => "text",
						"value" => @$data['alamat_kantor'],
					)),
					"telp_kantor" => form_input(array(
						"name" => "telp_kantor",
						"class" => "form-control telp_kantor",
						"type" => "text",
						"value" => @$data['telp_kantor'],
					)),
					
					"jenis_simpanan" => form_select(array(
						"config" => array(
							"name" => "jenis_simpanan[]",
							"class" => "form-control s2 jenis_simpanan",
							"multiple" => true,
						),
						"list" => @$list_simpanan_nonanggota,
						"selected" => $jenis_simpanan_selected,
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			// header('Content-Type: Application/json');
			// echo json_encode($this->input->post()); die();
			
			$is_anggota = $this->input->post('is_anggota');
			
			$data = array(
				"nik" => $this->input->post('nik'),
				"kota_dikeluarkan_ktp" => $this->input->post('kota_dikeluarkan_ktp'),
				"nama_lengkap" => $this->input->post('nama_lengkap'),
				"nama_panggilan" => $this->input->post('nama_panggilan'),
				"agama" => $this->input->post('agama'),
				"daerah_asal" => $this->input->post('daerah_asal'),
				"tanggal_lahir" => $this->input->post('tanggal_lahir'),
				"nama_ibukandung" => $this->input->post('nama_ibukandung'),
				"jenis_kelamin" => $this->input->post('jenis_kelamin'),
				"alamat_sekarang" => $this->input->post('alamat_sekarang'),
				"kodepos" => $this->input->post('kodepos'),
				"telp_pribadi" => $this->input->post('telp_pribadi'),
				"status_pernikahan" => $this->input->post('status_pernikahan'),
				"jumlah_tanggungan" => $this->input->post('jumlah_tanggungan'),
				"pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
				"pekerjaan" => $this->input->post('pekerjaan'),
				"jabatan" => $this->input->post('jabatan'),
				"nama_kantor" => $this->input->post('nama_kantor'),
				"bidang_usaha" => $this->input->post('bidang_usaha'),
				"alamat_kantor" => $this->input->post('alamat_kantor'),
				"telp_kantor" => $this->input->post('telp_kantor'),
				"is_anggota" => $this->input->post('is_anggota')
			);
			
			// header('Content-Type: Application/json');
			// echo json_encode($data); die();
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("nama_lengkap")) {
				$err .= form_error("nama_lengkap");
			}
			// if (form_error("content")) {
			// 	$err .= form_error("content");
			// }
			
			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "nama_lengkap",
					"label" => "Nama Lengkap",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				// array(
				// 	"field" => "content",
				// 	"label" => "Konten",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
}