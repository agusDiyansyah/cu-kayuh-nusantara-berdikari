<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok_anggota extends Admin_controller {
	private $stat = false;
	private $title = "Anggota Kelompok";
	private $module = "member/kelompok_anggota";
	private $id_data_kelompok;
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_kelompok_anggota", "M_app");
		}

		public function tes () {
			return "lorem";
		}
		
		public function index () {
			show_404();
		}
		
		public function daftar ($id_data_kelompok = 0) {
			if ($id_data_kelompok > 0) {
				// datatables
				$this->output->js('assets/themes/admin/vendors/datatables/jquery.dataTables.js');
				$this->output->js('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js');
				$this->output->css('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');
				
				// validate
				$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
				
				$this->output->script_foot("$this->module/_gudang/data.js");
				
				$sql = $this->db
					->where("id_data_kelompok", $id_data_kelompok)
					->select("nama_kelompok")
					->get("data_kelompok", 1);
					
				$val = $sql->row();
				
				$data = array(
					"title" => $this->title,
					"link_add" => base_url("$this->module/form/$id_data_kelompok"),
					"link_back" => base_url("member/kelompok"),
					"id_data_kelompok" => $id_data_kelompok,
					"nama_kelompok" => $val->nama_kelompok,
					
					"filter" => array()
				);
				
				$this->load->view("$this->module/data", $data);
			} else {
				show_404();
			}
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($_POST);
			}
			return;
		}

		public function detail ($id_member = 0, $id_data_kelompok = 0) {
			if ($id_member > 0 OR $id_data_kelompok > 0) {
				$this->load->module("member");
				$this->member->detail($id_member);
				$back = base_url("$this->module/daftar/$id_data_kelompok");

				$this->output->script_foot_string("
					$(document).ready(function () {
						$('.mn-member').removeClass('active');
						$('.mn-kelompok').addClass('active');
						$('.btn-back').attr('href', '$back');
						$('.aksi').hide();
						$('.btn-edit-member').hide();
					});
				");
			} else {
				show_404();
				die();
			}
		}

		public function detail_nik ($id_data_kelompok, $nik) {
			if (is_numeric($nik)) {
				$sql = $this->db
					->select("id_member")
					->where("nik", $nik)
					->get("data_member", 1);

				if ($sql->num_rows() > 0) {
					$sql = $sql->row();
					$id_member = $sql->id_member;

					$this->detail($id_member, $id_data_kelompok);
				} else {
					$this->output->script_foot_string('
						$(document).ready(function () {
							var template = "";
							
							template += \'<div class="container-fluid">\';
							template += \'	<div class="row">\';
							template += \'		<div class="col-xs-12">\';
							template += \'			<div class="alert alert-warning" role="alert"> \';
							template += \'				Mohon maaf No NIK belum terdaftar didalam daftar member\';
							template += \'			</div>\';
							template += \'		</div>\';
							template += \'	</div>\';
							template += \'</div>\';

							$("header").after(template);
						});
					');
				}
				
			} else {
				
			}
		}
		
		public function form ($id_data_kelompok = 0, $id = 0) {
			$this->cekParsingId($id_data_kelompok);
			
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");
			
			// custom
			$this->output->script_foot("$this->module/_gudang/form.js");
			
			if ($id > 0) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}
		
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_data_kelompok");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}

		public function cekMemberByNIK () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post("q")) {
				$nik = $this->input->post("q");

				$data = array();

				if (is_numeric($nik)) {
					$sql = $this->db
						->like("nik", $nik, "both")
						->select("nik, nama_lengkap, jenis_kelamin, telp_pribadi")
						->get("data_member");

					if ($sql->num_rows() > 0) {
						foreach ($sql->result() as $d) {
							array_push($data, array(
								"disabled" => false, 
								"id" => $d->nik, 
								"text" => "$d->nik - $d->nama_lengkap",
								"nama" => $d->nama_lengkap,
								"jenis_kelamin" => $d->jenis_kelamin,
								"telp_pribadi" => $d->telp_pribadi,
							));
						}
					} else {
						array_push($data, array('disabled' => false, 'id' => $nik, 'text' => $nik));
						array_push($data, array('disabled' => true, 'id' => " ", 'text' => "NIK yang diinput belum didaftarkan pada data member"));
					}
				} else {
					array_push($data, array('disabled' => true, 'id' => " ", 'text' => "NIK harus berupa bilangan numerik"));
				}

				echo json_encode($data);
			} else {
				show_404();
				die();
			}
		}
	// PRIVATE
		private function add () {
			$data = $this->_formInputData(array(
				"id_data_kelompok" => $this->id_data_kelompok,
				
				// "nik" => "651651651651651",
				// "nama" => "Agus Diyansyah",
				// "jenis_kelamin" => "L",
				// "jabatan" => "anggota",
				// "no_telp" => "0876543210000",
				// "email" => "agus@mial.com",
				// "alamat" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			));
			
			$this->load->view("$this->module/form", $data);
		}
		
		private function edit ($id) {
			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"id_data_kelompok" => $val->id_data_kelompok,
					"nik" => $val->nik,
					"nama" => $val->nama,
					"jenis_kelamin" => $val->jenis_kelamin,
					"jabatan" => $val->jabatan,
					"no_telp" => $val->no_telp,
					"email" => $val->email,
					"alamat" => $val->alamat,
				));
				
				$this->load->view("$this->module/form", $data);
			} else {
				show_404();
			}
		}
		
		private function add_proses () {
			$id_data_kelompok = $this->input->post('id_data_kelompok');
			$back = "$this->module/form/$id_data_kelompok";
			$submsg = "Data gagal di proses";
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "$this->module/daftar/$id_data_kelompok";
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$sql = $this->M_app->cekId($id, "id_data_kelompok");
			
			if ($sql->num_rows() > 0) {
				$id_data_kelompok = $this->input->post('id_data_kelompok');
				$back = "$this->module/form/$id_data_kelompok";
				$submsg = "Data gagal di proses";
				
				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$data = $this->_formPostInputData();
					$up = $this->M_app->edit($data, $id);
					
					if ($up) {
						$this->stat = true;
						$back = "$this->module/daftar/$id_data_kelompok";
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}

		private function cekParsingId ($id_data_kelompok = 0) {
			if (
				empty($id_data_kelompok) OR
				$id_data_kelompok == 0
			) {
				show_404();
				die();
			} else {
				$this->id_data_kelompok = $id_data_kelompok;
				return;
			}
		}
		
		private function _formInputData ($data = array()) {
			$sql = $this->db
				->where("id_data_kelompok", $data['id_data_kelompok'])
				->select("nama_kelompok")
				->get("data_kelompok", 1);
				
			$val = $sql->row();

			// cek data anggota di data member
			$member = false;
			if (!empty($data['nik'])) {
				$sql = $this->db
					->select("id_data_member")
					->where("nik", $data['nik'])
					->get("data_member");
				if ($sql->num_rows() > 0) {
					$member = true;
				}
			}
			
			return array(
				"title" => $this->title,
				"nama_kelompok" => $val->nama_kelompok,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("$this->module/daftar/$data[id_data_kelompok]"),
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						)),
						"id_data_kelompok" => form_input(array(
							"name" => "id_data_kelompok",
							"class" => "id_data_kelompok",
							"type" => "text",
							"value" => @$data['id_data_kelompok'],
						)),
						"is_member" => form_input(array(
							"name" => "is_member",
							"class" => "is_member",
							"type" => "text",
							"value" => $member,
						)),
					),
					
					"nik" => form_select(array(
						"config" => array(
							"name" => "nik",
							"class" => "form-control nik",
						),
						"list" => ( (empty($data['nik'])) ? array("" => "") : array($data['nik']) ),
						"selected" => @$data['nik'],
					)),
					"nama" => form_input(array(
						"name" => "nama",
						"class" => "form-control nama",
						"type" => "text",
						"value" => @$data['nama'],
					)),
					"jenis_kelamin" => form_select(array(
						"config" => array(
							"name" => "jenis_kelamin",
							"class" => "form-control jenis_kelamin s2",
						),
						"list" => array(
							"" => "",
							"L" => "Laki-laki",
							"P" => "Perempuan",
						),
						"selected" => @$data['jenis_kelamin'],
					)),
					"jabatan" => form_select(array(
						"config" => array(
							"name" => "jabatan",
							"class" => "form-control jabatan s2",
						),
						"list" => array(
							"" => "",
							"1" => "Ketua",
							"2" => "Sekretaris",
							"3" => "Bendahara",
							"4" => "Anggota"
						),
						"selected" => @$data['jabatan'],
					)),
					"no_telp" => form_input(array(
						"name" => "no_telp",
						"class" => "form-control no_telp",
						"type" => "text",
						"value" => @$data['no_telp'],
					)),
					"email" => form_input(array(
						"name" => "email",
						"class" => "form-control email",
						"type" => "email",
						"value" => @$data['email'],
					)),
					"alamat" => form_textarea(array(
						"name" => "alamat",
						"class" => "form-control alamat",
						"rows" => 5,
						"value" => @$data['alamat'],
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			$data = array(
				"id_data_kelompok" => $this->input->post('id_data_kelompok'),
				"nik" => $this->input->post('nik'),
				"nama" => $this->input->post('nama'),
				"jenis_kelamin" => $this->input->post('jenis_kelamin'),
				"jabatan" => $this->input->post('jabatan'),
				"no_telp" => $this->input->post('no_telp'),
				"email" => $this->input->post('email'),
				"alamat" => $this->input->post('alamat'),
			);
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("nik")) {
				$err .= form_error("nik");
			}
			// if (form_error("content")) {
			// 	$err .= form_error("content");
			// }
			
			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "nik",
					"label" => "NIK",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				// array(
				// 	"field" => "content",
				// 	"label" => "Konten",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
}