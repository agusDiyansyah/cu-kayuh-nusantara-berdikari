<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok extends Admin_controller {
	private $stat = false;
	private $title = "Kelompok";
	private $module = "member/kelompok";
	private $module_view = "member/kelompok";
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_kelompok", "M_app");
		}
		
		public function index () {
			// datatables
			$this->output->js('assets/themes/admin/vendors/datatables/jquery.dataTables.js');
			$this->output->js('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js');
			$this->output->css('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');
			
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/data.js");
			
			$data = array(
				"title" => $this->title,
				"link_add" => base_url("$this->module/form"),
				
				"filter" => array(
					"nama_kelompok" => form_input(array(
						"name" => "nama_kelompok",
						"class" => "form-control nama_kelompok",
						"type" => "text",
					)),
					"bidang_usaha" => form_input(array(
						"name" => "bidang_usaha",
						"class" => "form-control bidang_usaha",
						"type" => "text",
					)),
					"telp" => form_input(array(
						"name" => "telp",
						"class" => "form-control telp",
						"type" => "text",
					)),
					"email" => form_input(array(
						"name" => "email",
						"class" => "form-control email",
						"type" => "text",
					)),
					"nomor_anggota" => form_input(array(
						"name" => "nomor_anggota",
						"class" => "form-control nomor_anggota",
						"type" => "text",
					)),
				)
			);
			
			$this->load->view("$this->module_view/data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($_POST);
			}
			return;
		}

		public function detail ($id_member = 0) {
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/detail.js");
			
			$data = array(
				"id_member" => $id_member,
				"detail" => $this->widget_detail_kelompok($id_member, true),

				"nomor_anggota" => $this->M_app->nomor_anggota($id_member, 9),
				"simpanan" => $this->M_app->simpanan($id_member),
				"penarikan" => $this->M_app->penarikan($id_member),
				"pinjaman" => $this->M_app->pinjaman($id_member),
				"bunga_simpanan" => $this->M_app->bunga_simpanan($id_member),
			);

			// echo $this->db->last_query(); die();
			
			$this->load->view("$this->module_view/detail", $data);
		}

		public function widget_detail_kelompok ($id_member) {
			$sql_kelompok = $this->db
				->select("dk.*")
				->join("member m", "m.id_member = dk.id_member")
				->where("m.id_member", $id_member)
				->from("data_kelompok dk")
				->get();

			$sql_anggota = $this->db
				->select("a.nama, a.jabatan")
				->from("data_kelompok dk")
				->join("anggota a", "dk.id_data_kelompok = a.id_data_kelompok")
				->where("dk.id_member", $id_member)
				->order_by("a.jabatan", "asc")
				->order_by("a.id_anggota", "desc")
				->get();

			$data = array(
				"kelompok" => $sql_kelompok->row(),
				"anggota" => $sql_anggota->result(),
			);

			return $this->load->view("$this->module_view/detail_widget", $data, true);
		}
		
		public function form ($id = 0) {
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/form.js");
			
			if ($id > 0) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}

		public function edit_detail ($id_data_kelompok = 0, $id_member = 0) {
			if (
				empty($id_data_kelompok) OR
				$id_data_kelompok == 0 OR
				empty($id_member) OR
				$id_member == 0
			) {
				$this->msg("warning", "Data tidak ditemukan");
			} else {
				$this->form($id_data_kelompok);
				$this->output->script_foot_string("
					$('.btn-back').attr('href', '". base_url("member/kelompok/detail/$id_member") ."');
				", true);
			}
		}
		
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_data_kelompok");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}
	// PRIVATE
		private function add () {
			$data = $this->_formInputData(array(
				// "nama_kelompok" => "Qapuas",
				// "tanggal_berdiri" => "2016-08-08",
				// "bidang_usaha" => "Usaha Dagang",
				// "jumlah_anggota" => "4",
				// "no_akta" => "561ds51asd651ad65a1sd65a1sd",
				// "telp" => "08765432100",
				// "email" => "qapuas@mail.com",
				// "alamat" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			));
			
			$this->load->view("$this->module_view/form", $data);
		}
		
		private function edit ($id) {
			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"nama_kelompok" => $val->nama_kelompok,
					"tanggal_berdiri" => $val->tanggal_berdiri,
					"bidang_usaha" => $val->bidang_usaha,
					"jumlah_anggota" => $val->jumlah_anggota,
					"no_akta" => $val->no_akta,
					"telp" => $val->telp,
					"email" => $val->email,
					"alamat" => $val->alamat,
				));
				
				$this->load->view("$this->module_view/form", $data);
			} else {
				show_404();
			}
		}
		
		private function add_proses () {
			$back = "$this->module/form";
			$submsg = "Data gagal di proses";
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = $this->module;
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$sql = $this->M_app->cekId($id, "id_data_kelompok");
			
			if ($sql->num_rows() > 0) {
				$back = "$this->module/form";
				$submsg = "Data gagal di proses";
				
				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$data = $this->_formPostInputData();
					$up = $this->M_app->edit($data, $id);
					
					if ($up) {
						$this->stat = true;
						$back = $this->module;
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}
		
		private function _formInputData ($data = array()) {
			return array(
				"title" => $this->title,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("$this->module"),
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						))
					),
					
					"nama_kelompok" => form_input(array(
						"name" => "nama_kelompok",
						"class" => "form-control nama_kelompok",
						"type" => "text",
						"value" => @$data['nama_kelompok'],
					)),
					"tanggal_berdiri" => form_input(array(
						"name" => "tanggal_berdiri",
						"class" => "form-control tanggal_berdiri date",
						"type" => "text",
						"value" => @$data['tanggal_berdiri'],
					)),
					"bidang_usaha" => form_input(array(
						"name" => "bidang_usaha",
						"class" => "form-control bidang_usaha",
						"type" => "text",
						"value" => @$data['bidang_usaha'],
					)),
					"jumlah_anggota" => form_input(array(
						"name" => "jumlah_anggota",
						"class" => "form-control jumlah_anggota",
						"type" => "number",
						"value" => @$data['jumlah_anggota'],
					)),
					"no_akta" => form_input(array(
						"name" => "no_akta",
						"class" => "form-control no_akta",
						"type" => "text",
						"value" => @$data['no_akta'],
					)),
					"telp" => form_input(array(
						"name" => "telp",
						"class" => "form-control telp",
						"type" => "text",
						"value" => @$data['telp'],
					)),
					"email" => form_input(array(
						"name" => "email",
						"class" => "form-control email",
						"type" => "email",
						"value" => @$data['email'],
					)),
					"alamat" => form_textarea(array(
						"rows" => 2,
						"name" => "alamat",
						"class" => "form-control alamat",
						"type" => "text",
						"value" => @$data['alamat'],
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			$data = array(
				"nama_kelompok" => $this->input->post('nama_kelompok'),
				"tanggal_berdiri" => $this->input->post('tanggal_berdiri'),
				"bidang_usaha" => $this->input->post('bidang_usaha'),
				// "jumlah_anggota" => $this->input->post('jumlah_anggota'),
				"no_akta" => $this->input->post('no_akta'),
				"telp" => $this->input->post('telp'),
				"email" => $this->input->post('email'),
				"alamat" => $this->input->post('alamat'),
			);
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("nama_kelompok")) {
				$err .= form_error("nama_kelompok");
			}
			// if (form_error("content")) {
			// 	$err .= form_error("content");
			// }
			
			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "nama_kelompok",
					"label" => "Nama Kelompok",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				// array(
				// 	"field" => "content",
				// 	"label" => "Konten",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
}