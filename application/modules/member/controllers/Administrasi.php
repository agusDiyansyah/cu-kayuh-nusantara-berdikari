<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrasi extends Admin_controller {
	private $stat = false;
	private $title = "Administrasi Anggota";
	private $module = "member/administrasi";
	private $module_view = "member/administrasi";
	private $link_back;
	private $jenis_adm = array(
		1 => "Administrasi Keanggotaan",
		2 => "Dana Pengembangan",
		3 => "Iuran Gedung",
		4 => "Dana Solidaritas",
		5 => "Dana Solidaritas Dukacita",
		6 => "Dana Solidaritas Sosial",
	);
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();

			$this->link_back = (empty($this->session->userdata("link_back"))) ? "" : $this->session->userdata("link_back");
			
			$this->load->model("M_administrasi", "M_app");
		}

		public function index ($id_member = 0) {
			$this->load->module("member");
			$detail = $this->member->widget_detail_member($id_member);
			$adm = $this->widget_data($id_member);

			$data = array(
				"adm" => $adm,
				"detail" => $detail
			);

			$this->session->set_userdata("link_back", "member/administrasi/$id_member");

			$this->load->view("data", $data);
		}

		public function widget_data ($id_member, $conf = array()) {
			if (
				empty($id_member) OR
				$id_member == 0
			) {
				show_404();
				die();
			} else {
				// cek apakah member adalah anggota atau bukan
				$sql = $this->db
					->select("is_anggota")
					->where("id_member", $id_member)
					->where("is_delete", 0)
					->get("data_member", 1)
					->row();

				$is_anggota = $sql->is_anggota;
			}

			// ordering iuran adm  berdasarkan tanggal terbesar
			$this->db->query("DROP TABLE IF EXISTS tmp_iuran;");
			$this->db->query("
				CREATE TEMPORARY TABLE tmp_iuran (
					SELECT
						id_jenis_adm,
						tanggal,
						jumlah 
					FROM
						adm_iuran 
					WHERE
						id_member = $id_member 
						AND is_delete = '0' 
					ORDER BY
						id_jenis_adm,
						tanggal DESC
				);
			");

			// jumlahkan dana admistrasi berdasarkan jenis administrasi
			$this->db->query("DROP TABLE IF EXISTS tmp_adm;");
			$this->db->query("
				CREATE TEMPORARY TABLE tmp_adm (
					SELECT
						id_jenis_adm, SUM(jumlah) jumlah, tanggal
					FROM
						tmp_iuran
					GROUP BY
						id_jenis_adm
				);
			");

			// menampilkan nama jenis adm yang telah dilengkapi dan abelum di lengkapi
			$sql_adm = $this->db
				->select("j.id_jenis_adm, jenis_adm, jumlah, tanggal")
				->from("adm_jenis j")
				->join("tmp_adm a", "j.id_jenis_adm = a.id_jenis_adm", "left")
				->order_by("j.id_jenis_adm")
				->get();
			
			$data = array(
				"is_anggota" => $is_anggota,
				"id_member" => $id_member,

				"adm" => $sql_adm,
				
				"filter" => array(
					"hide" => array(
						"id_member" => form_input(array(
							"name" => "id_member",
							"class" => "id_member",
							"value" => $id_member,
						)),
					),
					"kategori" => form_input(array(
						"name" => "kategori",
						"class" => "form-control kategori",
						"type" => "text",
					)),
				)
			);
			
			return $this->load->view("$this->module_view/w_data.php", $data, true);
		}
		
		public function angsuran ($id_jenis_adm = 0, $id_member = 0, $conf = array()) {
			$this->cekParsingId($id_jenis_adm, $id_member);
			$this->load->module("member");
			$detail = $this->member->widget_detail_member($id_member);

			// datatables
			$this->output->js('assets/themes/admin/vendors/datatables/jquery.dataTables.js');
			$this->output->js('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js');
			$this->output->css('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');

			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/data.js");
			
			$data = array(
				"title" => $this->title,
				"link_add" => base_url("$this->module/form/$id_jenis_adm/$id_member"),
				"link_back" => base_url($this->link_back),
				"detail" => $detail,
				"jenis_adm" => strtoupper($this->jenis_adm[$id_jenis_adm]),
				
				"filter" => array(
					"hide" => array(
						"id_jenis_adm" => form_input(array(
							"name" => "id_jenis_adm",
							"class" => "id_jenis_adm",
							"value" => $id_jenis_adm,
						)),
						"id_member" => form_input(array(
							"name" => "id_member",
							"class" => "id_member",
							"value" => $id_member,
						)),
					),
					
					"kategori" => form_input(array(
						"name" => "kategori",
						"class" => "form-control kategori",
						"type" => "text",
					)),
				)
			);
			
			$this->load->view("$this->module_view/angsuran_data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($this->input->post());
			}
			return;
		}

		public function getTotalSimpanan () {
			$this->output->unset_template();

			if ($this->input->is_ajax_request() AND $this->input->post()) {
				$id_jenis_adm = $this->input->post("id_jenis_adm");
				$id_member = $this->input->post("id_member");

				$sql = $this->db
					->where("id_jenis_adm", $id_jenis_adm)
					->where("id_member", $id_member)
					->where("is_delete", 0)
					->select("SUM(jumlah) jumlah")
					->get("simpanan")
					->row();

				$jumlah_simpanan = (empty($sql->jumlah)) ? 0 : $sql->jumlah;

				echo json_encode(array(
					"jumlah_simpanan" => number_format($jumlah_simpanan, 0, ",", ".")
				));
			} else {
				show_404();
			}	
		}
		
		public function form ($id_jenis_adm = 0, $id_member = 0, $id = 0) {
			$this->cekParsingId($id_jenis_adm, $id_member);

			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

			// validate
			$this->output->js("assets/themes/admin/vendors/jquery-validate/jquery.validate.js");
			
			//custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/form.js");

			$detail = array(
				"id_member" => $id_member,
				"id_jenis_adm" => $id_jenis_adm
			);

			if ($id > 0) {
				$this->edit($id, $detail);
			} else {
				$this->add($detail);
			}
		}
		
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_jenis_adm");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}

		public function getResume () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post()) {
				$id_jenis_adm = $this->input->post("id_jenis_adm");
				$id_member = $this->input->post("id_member");

				$iuran = $this->db
					->select("SUM(i.jumlah) jumlah")
					->where("i.id_jenis_adm", $id_jenis_adm)
					->where("i.id_member", $id_member)
					->where("i.is_delete", 0)
					->get("adm_iuran i")
					->row();

				$iuran = (empty($iuran->jumlah)) ? 0 : $iuran->jumlah;

				echo json_encode(array(
					"iuran" => number_format($iuran, 0, ",", "."),
				));
			} else {
				show_404();
			}
		}

	// PRIVATE
		private function add ($detail) {
			$this->cekParsingId($detail['id_jenis_adm'], $detail['id_member']);

			$data = $this->_formInputData(array(
				"id_jenis_adm" => $detail['id_jenis_adm'],
				"id_member" => $detail['id_member'],
			));
			
			$this->load->view("$this->module_view/angsuran_form", $data);
		}
		
		private function edit ($id, $detail) {
			$this->cekParsingId($detail['id_jenis_adm'], $detail['id_member']);

			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"id_jenis_adm" => $detail['id_jenis_adm'],
					"id_member" => $detail['id_member'],

					"jumlah" => $val->jumlah,
					"tanggal" => $val->tanggal,
					"keterangan" => $val->keterangan,
				));
				
				$this->load->view("$this->module_view/angsuran_form", $data);
			} else {
				show_404();
			}
		}
		
		private function add_proses () {
			$id_jenis_adm = $this->input->post("id_jenis_adm");
			$id_member = $this->input->post("id_member");

			$this->cekParsingId($id_jenis_adm, $id_member);

			$back = "$this->module/angsuran_form/$id_jenis_adm/$id_member";
			$submsg = "Data gagal di proses";

			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();

				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "$this->module/angsuran/$id_jenis_adm/$id_member";
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$id_jenis_adm = $this->input->post("id_jenis_adm");
			$id_member = $this->input->post("id_member");
			
			$this->cekParsingId($id_jenis_adm, $id_member);

			$sql = $this->M_app->cekId($id, "id_iuran_adm");

			if ($sql->num_rows() > 0) {
				$back = "$this->module/angsuran_form/$id_jenis_adm/$id_member/$id";
				$submsg = "Data gagal di proses";

				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$data = $this->_formPostInputData();
					
					$up = $this->M_app->edit($data, $id);
					
					if ($up) {
						$this->stat = true;
						$back = "$this->module/angsuran/$id_jenis_adm/$id_member";
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}

		private function cekParsingId ($id_jenis_adm, $id_member) {
			if (
				empty($id_jenis_adm) OR
				empty($id_member)
			) {
				show_404();
				die();
			} else {
				return;
			}
		}
		
		private function _formInputData ($data = array()) {
			$this->load->module("member");

			return array(
				"title" => $this->title,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("$this->module/angsuran/$data[id_jenis_adm]/$data[id_member]"),

				"detail" => $this->member->widget_detail_member($data['id_member']),
				"jenis_adm" => strtoupper($this->jenis_adm[$data['id_jenis_adm']]),
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						)),
						"id_jenis_adm" => form_input(array(
							"name" => "id_jenis_adm",
							"class" => "id_jenis_adm",
							"type" => "text",
							"value" => @$data['id_jenis_adm'],
						)),
						"id_member" => form_input(array(
							"name" => "id_member",
							"class" => "id_member",
							"type" => "text",
							"value" => @$data['id_member'],
						)),
					),
					
					"tanggal" => form_input(array(
						"name" => "tanggal",
						"class" => "form-control date tanggal",
						"type" => "text",
						"value" => @$data['tanggal'],
					)),
					"jumlah" => form_input(array(
						"name" => "jumlah",
						"class" => "form-control jumlah",
						"type" => "number",
						"value" => @$data['jumlah'],
					)),
					"keterangan" => form_textarea(array(
						"name" => "keterangan",
						"class" => "form-control keterangan",
						"value" => @$data['keterangan'],
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			$data = array(
				"id_jenis_adm" => $this->input->post("id_jenis_adm"),
				"id_member" => $this->input->post("id_member"),
				"jumlah" => $this->input->post("jumlah"),
				"tanggal" => $this->input->post("tanggal"),
				"keterangan" => $this->input->post("keterangan"),
			);
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("id_jenis_adm")) {
				$err .= form_error("id_jenis_adm");
			}
			if (form_error("id_member")) {
				$err .= form_error("id_member");
			}
			if (form_error("jumlah")) {
				$err .= form_error("jumlah");
			}
			if (form_error("tanggal")) {
				$err .= form_error("tanggal");
			}

			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "tanggal",
					"label" => "Tanggal simpanan",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "jumlah",
					"label" => "Jumlah penarikan dana",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),

				array(
					"field" => "id_jenis_adm",
					"label" => "ERROR JENIS SIMPANAN",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "id_member",
					"label" => "ERROR MEMBER",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
}