<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_barang extends Admin_controller {
	private $stat = false;
	private $title = "Jenis Operasional";
	private $module = "operasional/jenis_barang";
	private $module_view = "operasional/jenis_barang";
	
	// DEFAULT
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("operasional/M_jenis_barang", "M_app");
		}
	// END OF DEFAULT

	//  INDEX
		public function index () {
			// datatables
			$this->output->js('assets/themes/admin/vendors/datatables/jquery.dataTables.js');
			$this->output->js('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js');
			$this->output->css('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');
			
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			// custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/data.js");

			$data = array(
				"title" => $this->title,
				"link_add" => base_url("$this->module/form"),
				
				"filter" => array(
					"jenis_barang" => form_input(array(
						"name" => "jenis_barang",
						"class" => "form-control jenis_barang",
						"type" => "text",
					)),
					"no_jenis" => form_input(array(
						"name" => "no_jenis",
						"class" => "form-control no_jenis",
						"type" => "text",
					)),
				)
			);
			
			$this->load->view("$this->module_view/data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($_POST);
			}
			return;
		}
	//  END OF INDEX
		
	// FORM
		public function form ($id = 0) {
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			// custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/form.js");
			
			if ($id > 0) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}

		private function add () {
			$data = $this->_formInputData();
			
			$this->load->view("$this->module_view/form", $data);
		}
		
		private function edit ($id) {
			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"jenis_barang" => $val->jenis_barang,
					"no_jenis" => $val->no_jenis,
				));
				
				$this->load->view("$this->module_view/form", $data);
			} else {
				show_404();
			}
		}
	// END OF FORM
	
	// PROSES	
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}

		private function add_proses () {
			$back = "$this->module/form";
			$submsg = "Data gagal di proses";
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "$this->module";
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$sql = $this->M_app->cekId($id, "id_jenis_barang");
			
			if ($sql->num_rows() > 0) {
				$back = "$this->module/form/$id";
				$submsg = "Data gagal di proses";
				
				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$data = $this->_formPostInputData();
					$up = $this->M_app->edit($data, $id);
					
					if ($up) {
						$this->stat = true;
						$back = "$this->module";
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_jenis_barang");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}
	// END OF PROSES

	// OTHER
		private function _formInputData ($data = array()) {
			return array(
				"title" => $this->title,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("$this->module"),
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						)),
					),
					
					"jenis_barang" => form_input(array(
						"name" => "jenis_barang",
						"class" => "form-control jenis_barang",
						"type" => "text",
						"value" => @$data['jenis_barang'],
					)),
					"no_jenis" => form_input(array(
						"name" => "no_jenis",
						"class" => "form-control no_jenis",
						"type" => "text",
						"value" => @$data['no_jenis'],
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			$data = array(
				"jenis_barang" => $this->input->post("jenis_barang"),
				"no_jenis" => $this->input->post("no_jenis"),
			);
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("jenis_barang")) {
				$err .= form_error("jenis_barang");
			}
			if (form_error("no_jenis")) {
				$err .= form_error("no_jenis");
			}
			
			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "jenis_barang",
					"label" => "Jenis operasional",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "no_jenis",
					"label" => "Nomor jenis",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
	// END OF OTHER
}