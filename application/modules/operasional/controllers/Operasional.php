<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operasional extends Admin_controller {
	private $stat = false;
	private $title = "operasional";
	private $module = "operasional";
	private $module_view = "operasional/operasional";
	
	// DEFAULT
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("operasional/M_operasional", "M_app");
		}
	// END OF DEFAULT

	//  INDEX
		public function index () {
			// datatables
			$this->output->js('assets/themes/admin/vendors/datatables/jquery.dataTables.js');
			$this->output->js('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js');
			$this->output->css('assets/themes/admin/vendors/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');
			
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');

			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");

			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/data.js");

			$data = array(
				"title" => $this->title,
				"link_add" => base_url("$this->module/form"),
				
				"filter" => array(
					"id_jenis_barang" => form_select(array(
						"config" => array(
							"name" => "id_jenis_barang",
							"class" => "form-control s2 id_jenis_barang",
						),
						"list" => array(
							"db" => array(
								"id" => "id_jenis_barang",
								"title" => "jenis_barang",
								"table" => "op_jenis_barang",
							)
						),
					)),
					"nama_barang" => form_input(array(
						"name" => "nama_barang",
						"class" => "form-control nama_barang",
						"type" => "text",
					)),
					"no_jenis" => form_input(array(
						"name" => "no_jenis",
						"class" => "form-control no_jenis",
						"type" => "text",
					)),

					"tanggal1" => form_input(array(
						"name" => "tanggal1",
						"class" => "form-control date tanggal1",
						"type" => "text",
					)),
					"tanggal2" => form_input(array(
						"name" => "tanggal2",
						"class" => "form-control date tanggal2",
						"type" => "text",
					)),
				)
			);
			
			$this->load->view("$this->module_view/data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($_POST);
			}
			return;
		}
	//  END OF INDEX
		
	// FORM
		public function form ($id = 0) {
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");

			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');

			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");
			
			// custom
			$this->output->script_foot("$this->module_view/_gudang/_global.js");
			$this->output->script_foot("$this->module_view/_gudang/form.js");
			
			if ($id > 0) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}

		private function add () {
			$data = $this->_formInputData();
			
			$this->load->view("$this->module_view/form", $data);
		}
		
		private function edit ($id) {
			$sql = $this->M_app->cekId($id);
			
			if ($sql->num_rows() > 0) {
				$val = $sql->row();
				
				$data = $this->_formInputData(array(
					"id" => $id,
					"id_jenis_barang" => $val->id_jenis_barang,
					"id_operasional" => $val->id_operasional,
					"nama_barang" => $val->nama_barang,
					"jumlah" => $val->jumlah,
					"satuan" => $val->satuan,
					"harga" => $val->harga,
					"tanggal" => $val->tanggal,
					"keterangan" => $val->keterangan,
				));
				
				$this->load->view("$this->module_view/form", $data);
			} else {
				show_404();
			}
		}
	// END OF FORM
	
	// PROSES	
		public function proses () {
			$this->_rules();
			$id = $this->input->post("id");
			
			if (!empty($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}

		private function add_proses () {
			$back = "$this->module/form";
			$submsg = "Data gagal di proses";
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				$add = $this->M_app->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "$this->module";
					$submsg = "Data berhasil di proses";
				}
			}
			
			$this->_notif($back, $submsg);
		}
		
		private function edit_proses ($id) {
			$sql = $this->M_app->cekId($id, "id_operasional");
			
			if ($sql->num_rows() > 0) {
				$back = "$this->module/form/$id";
				$submsg = "Data gagal di proses";
				
				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					$data = $this->_formPostInputData();
					$up = $this->M_app->edit($data, $id);
					
					if ($up) {
						$this->stat = true;
						$back = "$this->module";
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg);
			} else {
				show_404();
			}
		}
	
		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, "id_operasional");
				
				if ($sql->num_rows() > 0) {
					$stat = false;
					
					$del = $this->M_app->delete($id);
					
					if ($del) {
						$stat = true;
					}
					
					echo json_encode(array(
						"stat" => $stat
					));
				} else {
					show_404();
				}
				
			} else {
				show_404();
			}
		}
	// END OF PROSES

	// OTHER		
		private function _formInputData ($data = array()) {
			return array(
				"title" => $this->title,
				"form_action" => base_url("$this->module/proses"),
				"link_back" => base_url("$this->module"),
				
				"input" => array(
					"hide" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "id",
							"type" => "text",
							"value" => @$data['id'],
						)),
					),
					
					"id_jenis_barang" => form_select(array(
						"config" => array(
							"name" => "id_jenis_barang",
							"class" => "form-control s2 id_jenis_barang",
						),
						"list" => array(
							"db" => array(
								"table" => "op_jenis_barang",
								"id" => "id_jenis_barang",
								"title" => "jenis_barang",
							)
						),
						"selected" => @$data['id_jenis_barang'],
					)),
					"nama_barang" => form_input(array(
						"name" => "nama_barang",
						"class" => "form-control nama_barang",
						"type" => "text",
						"value" => @$data['nama_barang'],
					)),
					"jumlah" => form_input(array(
						"name" => "jumlah",
						"class" => "form-control jumlah",
						"type" => "number",
						"value" => @$data['jumlah'],
					)),
					"satuan" => form_input(array(
						"name" => "satuan",
						"class" => "form-control satuan",
						"type" => "text",
						"value" => @$data['satuan'],
					)),
					"harga" => form_input(array(
						"name" => "harga",
						"class" => "form-control harga",
						"type" => "number",
						"value" => @$data['harga'],
					)),
					"tanggal" => form_input(array(
						"name" => "tanggal",
						"class" => "form-control date tanggal",
						"type" => "text",
						"value" => @$data['tanggal'],
					)),
					"keterangan" => form_textarea(array(
						"name" => "keterangan",
						"class" => "form-control keterangan",
						"rows" => 5,
						"value" => @$data['keterangan'],
					)),
				)
			);
		}
		
		private function _formPostInputData () {
			$data = array(
				"id_jenis_barang" => $this->input->post("id_jenis_barang"),
				"nama_barang" => $this->input->post("nama_barang"),
				"jumlah" => $this->input->post("jumlah"),
				"tanggal" => $this->input->post("tanggal"),
				"satuan" => $this->input->post("satuan"),
				"harga" => $this->input->post("harga"),
				"keterangan" => $this->input->post("keterangan"),
			);
			
			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if (form_error("id_jenis_barang")) {
				$err .= form_error("id_jenis_barang");
			}
			if (form_error("nama_barang")) {
				$err .= form_error("nama_barang");
			}
			if (form_error("jumlah")) {
				$err .= form_error("jumlah");
			}
			if (form_error("tanggal")) {
				$err .= form_error("tanggal");
			}
			if (form_error("harga")) {
				$err .= form_error("harga");
			}
			
			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "id_jenis_barang",
					"label" => "Jenis barang",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "jumlah",
					"label" => "Jumlah barang",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "tanggal",
					"label" => "Tanggal",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "nama_barang",
					"label" => "Nama barang",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "harga",
					"label" => "Harga barang",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
		
		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "msg", _notif(true, "Data berhasil di proses", $submsg) );
			} else {
				$this->session->set_flashdata( "msg", _notif(false, "Data gagal di proses", $submsg) );
			}

			redirect($back);
		}
	// END OF OTHER
}