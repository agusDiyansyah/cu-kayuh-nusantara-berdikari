<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_barang extends CI_Model {
	private $tb = "op_jenis_barang";
	private $tb_id = "id_jenis_barang";

	public function __construct() {
		parent::__construct();
	}

	public function data ($post, $debug = false) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
			$this->db
				->from("$this->tb j")
				->where("j.is_delete", 0);
			
			// filter user level
			
			$orderColumn = array(
				2 => "j.jenis_barang",
			);
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by("j.$this->tb_id", $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

			if (!empty($post['jenis_barang'])) {
				$this->db->like("j.jenis_barang", $post['jenis_barang'], "both");
			}
			if (!empty($post['no_jenis'])) {
				$this->db->like("j.no_jenis", $post['no_jenis'], "both");
			}

		$this->db->stop_cache();

			// get num rows
			$this->db->select("j.$this->tb_id");
			$rowCount = $this->db->get()->num_rows();

			// get result
			$val = $this->db
				->select("
					j.jenis_barang,
					j.no_jenis,
					j.id_jenis_barang
				")
				->limit($post['length'], $post['start'])
				->get()
				->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}operasional/jenis_barang/form/$data->id_jenis_barang' id='btn-detail'>
					Edit
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_jenis_barang' id='btn-delete'>
					Hapus
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";
			
			$baris = array(
				"no" => $no,
				"aksi" => $aksi,
				"jenis_barang" => $data->jenis_barang,
				"no_jenis" => $data->no_jenis,
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert($this->tb, $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("x.$this->tb_id", $id)
			->get("$this->tb x");
	}
}