<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_operasional extends CI_Model {
	private $tb = "op_data";
	private $tb_id = "id_operasional";

	public function __construct() {
		parent::__construct();
	}

	public function data ($post, $debug = false) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
			$this->db
				->from("$this->tb o")
				->join("op_jenis_barang j", "j.id_jenis_barang = o.id_jenis_barang")
				->where("o.is_delete", 0)
				->where("j.is_delete", 0);
			
			// filter user level
			
			$orderColumn = array(
				2 => "j.id_jenis_barang",
				3 => "o.nama_barang",
				4 => "o.tanggal",
				5 => "o.jumlah",
				6 => "o.harga",
			);

			if (!empty($post['tanggal1'])) {
				$this->db->where("o.tanggal >= '$post[tanggal1]'");
			}
			if (!empty($post['tanggal2'])) {
				$this->db->where("o.tanggal <= '$post[tanggal2]'");
			}
			if (!empty($post['nama_barang'])) {
				$this->db->like("o.nama_barang", $post['nama_barang'], "both");
			}
			if (!empty($post['no_jenis'])) {
				$this->db->like("j.no_jenis", $post['no_jenis'], "both");
			}
			if (!empty($post['id_jenis_barang'])) {
				$this->db->where("o.id_jenis_barang", $post['id_jenis_barang']);
			}
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by("o.$this->tb_id", $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

		$this->db->stop_cache();

			// get num rows
			$this->db->select("o.$this->tb_id");
			$rowCount = $this->db->get()->num_rows();

			// get result
			$val = $this->db
				->select("
					j.jenis_barang,
					j.no_jenis,
					o.id_operasional,
					o.nama_barang,
					o.tanggal,
					o.jumlah,
					o.keterangan,
					(o.jumlah * o.harga) total,
					o.harga
				")
				->limit($post['length'], $post['start'])
				->get()
				->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}operasional/form/$data->id_operasional' id='btn-detail'>
					Edit
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_operasional' id='btn-delete'>
					Hapus
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";
			
			$baris = array(
				"no" => $no,
				"aksi" => $aksi,
				"jenis_barang" => "$data->jenis_barang <br> <small><b>$data->no_jenis</b></small>",
				"nama_barang" => $data->nama_barang,
				"tanggal" => konversi_tanggal($data->tanggal, "j F Y"),
				"jumlah" => $data->jumlah,
				"harga" => number_format($data->harga, 2, ',', '.'),
				"total" => number_format($data->total, 2, ',', '.'),
				"keterangan" => $data->keterangan,
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert($this->tb, $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where($this->tb_id, $id)
			->update($this->tb, array(
				"is_delete" => 1
			));
	}
	
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("x.$this->tb_id", $id)
			->get("$this->tb x");
	}
}