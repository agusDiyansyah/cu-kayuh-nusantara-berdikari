<?php echo $this->session->flashdata('msg'); ?>

<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="#" class="btn btn-default btn-tool-src">Pencarian Rinci</a>
				<a href="<?php echo $link_add ?>" class="btn btn-success btn-add">Tambah Data</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid pencarian" style="display: none;">
	<div class="row">
		<div class="col-xs-12">
			<form action="" method="POST" role="form" class="form-filter">

				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label for="">Nama Operasional</label>
							<?php echo $filter['nama_barang'] ?>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label for="">Tanggal</label>
							<div class="input-group">
								<?php echo $filter['tanggal1'] ?>
								<span class="input-group-addon" id="basic-addon1">s/d</span>
								<?php echo $filter['tanggal2'] ?>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label for="">Jenis</label>
							<?php echo $filter['id_jenis_barang'] ?>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label for="">Nomor Jenis</label>
							<?php echo $filter['no_jenis'] ?>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-success btn-cari">Cari</button>
				<button type="button" class="btn btn-default btn-reset">Reset</button>
			</form>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<table class="table table-list table-hover table-striped">
				<thead>
					<tr>
						<th width="2%">#</th>
						<th width="5%">Aksi</th>
						<th>Jenis</th>
						<th>Nama Operasional</th>
						<th>Tanggal</th>
						<th>Jumlah</th>
						<th>Harga</th>
						<th>Total</th>
						<th>Keterangan</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>