$(document).ready(function() {
	$('.s2').select2({
		width: "100%"
	});

	$(".date").datepicker({
		format: "yyyy-mm-dd",
		autoclose: true
	});

	$('.form').validate({
		ignore: [],
		errorClass: "error",
		rules:{
			id_jenis_barang: { required: true },
			nama_barang: { required: true },
			jumlah: { required: true },
			tanggal: { required: true },
			harga: { required: true },
		},
		messages:{
			id_jenis_barang: { required: "Jenis barang tidak boleh kosong" },
			nama_barang: { required: "Nama tidak boleh kosong" },
			jumlah: { required: "Jumlah tidak boleh kosong" },
			tanggal: { required: "Tanggal tidak boleh kosong" },
			harga: { required: "Harga tidak boleh kosong" },
		},
		errorPlacement : function (error, element) {
			error.insertAfter(element);
		},
		highlight : function (element, validClass) {
			$(element).parent().addClass('has-error');
		},
		unhighlight : function (element, validClass) {
			$(element).parent().removeClass('has-error');
		}
	});
});
