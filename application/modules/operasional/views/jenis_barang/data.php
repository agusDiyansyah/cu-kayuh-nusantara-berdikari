<?php echo $this->session->flashdata('msg'); ?>

<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="#" class="btn btn-default btn-tool-src">Pencarian Rinci</a>
				<a href="<?php echo $link_add ?>" class="btn btn-success btn-add">Tambah Data</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid pencarian" style="display: none;">
	<div class="row">
		<div class="col-xs-12">
			<form action="" method="POST" role="form" class="form-filter">

				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label for="">Jenis</label>
							<?php echo $filter['jenis_barang'] ?>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label for="">Nomor Jenis</label>
							<?php echo $filter['no_jenis'] ?>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-success btn-cari">Cari</button>
				<button type="button" class="btn btn-default btn-reset">Reset</button>
			</form>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<table class="table table-list table-hover table-striped">
				<thead>
					<tr>
						<th width="2%">#</th>
						<th width="5%">Aksi</th>
						<th>Nomor</th>
						<th>Jenis Operasional</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>