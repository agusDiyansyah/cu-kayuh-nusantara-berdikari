$(document).ready(function() {
	$(".table-list").DataTable({
		"ajax": {
			"url"    :"<?php echo base_url("operasional/jenis_barang/data") ?>",
			"method" :"POST",
			"data"   : function ( d ) {
				var filter = $(".form-filter");

				d.jenis_barang = filter.find('.jenis_barang').val();
				d.no_jenis = filter.find('.no_jenis').val();
			}
		},
		"dom": "<'row'<'col-sm-12'<'pull-right'<'toolbarindex'>><'pull-left'l>r<'clearfix'>>><'row'<'table-responsive'<'col-xs-12't>>><'row'<'col-sm-6'<'pull-left'<'toolbar'> i>><'col-sm-6'<'pull-right'p>><'clearfix'>>",
		"columns": [
			{"data": "no"},
			{"data": "aksi"},
			{"data": "no_jenis"},
			{"data": "jenis_barang"},
		],

		"pageLength"  : 100,
		"deferRender" : true,
		"serverSide"  : true,
		"processing"  : false,
		"filter"      : false,
		"ordering"    : true,
		"bLengthChange": false,

		"order": [[ 0, "desc" ]],

		"columnDefs": [
			{ "targets": 0, "orderable": false },
			{ "targets": 1, "orderable": false },
		],

		"language": {
			"sProcessing"   : "Sedang memproses...",
			"sLengthMenu"   : "Tampilkan _MENU_ entri",
			"sZeroRecords"  : "Tidak ditemukan data",
			"sInfo"         : "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
			"sInfoEmpty"    : "Menampilkan 0 sampai 0 dari 0 entri",
			"sInfoFiltered" : "(difilter dari _MAX_ entri keseluruhan)",
			"sInfoPostFix"  : "",
			"sUrl"          : "",
			"oPaginate"     : {
				"sFirst"        : "Pertama",
				"sPrevious"     : "<<",
				"sNext"         : ">>",
				"sLast"         : "Terakhir"
			}
		},
	});

	$(".table-list").on("click", "#btn-delete", function(event) {
		var id = $(this).data("id");
		
		swal({
			title: "Apakah anda yakin",
			text: "Anda akan menghapus data ini",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#f4516c",
			cencelButtonColor: "#eaeaea",
			confirmButtonText: "Hapus Data",
			cancelButtonText: "Batalkan",
			showLoaderOnConfirm: true,
			closeOnConfirm: true,
			closeOnCancel: true
		},
		function (isConfirm) {
			if (isConfirm) {
				$.ajax({
					url: "<?php echo base_url("operasional/jenis_barang/delete_proses") ?>",
					type: "post",
					dataType: "json",
					data: { id: id },
					success: function (json) {
						if (json.stat) {
							notif(true, "Data berhasil di hapus");
							refreshTable();
						} else {
							notif(false, "Data gagal di hapus");
						}
					}
				});
			}
		});
	});

	$(".form-filter").validate({
		submitHandler : function(form) {
			refreshTable();
			return false;
		}
	});

	$(".form-filter").on('click', '.btn-reset', function(event) {
		event.preventDefault();

		$(".form-filter").find("input").val("");

		refreshTable();
	});
});

function refreshTable () {
	var dtable = $(".table-list").DataTable();
	dtable.draw();
}