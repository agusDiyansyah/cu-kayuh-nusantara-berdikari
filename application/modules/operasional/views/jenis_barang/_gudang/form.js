$(document).ready(function() {
	$('.form').validate({
		ignore: [],
		errorClass: "error",
		rules:{
			jenis_barang: { required:true },
		},
		messages:{
			jenis_barang: { required: "Jenis barang tidak boleh kosong" },
		},
		errorPlacement : function (error, element) {
			error.insertAfter(element);
		},
		highlight : function (element, validClass) {
			$(element).parent().addClass('has-error');
		},
		unhighlight : function (element, validClass) {
			$(element).parent().removeClass('has-error');
		}
	});
});
