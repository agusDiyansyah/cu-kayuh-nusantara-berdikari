<?php echo $this->session->flashdata('msg'); ?>
<div class="container-fluid action-button">
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<form class="form" action="<?php echo $form_action ?>" method="post">
				<div class="hide">
					<?php echo $input['hide']['id'] ?>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<span style="font-weight: 600">FORM JENIS OPERASIONAL</span>
						<hr style="margin: 10px 0px;">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="" class="control-label">Jenis Operasional</label>
							<?php echo $input['jenis_barang'] ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="" class="control-label">Nomor Jenis</label>
							<?php echo $input['no_jenis'] ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="pull-right">
							<a href="<?php echo $link_back ?>" class="btn btn-default">Kembali</a>
							<button type="submit" name="button" class="btn btn-success">Proses</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>