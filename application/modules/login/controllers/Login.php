<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	private $msg = "Data gagal di proses";
	private $stat = false;
	
	private $redirectSuccsessModule;
	private $redirectDestroyModule = "login";

	public function __construct () {
		parent::__construct ();
		
		$this->redirectSuccsessModule = "home";
		if (!empty($this->session->flashdata("last_module"))) {
			$this->redirectSuccsessModule = $this->session->flashdata('last_module');
		}
		
		$this->output->set_title("CU Kayuh Nusantara Berdikari");
		$this->load->model("M_login");
	}

	public function index () {
		// UNTUK MENGECEK APAKAH SUDAH LOGIN
		if ($this->session->userdata('login') == true) {
			$notif = _notif(true, "Sudah Login", "Selamat Datang ".$this->session->userdata('nama_lengkap'));
			$this->session->set_flashdata('message', $notif);
			redirect($this->redirectSuccsessModule,'refresh');
		}

		$data = array(
			"username" => form_input(array(
				"type" => "text",
				"class" => "form-control username",
				"id" => "username",
				"name" => "username",
				"required" => "true",
			)),
			"pass" => form_input(array(
				"type" => "password",
				"class" => "form-control pass",
				"id" => "pass",
				"name" => "pass",
				"required" => "true",
			)),
		);

		$this->load->view("form", $data);
	}

	public function login_proses () {

		if ($this->input->post()) {
			$this->load->library('form_validation');
			$this->load->helper('security');
			$_backto = $this->redirectDestroyModule;

			//validasi form
			$config = array(
				array(
					"field" => "username",
					"label" => "Username",
					"rules" => "required|xss_clean",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "pass",
					"label" => "Password",
					"rules" => "required|xss_clean|trim",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class='text-danger'>", "</div>");
			$this->form_validation->set_rules($config);

			//jika validasi sukses
			if ($this->form_validation->run() == TRUE) {
				$username = $this->input->post('username');
				$password = $this->input->post('pass');
				
				$admin = $this->M_login->getUserData($username);
				
				$exp = $admin->forgot_exp;
				$proses = false;

				if ( !empty($exp) ) {
					$now = strtotime( date("Y-m-d H:i:s") );
					$exp = strtotime($exp);

					if ($now > $exp) {
						$proses = false;
						$this->msg = "Password yang anda inputkan tidak bisa di gunakan";
					} else {
						$proses = true;
					}

				} else {
					$proses = true;
				}

				if ($proses) {
					if (password_verify($password, $admin->pass)) {
						// $image = ( empty($admin->avatar) ) ? "" : base_url("assets/upload/image/user/thumb/$admin->avatar");
						$data = array(
							"id_user" => $admin->id_user,
							"username" => $admin->username,
							"pass" => $admin->pass,
							"nama_lengkap" => $admin->nama_lengkap,
							// "avatar" => $image,
							"login" => true,
							"upload_image_file_manager" => true,
						);

						$this->session->set_userdata($data);

						$ip = $this->input->ip_address();
						$server = $this->input->server("HTTP_USER_AGENT");

						$this->M_login->setIpAddress($username, $ip );

						$this->stat = true;
						$_backto = $this->redirectSuccsessModule;
					} else {
						$this->msg = "Maaf, nama dan atau sandi Anda salah";
					}
				}
			}

			redirect($_backto);
		} else {
			show_404();
		}
	}

	public function forgot () {
		$email = $this->input->post('email');
		$stat = false;
		$kode = "";

		if ( empty($email) ) {
			show_404();
		} else {
			$sql = $this->M_login->cekEmail($email);
			if ($sql->num_rows() > 0) {
				$stat = true;

				$exp = date('Y-m-d', strtotime('+1 days', strtotime( date("Y-m-d") ))) . " 00:00:00";
				$new_pass = strtoupper(substr(md5($email.time()),2,7));
				$pass = password_hash($new_pass, PASSWORD_BCRYPT, array('cost' => 12));

				$this->M_login->ubahPassword($email, $pass, $exp);
			}

		}

		echo json_encode(array(
			"stat" => $stat,
			"exp" => konversi_tanggal($exp, "j F Y H:i:s")
		));
	}

	public function cek_login () {
		$status_login = $this->session->userdata('login');
		$username = $this->session->userdata('username');
		$pass = $this->session->userdata('pass');
		
		$sql = $this->db
			->select("id_user")
			->where("username", $username)
			->where("pass", $pass)
			->get("user", 1);
			
		if (!isset($status_login) || $status_login != TRUE || $sql->num_rows() == 0) {
			$this->session->sess_destroy();
			redirect($this->redirectDestroyModule);
		} else {
		}
	}

	public function terlarang ($level_forbiden) {
		if ($this->session->userdata('level') == $level_forbiden) {
			redirect('login/forbiden');
		}
	}

	public function logout () {
		$this->session->sess_destroy();
		redirect($this->redirectDestroyModule);
	}

	public function forbiden () {
		$this->output->set_template('adminLTE/default');
		$this->output->set_title($this->title);
		$data['title'] = "Forbiden Area";
		$data['subtitle'] = "Anda tidak memiliki hak akses ke halaman ini";
		$this->logout();
		$this->load->view('forbiden', $data);
	}

	public function check_captcha ($val) {
		if ($this->recaptcha->check_answer($this->input->ip_address(), $this->input->post('recaptcha_challenge_field'), $val)) {
			return TRUE;
		}
		
		$this->form_validation->set_message('check_captcha', $this->lang->line('recaptcha_incorrect_response'));
		return FALSE;
	}
}