<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_controller {
	private $title = "Home";
	private $module = "home";
	private $module_view = "home/home";

	// PUBLIC
		public function __construct () {
			parent::__construct ();

			$this->output->script_foot("$this->module_view/_gudang/_global.js");

			$this->load->model("M_home", "M_app");
		}

		public function index () {
			$data = array();

			$this->load->view("$this->module_view/data", $data);
		}
		
		public function data () {
			
		}
		
		public function form ($id = 0) {
			if ($id > 0 AND is_int($id)) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}
		
		public function proses () {
			$id = $this->input->post("id");
			
			if (!empty($id) AND is_int($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}
	
	// PRIVATE
		private function add () {
			
		}
		
		private function edit ($id) {
			
		}
		
		private function add_proses () {
			
		}
		
		private function edit_proses ($id) {
			
		}
		
		public function delete_proses () {
			
		}
		
		private function _formInputData () {
			
		}
		
		private function _formPostInputData () {
			
		}
		
		private function _formPostProsesError () {
			
		}
		
		private function _rules () {
			
		}
}