<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends MX_Controller {

	public function __construct () {
		parent::__construct ();
		Modules::run("login/cek_login");
		$this->output->set_title("CU Kayuh Nusantara Berdikari");
		$this->output->set_template("admin/default");
		$this->output->set_output_data("conf", $this->config->load("appconf", true));

		$this->_grant();
	}

	public function msg ($type = "success", $msg = "Awesome") {
		$this->output->script_foot_string('
		$(document).ready(function () {
			var template = "";
			
			template += \'<div class="notif notif-tmp">\';
			template += \'	<div style="border-radius: 0px; padding: 21px;" class="alert alert-'.$type.'" role="alert"> \';
			template += \'		'.$msg.'\';
			template += \'	</div>\';
			template += \'</div>\';

			$("header").after(template);

			$(".notif-tmp").click(function () {
				$(this).remove();
			});
		});
		');
	}

	private function _grant () {
		$conf = $this->config->load("appconf", true);
		$link = "";

		for ($i = 1; $i <= $this->uri->total_segments(); $i++) { 
			if (!empty($this->uri->segment($i)) AND !is_numeric($this->uri->segment($i))) {
				$link .= $this->uri->segment($i) . "/";
			}
		}

		$grant = array(
			"host" => array(
				"home/",
				"login/",

				// anggota
				"member/",
				"member/form/",
				"member/data/",
				"member/proses/",
				"member/detail/",
				
				"member/administrasi/getResume/",
				"member/administrasi/data/",
				"member/administrasi/angsuran/",
				"member/administrasi/form/",
				"member/administrasi/proses/",

				// kelompok
				"member/kelompok/",
				"member/kelompok/form/",
				"member/kelompok/proses/",
				"member/kelompok/edit_detail/",
				"member/kelompok/detail/",

				// simpanan anggota
				"simpanan/bunga/",
				"simpanan/bunga/getResume/",
				"simpanan/bunga/data/",
				"simpanan/bunga/form/",
				"simpanan/bunga/proses/",

				"simpanan/anggota/transaksi/",
				"simpanan/anggota/getTotalSimpanan/",
				"simpanan/anggota/data/",
				"simpanan/anggota/form/",
				"simpanan/anggota/proses/",

				"simpanan/penarikan/transaksi/",
				"simpanan/penarikan/form/",
				"simpanan/penarikan/proses/",

				"simpanan/anggota_non/transaksi/",
				"simpanan/anggota_non/form/",
				"simpanan/anggota_non/proses/",

				// simpanan kelompok
				"simpanan/bunga/kelompok/",
				"simpanan/bunga/kelompok/form",
				"simpanan/kelompok/transaksi/",
				"simpanan/kelompok/form/",
				"simpanan/kelompok/proses/",
				
				"simpanan/penarikan/kelompok/",
				"simpanan/penarikan/kelompok/form/",
				"simpanan/penarikan/kelompok/proses/",

				// pinjaman anggota
				"pinjaman/form/",
				"pinjaman/proses/",
				"pinjaman/angsuran/transaksi/",

				// pinjaman kelompok
				"pinjaman/kelompok/form/",
				"pinjaman/kelompok/proses/",
				"pinjaman/angsuran/kelompok/",
				
				// operasional
				"operasional/",
				"operasional/form/",
				"operasional/proses/",
				
				"operasional/jenis_barang/",
				"operasional/jenis_barang/form/",
				"operasional/jenis_barang/proses/",

				// laporan
				"laporan/administrasi/",
				"laporan/simpanan/",
				"laporan/simpanan_bunga/",
				"laporan/simpanan_penarikan/",
				"laporan/pinjaman/",
				"laporan/pinjaman_angsuran/",
				"laporan/operasional/",

				"laporan/administrasi/laporan/",
				"laporan/simpanan/laporan/",
				"laporan/simpanan_bunga/laporan/",
				"laporan/simpanan_penarikan/laporan/",
				"laporan/pinjaman/laporan/",
				"laporan/pinjaman_angsuran/laporan/",
				"laporan/operasional/laporan/",
			),
			"remote" => array(
				"home/",
				"login/",

				// laporan
				"laporan/administrasi/",
				"laporan/simpanan/",
				"laporan/simpanan_bunga/",
				"laporan/simpanan_penarikan/",
				"laporan/pinjaman/",
				"laporan/pinjaman_angsuran/",
				"laporan/operasional/",

				"laporan/administrasi/laporan/",
				"laporan/simpanan/laporan/",
				"laporan/simpanan_bunga/laporan/",
				"laporan/simpanan_penarikan/laporan/",
				"laporan/pinjaman/laporan/",
				"laporan/pinjaman_angsuran/laporan/",
				"laporan/operasional/laporan/",
			)
		);

		$cek = "ok";
		if (!in_array($link, $grant[$conf['request_from']]) AND !$this->input->is_ajax_request()) {
			show_404();
		}
	}
	
}