<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Public_controller extends MX_Controller {

	public function __construct () {
		parent::__construct ();
		
		$this->output->set_output_data("kategori", $this->db->get("produk_kategori"));
		$this->output->set_output_data("page", $this->db
			->where_not_in("id_page", array(1, 2))
			->get("page"));
		$this->output->set_template("public/default");
	}
	
}