<aside class="sidebar">
	<div class="app-name">
		<div class="title">
			<span>CU</span>KNB
			<!-- <span><?php echo $this->uri->segment(1); ?></span> -->
			<small><p>KAYUH NUSANTARA BERDIKARI</p></small>
		</div>
		<div class="alt">M</div>
	</div>
	<ul class="sidebar-menu">
		<?php if ($conf['request_from'] == "host"): ?>
			<li class="divider">Menu Utama</li>

			<li class="mn-home">
				<a href="<?php echo site_url("home") ?>">
					<div class="icon"><i class="fa fa-home"></i></div><span>Home</span>
				</a>
			</li>
			
			<li class="divider">Keanggotaan</li>
			
			<li class="mn-member">
				<a href="<?php echo site_url("member") ?>">
					<div class="icon"><i class="fa fa-user"></i></div><span>Anggota</span>
				</a>
			</li>
			
			<li class="mn-kelompok">
				<a href="<?php echo site_url("member/kelompok") ?>">
					<div class="icon"><i class="fa fa-users"></i></div><span>Kelompok</span>
				</a>
			</li>
			
			<li class="divider">Operasional</li>

			<li class="mn-operasional">
				<a href="<?php echo site_url("operasional") ?>">
					<div class="icon"><i class="fa fa-book"></i></div><span>Operasional</span>
				</a>
			</li>

			<li class="mn-operasional-jenis_barang">
				<a href="<?php echo site_url("operasional/jenis_barang") ?>">
					<div class="icon"><i class="fa fa-list"></i></div><span>Jenis Operasional</span>
				</a>
			</li>
		<?php endif ?>

		<li class="divider">Laporan</li>
		
		<li class="mn-laporan-administrasi">
			<a href="<?php echo site_url("laporan/administrasi") ?>">
				<div class="icon"><i class="fa fa-book"></i></div><span>Administrasi</span>
			</a>
		</li>

		<li class="mn-laporan-simpanan dropdown-sidebar">
			<a href="mailbox.html">
				<div class="icon">
					<i class="fa fa-book"></i>
				</div>
				<span>Simpanan</span>
			</a>
			<ul class="sidebar-menu">
				<li class="mn-laporan-simpanan-angsuran">
					<a href="<?php echo site_url("laporan/simpanan") ?>">Angsuran Simpanan
						<div class="icon"><i class="fa fa-circle-o"></i></div>
					</a>
				</li>

				<li class="mn-laporan-simpanan-bunga">
					<a href="<?php echo site_url("laporan/simpanan_bunga") ?>">Bunga Simpanan
						<div class="icon"><i class="fa fa-circle-o"></i></div>
					</a>
				</li>

				<li class="mn-laporan-simpanan-penarikan">
					<a href="<?php echo site_url("laporan/simpanan_penarikan") ?>">Penarikan Dana
						<div class="icon"><i class="fa fa-circle-o"></i></div>
					</a>
				</li>
			</ul>
		</li>

		<li class="mn-laporan-pinjaman dropdown-sidebar">
			<a href="mailbox.html">
				<div class="icon">
					<i class="fa fa-book"></i>
				</div>
				<span>Pinjaman</span>
			</a>
			<ul class="sidebar-menu">
				<li class="mn-laporan-pinjaman-list">
					<a href="<?php echo site_url("laporan/pinjaman") ?>">Laporan Pinjaman
						<div class="icon"><i class="fa fa-circle-o"></i></div>
					</a>
				</li>

				<li class="mn-laporan-pinjaman-angsuran">
					<a href="<?php echo site_url("laporan/pinjaman_angsuran") ?>">Angsuran
						<div class="icon"><i class="fa fa-circle-o"></i></div>
					</a>
				</li>
			</ul>
		</li>

		<li class="mn-laporan-operasional">
			<a href="<?php echo site_url("laporan/operasional") ?>">
				<div class="icon"><i class="fa fa-book"></i></div><span>Operasional</span>
			</a>
		</li>

		<!-- <li class="divider">Advanced</li>

		<li class="mn-user">
			<a href="<?php echo site_url("user") ?>">
				<div class="icon"><i class="fa fa-users"></i></div><span>Pengguna</span>
			</a>
		</li> -->
	</ul>
</aside>