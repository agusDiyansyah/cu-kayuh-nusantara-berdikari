<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title ?></title>

		<style>
			body {
				margin: 0px;
				font-family: "Arial";
				font-size: 12px;
			}
			.right-align {
				text-align: right
			}
			.left-align {
				text-align: left
			}
			.center-align {
				text-align: center
			}
			.tool {
				background-color: #E2E8E8;
				text-align: right;
				border-bottom: 1px solid #000;
			}
			.btn-tool {
				padding: 12px 14px;
				display: inline-block;
				color: #E1E7E7;
				background-color: #35475A;
				border-left: 1px solid #A3B1B7;
				text-decoration: none;
			}
			.btn-print {
				margin-right: -4px;
			}
			.btn-tool:hover {
				background-color: #2B3A4A
			}
			.title {
				margin-top: 15px;
				margin-bottom: 100px;
			}
			.title h2 {
				margin-top: 5px;
				margin-bottom: 0px;
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, .foot {
				font-size: 12.5px;
			}
			table, td, th {
				border: 1px solid #000;
			}
			table th, table td {
				padding: 5px;
				vertical-align: top;
			}
			table thead tr {
				background-color: #E1E7E7 !important;
			}
			.foot {
				margin-top: 100px;
				text-align: right;
				padding-bottom: 100px;
				padding-right: 100px;
			}
			@media print {
				.tool {
					display: none !important;
				}
			}
		</style>
		
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/jquery/dist/jquery.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/excel/export_excel.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".btn-print").click(function(event) {
					event.preventDefault();
					window.print();
				});
				$(".btn-excel").click(function(event) {
					event.preventDefault();
					$("#laporan").btechco_excelexport({
						containerid: "laporan",
						datatype: $datatype.Table
					});
				});
			});
		</script>

		<?php echo $script_head ?>
	</head>
	<body>
		<div class="tool">
			<a href="" class="btn-tool btn-print">PRINT</a>
			<a href="" class="btn-tool btn-excel">EXPORT TO EXCEL</a>
		</div>
		<div class="laporan" id="laporan">
			<?php echo $output ?>
		</div>
	</body>
	<?php echo $script_foot ?>
</html>