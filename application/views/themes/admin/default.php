<!DOCTYPE html>
<html class="undefined">
	<head>
		<title>
			<?php echo $title; ?>
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<!-- css vendor-->
		<?php
		if (!empty($meta)) {
			foreach($meta as $name=>$content){
				echo "\n\t\t";
				?>
					<meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" />
				<?php
			}
		}
		echo "\n";

		if (!empty($canonical)) {
			echo "\n\t\t";
			?>
				<link rel="canonical" href="<?php echo $canonical?>" />
			<?php

		}
		echo "\n\t";
		?>

		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/stylesheets/default-color-scheme.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/stylesheets/default-style.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/stylesheets/custom-style.css">

		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/themes/admin/vendors/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/themes/admin/vendors/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/themes/admin/vendors/ionicons/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/themes/admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/sweetalert/sweetalert.css">
		<!-- specific css on page-->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/stylesheets/font.css">
		<!-- custom css-->
		<?php echo $script_head ?>

		<?php 
			foreach ($css as $file) {
				echo "\n\t\t";
				?>
					<link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" />
				<?php
			} 
			echo "\n\t";
		?>

		<!-- javascript vendor-->
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/jquery/dist/jquery.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/javascripts/main.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/javascripts/custom.js"></script>
		<!-- specific js on page-->

		<?php 
			foreach ($js as $file) {
				echo "\n\t\t";
				?>
				<script src="<?php echo $file; ?>"></script>
				<?php
			} 
			echo "\n\t";
		?>

	</head>
	<body>
		<div class="check-device"></div>
		
		<?php echo $this->load->view('themes/admin/menu'); ?>

		<section class="wrapper">
			<header>
				<div class="body-header">
					<div class="sidebar-action pull-left"><i class="ion-chevron-left hidden-xs"></i><i class="ion-chevron-right visible-xs"></i></div>
					<div class="right-sidebar-action pull-right"><i class="fa fa-bars"></i></div>
					<!-- user-->
					<div class="user-header pull-right">
						<div style="background-image: url('<?php echo base_url() ?>assets/themes/admin/images/thumb-user-photo.jpg')" class="photo"></div>
						<div class="user-text"><span class="title-user"><?= $this->session->userdata('nama_lengkap'); ?></span><span class="subtitle-user">Administrator</span></div>
						<div class="user-tool">
							<ul>
								<li><a href="<?php echo base_url('user/form/'.$this->session->userdata('id_user')) ?>"><i class="ion-edit"></i>Edit profil</a></li>
								<li><a href="#"><i class="ion-link"></i>Go to website</a></li>
								<li>
									<a href="<?php echo base_url('login/logout') ?>">
										<i class="ion-power"></i>Sign-out
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- end of user-->
					<!-- navigation-->
					<!-- <nav class="navigation pull-right"><span class="btn-nav-mobile pull-right"><i class="ion-chevron-down"></i></span>
						<ul>
							<li class="drop-down"><a href="#">Dropdown</a>
								<ul>
									<li><a href="#"><span>Project</span><span class="badge badge-primary pull-right">5</span></a></li>
									<li><a href="#"><span>Task</span><span class="badge badge-success pull-right">999</span></a></li>
									<li><a href="#"><span>Email</span><span class="badge badge-danger pull-right">73</span></a></li>
									<li><a href="#">Others</a></li>
								</ul>
							</li>
							<li class="megamenu"><a href="#">Megamenu</a>
								<ul>
									<li>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
											cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
											proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
										</p>
									</li>
								</ul>
							</li>
							<li class="search-header"><a href="#"><i class="fa fa-search"></i></a>
								<ul>
									<li>
										<form>
											<input type="text" placeholder="Search Here">
										</form>
									</li>
								</ul>
							</li>
						</ul>
					</nav> -->
					<!-- <nav class="navigation pull-left">
						<ul class="notification-wrapper">
							<li class="notification"><a href="#"><i class="fa fa-bell"></i><span class="badge badge-primary">5</span></a>
								<ul>
									<li>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											consequat.
										</p>
									</li>
								</ul>
							</li>
							<li class="notification"><a href="#"><i class="fa fa-envelope"></i><span class="badge badge-success">20</span></a>
								<ul>
									<li>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											consequat.
										</p>
									</li>
								</ul>
							</li>
						</ul>
					</nav> -->
					<!-- end of navigation-->
				</div>
			</header>
			
			<?php echo $output;?>
			<!-- end of breadcrumb-->
			<div class="container-fluid">
				<!-- right sidebar-->
				<div class="right-sidebar">
					<div class="body-right-sidebar">
						<div class="user">
							<div class="user-wrapper">
								<div class="user-photo"><img src="<?php echo base_url() ?>assets/themes/admin/images/thumb-user-photo.jpg"></div><span class="user-name"><?= $this->session->userdata('nama_lengkap'); ?></span><span class="user-level">administrator</span>
							</div>
							<a href="<?php echo base_url('user/form/'.$this->session->userdata('id_user')) ?>"> <i class="ion-edit"></i>Edit Profile</a>
							<a href="#"> <i class="ion-link"></i>Go To Website</a>
							<a href="<?php echo base_url('login/logout') ?>">
								<i class="ion-power"></i>Sign-out
							</a>
						</div>
						<h4 class="right-sidebar-heading">Online People</h4>
						<div class="online-people">
							<div class="list-online-people">
								<figure class="avatar avatar-sm avatar-rounded pull-left mr-md"><img src="<?php echo base_url() ?>assets/themes/admin/images/thumb-user-photo.jpg"></figure><span class="online-status green"></span><span class="name-user">Deddy Febriyadi</span><span class="status-user">online 2 minute ago</span>
							</div>
							<div class="list-online-people">
								<figure class="avatar avatar-sm avatar-rounded pull-left mr-md"><img src="<?php echo base_url() ?>assets/themes/admin/images/thumb-user-photo-2.jpg"></figure><span class="online-status green"></span><span class="name-user">Adi Purwan</span><span class="status-user">online 4 minute ago</span>
							</div>
							<div class="list-online-people">
								<figure class="avatar avatar-sm avatar-rounded pull-left mr-md"><img src="<?php echo base_url() ?>assets/themes/admin/images/thumb-user-photo-3.jpg"></figure><span class="online-status yellow"></span><span class="name-user">Ridwan Tasa Dirsa</span><span class="status-user">online 6 minute ago</span>
							</div>
							<div class="list-online-people">
								<figure class="avatar avatar-sm avatar-rounded pull-left mr-md"><img src="<?php echo base_url() ?>assets/themes/admin/images/thumb-user-photo-4.jpg"></figure><span class="online-status yellow"></span><span class="name-user">Agus Diyansyah</span><span class="status-user">online 8 minute ago</span>
							</div>
							<div class="list-online-people">
								<figure class="avatar avatar-sm avatar-rounded pull-left mr-md"><img src="<?php echo base_url() ?>assets/themes/admin/images/thumb-user-photo-5.jpg"></figure><span class="online-status red"></span><span class="name-user">Tri Akasa</span><span class="status-user">online 10 minute ago</span>
							</div>
						</div><a href="#" class="btn btn-info btn-block mt-md mb-xl">More People</a>
						<h4 class="right-sidebar-heading">Timeline Activity</h4>
						<div class="sidebar-timeline">
							<div class="list-timeline primary"><span class="datetime-timeline">07:00</span>
								<div class="timeline-item"><strong>Deddy Febriyadi</strong> start following you</div>
							</div>
							<div class="list-timeline primary"><span class="datetime-timeline">08:00</span>
								<div class="timeline-item"><strong>Agus Diyansyah</strong> start following you</div>
							</div>
							<div class="list-timeline primary"><span class="datetime-timeline">09:00</span>
								<div class="timeline-item"><strong>Tri Akasa</strong> send you email</div>
							</div>
							<div class="list-timeline info"><span class="datetime-timeline">13:00</span>
								<div class="timeline-item"><strong>Agus Diyansyah</strong> added task</div>
							</div>
							<div class="list-timeline info"><span class="datetime-timeline">14:00</span>
								<div class="timeline-item"><strong>Ridwan Tasa Dirsa</strong> added task</div>
							</div>
							<div class="list-timeline warning"><span class="datetime-timeline">15:30</span>
								<div class="timeline-item"><strong>Deddy Febriyadi</strong> added project</div>
							</div>
							<div class="list-timeline warning"><span class="datetime-timeline">16:00</span>
								<div class="timeline-item"><strong>Adi Purwan</strong> added project</div>
							</div>
							<div class="list-timeline danger"><span class="datetime-timeline">17:30</span>
								<div class="timeline-item"><strong>Deddy Febriyadi</strong> remove project</div>
							</div>
							<div class="list-timeline danger"><span class="datetime-timeline">19:00</span>
								<div class="timeline-item"><strong>Adi Purwan</strong> remove task</div>
							</div>
							<div class="list-timeline default"><span class="datetime-timeline">20:30</span>
								<div class="timeline-item"><strong>Deddy Febriyadi</strong> send you message</div>
							</div>
							<div class="list-timeline default"><span class="datetime-timeline">21:00</span>
								<div class="timeline-item"><strong>Adi Purwan</strong> send you message</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php echo $script_foot ?>
	</body>
</html>