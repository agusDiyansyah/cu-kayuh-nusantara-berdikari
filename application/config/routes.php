<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';

// SIMPANAN
	$route['simpanan/(:num)'] = 'simpanan/index/$1';
	$route['simpanan/kelompok/transaksi/(:num)'] = 'simpanan/simpanan_kelompok/transaksi/$1';
	$route['simpanan/kelompok/form/(:num)'] = 'simpanan/simpanan_kelompok/form/$1';
	$route['simpanan/kelompok/form/(:num)/(:num)'] = 'simpanan/simpanan_kelompok/form/$1/$2';
	$route['simpanan/kelompok/proses'] = 'simpanan/simpanan_kelompok/proses';
// END OF SIMPANAN

// SIMPANAN PENARIKAN
	$route['simpanan/penarikan/kelompok/(:num)'] = 'simpanan/penarikan_kelompok/transaksi/$1';
	$route['simpanan/penarikan/kelompok/form/(:num)'] = 'simpanan/penarikan_kelompok/form/$1';
	$route['simpanan/penarikan/kelompok/form/(:num)/(:num)'] = 'simpanan/penarikan_kelompok/form/$1/$2';
	$route['simpanan/penarikan/kelompok/proses'] = 'simpanan/penarikan_kelompok/proses';
// END OF SIMPANAN PENARIKAN

// SIMPANAN BUNGA
	$route['simpanan/bunga/(:num)'] = 'simpanan/bunga/index/$1';

	$route['simpanan/bunga/kelompok/(:num)'] = 'simpanan/bunga_kelompok/index/$1';
	$route['simpanan/bunga/kelompok/form/(:num)'] = 'simpanan/bunga_kelompok/form/$1';
	$route['simpanan/bunga/kelompok/form/(:num)/(:num)'] = 'simpanan/bunga_kelompok/form/$1/$2';
	$route['simpanan/bunga/kelompok/proses'] = 'simpanan/bunga_kelompok/proses';
// END OF SIMPANAN BUNGA

// PINJAMAN MEMBER
	$route['pinjaman/(:num)'] = 'pinjaman/index/$1';
// END OF PINJAMAN MEMBER

// ADMINISTRASI MEMBER ANGGOTA
	$route['member/administrasi/(:num)'] = 'member/administrasi/index/$1';
// END OF ADMINISTRASI MEMBER ANGGOTA

// PINJAMAN KELOMPOK
	$route['pinjaman/kelompok/form/(:num)'] = 'pinjaman/pinjaman_kelompok/form/$1';
	$route['pinjaman/kelompok/form/(:num)/(:num)'] = 'pinjaman/pinjaman_kelompok/form/$1/$2';
	$route['pinjaman/kelompok/proses'] = 'pinjaman/pinjaman_kelompok/proses';
	$route['pinjaman/kelompok/delete_proses'] = 'pinjaman/pinjaman_kelompok/delete_proses';
// END OF PINJAMAN KELOMPOK

// PINJAMAN ANGSURAN KELOMPOK
	$route['pinjaman/angsuran/kelompok/(:num)'] = 'pinjaman/angsuran_kelompok/transaksi/$1';
	$route['pinjaman/angsuran/kelompok/form/(:num)'] = 'pinjaman/angsuran_kelompok/form/$1';
	$route['pinjaman/angsuran/kelompok/form/(:num)/(:num)'] = 'pinjaman/angsuran_kelompok/form/$1/$2';
	$route['pinjaman/angsuran/kelompok/proses'] = 'pinjaman/angsuran_kelompok/proses';
// END OF PINJAMAN ANGSURAN KELOMPOK

// $route['404_override'] = 'dashboard';
$route['translate_uri_dashes'] = FALSE;