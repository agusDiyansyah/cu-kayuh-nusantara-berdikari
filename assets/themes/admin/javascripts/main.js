$(document).ready(function(){
	//COLLAPSE DROPDOWN SIDEBAR
	(function($){
		$(".sidebar-menu > .dropdown-sidebar > a").click(function(e){
			var parentDropdown = $(this).parent(".dropdown-sidebar");
			var otherParentDropdown = $(this).parent(".dropdown-sidebar").parent(".sidebar-menu").find('.dropdown-sidebar');
			var thisDropdown = $(this).next();
			var otherDropdown = $(this).parent(".dropdown-sidebar").parent(".sidebar-menu").find('.dropdown-sidebar > ul');
	
			if (parentDropdown.hasClass("collapsed")) {
				thisDropdown.slideUp(300, function() {
					parentDropdown.removeClass("collapsed");
				});
			} else {
				otherDropdown.slideUp(300, function(){
					otherParentDropdown.removeClass("collapsed");
				});
				thisDropdown.slideDown(300, function(){
					parentDropdown.addClass("collapsed");
				});
			}
			
			e.preventDefault();
		});
	}(jQuery));
	
	//HIDE OR SHOW SIDEBAR
	(function($){
		"use strict";

		var attrLayout = $("html").attr("class");

		if(attrLayout == "thin-sidebar"){
			$(".sidebar-action, .sidebar, .wrapper, header").addClass("changed");
		}

		$(".sidebar-action").click(function(){
			var device = $(".check-device").css("width");
			
			if ($(this).find('i.hidden-xs').hasClass('ion-chevron-left')) {
				$(this).find('i.hidden-xs')
					.removeClass('ion-chevron-left')
					.addClass('ion-chevron-right');
			} else {
				$(this).find('i.hidden-xs')
					.removeClass('ion-chevron-right')
					.addClass('ion-chevron-left');
			}
			
			if ($(this).find('i.visible-xs').hasClass('ion-chevron-left')) {
				$(this).find('i.visible-xs')
					.removeClass('ion-chevron-left')
					.addClass('ion-chevron-right');
			} else {
				$(this).find('i.visible-xs')
					.removeClass('ion-chevron-right')
					.addClass('ion-chevron-left');
			}
			
			if ($(".btn-nav-mobile").hasClass('active')) {
				$(".btn-nav-mobile").find('i')
					.removeClass('ion-chevron-up')
					.addClass('ion-chevron-down');
			}

			if (device == "1px") {
				$(this).toggleClass("changed");
				$(".sidebar, .wrapper, header").toggleClass("changed");

				$(this).removeClass("mb-sb-active");
				$("body, .sidebar, .wrapper, header").removeClass("mb-sb-active");

				$(".btn-nav-mobile").removeClass("active");
				$(".navigation > ul").removeClass("collapse");

			} else {
				$(this).toggleClass("mb-sb-active");
				$("body, .sidebar, .wrapper, header").toggleClass("mb-sb-active");

				$(".btn-nav-mobile").removeClass("active");
				$(".navigation > ul").removeClass("collapse");
			}
		});
	}(jQuery));

	//NAVIGATION
	function navigation(navClass){
		"use strict";

		$(navClass).click(function(e){

			var otherNav = $(".navigation > ul > li > a");

			if($(this).parent().hasClass("collapsed")){
				$(this).parent().removeClass("collapsed");
				$(this).removeClass("active");
			}else{
				otherNav.parent().removeClass("collapsed");
				otherNav.removeClass("active");
				$(this).parent().addClass("collapsed");
				$(this).addClass("active");
			}

			e.preventDefault();
		});
	}

	//HIDE OR SHOW DROPDOWN NAVIGATION
	navigation(".navigation > ul > li.drop-down > a");

	//HIDE OR SHOW MEGAMENU NAVIGATION
	navigation(".navigation > ul > li.megamenu > a");

	//HIDE OR SHOW NOTIFICATION NAVIGATION
	navigation(".navigation > ul > li.notification > a");

	//HIDE OR SHOW SEARCH IN HEADER
	navigation(".navigation > ul > li.search-header > a");

	//MOBILE NAVIGATION
	(function($){
		"use strict";

		$(".btn-nav-mobile").click(function(){
			$(this).toggleClass("active");
			$(".navigation > ul").toggleClass("collapse");
			
			if ($(".right-sidebar").hasClass('active')) {
				$(".right-sidebar").removeClass('active');
			}
			
			if ($(".user-header").find('.user-tool').hasClass('active')) {
				$(".user-header").find('.user-tool').removeClass('active');
				$(".user-header").find('.user-tool').slideUp(200);
			}
			
			$(".notification").removeClass('collapsed');
			
			if ($(".btn-nav-mobile").hasClass('active')) {
				$(".btn-nav-mobile").find('i')
					.removeClass('ion-chevron-down')
					.addClass('ion-chevron-up');
			} else {
				$(".btn-nav-mobile").find('i')
					.removeClass('ion-chevron-up')
					.addClass('ion-chevron-down');
			}

			if(!$(".navigation > ul").hasClass("collapse")){
				$(".navigation > ul > li").removeClass("collapsed");
			}
		});
	}(jQuery));
	
	// NOTIFIKASI
	$(".notification").click(function() {
		if ($(".btn-nav-mobile").hasClass('active')) {
			$(".btn-nav-mobile").removeClass('active');
			$(".btn-nav-mobile").next("ul").removeClass('collapse');
			$(".btn-nav-mobile").find('i')
				.removeClass('ion-chevron-up')
				.addClass('ion-chevron-down');
		}
		
		if ($(".user-header").find('.user-tool').hasClass('active')) {
			$(".user-header").find('.user-tool').removeClass('active');
			$(".user-header").find('.user-tool').slideUp(200);
		}
		
		if ($(".right-sidebar").hasClass('active')) {
			$(".right-sidebar").removeClass('active');
		}
	});

	//RIGHT SIDEBAR
	$(".right-sidebar-action").click(function(){
		$(".right-sidebar").toggleClass("active");
		$("body").toggleClass("disable-scroll");
		
		$(".notification").removeClass('collapsed');
		
		if ($(".btn-nav-mobile").hasClass('active')) {
			$(".btn-nav-mobile").removeClass('active');
			$(".btn-nav-mobile").next("ul").removeClass('collapse');
			$(".btn-nav-mobile").find('i')
				.removeClass('ion-chevron-up')
				.addClass('ion-chevron-down');
		}
		
		if ($(".user-header").find('.user-tool').hasClass('active')) {
			$(".user-header").find('.user-tool').removeClass('active');
			$(".user-header").find('.user-tool').slideUp(200);
		}
	});
	
	// USER HEADER
	$(".user-header").on('click', function(event) {
		var userTool = $(this).find('.user-tool');
		
		if (userTool.hasClass('active')) {
			userTool.removeClass('active');
			userTool.slideUp(200);
		} else {
			userTool.addClass('active');
			userTool.slideDown(200);
		}
		
		$(".notification").removeClass('collapsed');
		
		if ($(".btn-nav-mobile").hasClass('active')) {
			$(".btn-nav-mobile").removeClass('active');
			$(".btn-nav-mobile").next("ul").removeClass('collapse');
			$(".btn-nav-mobile").find('i')
				.removeClass('ion-chevron-up')
				.addClass('ion-chevron-down');
		}
		
		if ($(".right-sidebar").hasClass('active')) {
			$(".right-sidebar").removeClass('active');
		}
	});

	//SECOND SIDEBAR
	$(".second-sidebar .btn-mbl > .fa").click(function(){
		$(".body-second-sidebar").toggleClass("collapsed");
	});	
});

(function($){
	$(window).on("load",function(){
		$(".right-sidebar, .sidebar").mCustomScrollbar();
	});
})(jQuery);