$(document).ready(function() {
	$(".notif").on('click', function(event) {
		event.preventDefault();
		$(this).remove();
	});

	$(".btn-tool-src").on('click', function(event) {
		event.preventDefault();
		$(".pencarian").toggle();
	});
});

function notif (stat = false, title = "", msg = "") {
	var tagBefore = $("header");
	var notif = submsg = "";
	
	var bg = (stat) ? "notif-success" : "notif-danger";
	var icon = (stat) ? "fa-check" : "fa-exclamation-circle";
	
	if (msg == "") {
		submsg = "";
	} else {
		submsg = "<p style='margin-top: 10px;'>"+ msg +"</p>";
	}
	
	notif += "<div class='container-fluid notif "+bg+"'>";
	notif += "	<div class='row'>";
	notif += "		<div class='col-xs-12'>";
	notif += "			<h4 style='margin: 0px;'>";
	notif += "				<i class='fa "+icon+"'></i>";
	notif += "				"+title+"";
	notif += "			</h4>";
	notif += "			"+submsg+"";
	notif += "		</div>";
	notif += "	</div>";
	notif += "</div>";
	
	tagBefore.after(notif);
	$(".notif").on('click', function(event) {
		event.preventDefault();
		$(this).remove();
	});
}